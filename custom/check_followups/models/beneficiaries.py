# -*- coding: utf-8 -*-

from openerp import models, fields, api, _


class Beneficiaries(models.Model):
    _name = 'check.beneficiaries'

    name = fields.Char('Name')
    partner_ids = fields.Many2many('res.partner', string='Partners')
    partner_id = fields.Many2one('res.partner')
    code = fields.Char(string='Code', index=True, readonly=True,copy=False)
    address = fields.Char("Address")
    phone = fields.Char("Phone")
    mobile = fields.Char("Mobile")
    number = fields.Char("Number")
    email = fields.Char()
    sort_code = fields.Char("Sort Code")
    beneficiary_account = fields.Char("beneficiary Account")
    bank_account_ids = fields.One2many('check.beneficiaries.line','check_beneficiaries_id',string='Beneficiaries Details')


    @api.model
    def create(self, vals):
        partner_obj = self.env['res.partner']
        if not vals.get('code'):
            vals['code'] = self.env['ir.sequence'].next_by_code('check.beneficiaries') or _('New')
        
        res = super(Beneficiaries, self).create(vals)
        if 'partner_ids' in vals:
            if  vals.get('partner_ids'):
                if vals.get('partner_ids')[0][2]:
                    for ids in vals.get('partner_ids')[0][2]:
                        search_obj = partner_obj.search([('id', '=', ids)])

                        list_p = []
                        list_p.append((0,0,{'partner_id':search_obj.id, 'name':res.id }))
                        search_obj.write({'benefici_id':list_p})

           
        return res


    @api.multi
    def write(self, vals):
        partner_obj = self.env['res.partner']
        if 'partner_ids' in vals:

            ################# to delete 
            for par_id in self.partner_ids:
                if par_id.id not in vals.get('partner_ids')[0][2]:
                    for be in par_id.benefici_id:
                        if self.id == be.name.id:
                            be.unlink()
            ################################################################


            for ids in vals.get('partner_ids')[0][2]:
                search_obj = partner_obj.search([('id', '=', ids)])

                if not search_obj.benefici_id:
                    list_p = []
                    list_p.append((0,0,{'partner_id':search_obj.id, 'name':self.id }))
                    search_obj.write({'benefici_id':list_p})
                else:
                    flag = False
                    for benefici_id in search_obj.benefici_id:
                        if benefici_id.name.id == self.id:
                            flag = True
                            print benefici_id.name
                            
                    
                    if not flag:
                        list_p = []
                        list_p.append((0,0,{'partner_id':search_obj.id, 'name':self.id }))
                        search_obj.write({'benefici_id':list_p})


           
        return super(Beneficiaries, self).write(vals)

    @api.multi
    def unlink(self):
        partner_line_obj = self.env['res.partner.line'].search([('name.id', '=', self.id)])
        for partner in partner_line_obj:
            partner.unlink()

        return super(Beneficiaries, self).unlink()



class BeneficiariesBankAccount(models.Model):
    _name = 'beneficiaries.bank.account'

    name = fields.Char('Bank Name')

class CheckBeneficiariesLine(models.Model):
    _name = 'check.beneficiaries.line'

    check_beneficiaries_id = fields.Many2one('check.beneficiaries', string='Check Beneficiaries')
    bank_account_id = fields.Many2one('beneficiaries.bank.account', string='Bank Name')
    number = fields.Char('Account Number')
    ipan = fields.Char('Ipan Number')

