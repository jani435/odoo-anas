# -*- coding: utf-8 -*-

from odoo import models, fields, api

class reports_view(models.Model):
	_name = "reports_view"

	model = fields.Many2one('ir.model', string='Model')
	fields_ids = fields.Many2many('ir.model.fields', 'report_view_rel_ir_field', 'report_id', 'field_id', string='Fields')
	name = fields.Char(string='Name')
	domain = fields.Char(string='Domain')

	@api.multi
	def print_report(self):
		self.ensure_one()
		data = self.read()
		datas = {
		     'ids': [],
		     'model': 'ir.model',
		     'form': data[0]
		    }
		return self.env['report'].get_action(self, 'dynamic_report.report_template_custom', data=datas)