from odoo import fields, api, models ,_

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'
    
    ict_id = fields.Many2one('inter.company.transfer',string = "ICT",copy=False)
    
