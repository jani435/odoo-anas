{
    # App information
    
    'name': 'VE.18.05 Inter Company Transfer and Warehouse Transfer',
    'version': '10.0',
    'category': 'stock',
    'license': 'OPL-1',
    'summary' : 'Module to manage Inter Company Transfer and Inter Warehouse Transfer along with all required documents with easiest way by just simple configurations.',
    'description': """
   
    """,
    
    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com/',
    'maintainer': 'Emipro Technologies Pvt. Ltd.',
    
    
    # Dependencies
    
    'depends': ['delivery','sale','purchase','stock'],    
    'data': [
            'data/ir_sequence.xml',
            'data/inter_company_config.xml',
            'wizard/reverse_ict_wizard.xml',
            'view/configuration.xml',
            'view/intecompany_transaction.xml',
            'view/res_company.xml',
            'view/sale_order.xml',
            'view/purchase_order.xml',
            'view/stock_picking.xml',
            'reports/report.xml',
            'security/ICT_security.xml',
            'security/ir.model.access.csv',
             ],  
             
    # Odoo Store Specific   
    'images': ['static/description/Inter-Company-Transaction_covor.jpg'],         
               
    # Technical           
    'installable': True,
    'currency': 'EUR',
    'price': 79.00,
    'auto_install': False,
    'application': True,
}
