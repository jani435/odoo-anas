from odoo import fields, models

class ResCompany(models.Model):

    _inherit = "res.company"

    payment_method_id = fields.Many2one('account.payment.method', string='Payment Method Type',  oldname="payment_method")
    
