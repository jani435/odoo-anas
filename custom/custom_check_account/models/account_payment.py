# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
import time

class CheckBeneficiariesLine(models.Model):
    _inherit = 'check.beneficiaries.line'

    bpayment_id = fields.Many2one('account.payment', string="Beneficiary",)
    bpayment_line_id = fields.Many2one('account.payment.line', string="Beneficiary", ondelete='cascade')


class AccountPaymentLine(models.Model):
    _inherit = 'account.payment.line'


    @api.multi
    def action_delete(self):
	for rec in self:
		rec.unlink()

    @api.multi
    @api.depends('payment_id','payment_id.payment_to')
    def get_partners(self):
	p_obj= self.env['res.partner']
	for rec in self:
		if rec.payment_id.payment_to == 'supplier':
			p_ids= (p_obj.search([('supplier','=',True)])).ids
			rec.partners_ids = [(6,0,p_ids)]
		elif rec.payment_id.payment_to == 'customer':
			p_ids= (p_obj.search([('customer','=',True)])).ids
			rec.partners_ids = [(6,0,p_ids)]
		elif rec.payment_id.payment_to == 'employee':
			emp_list = []
			employee_ids = self.env['hr.employee'].search([])
			for emp in employee_ids:
				if emp.partner_id and emp.partner_id.id not in emp_list:
					emp_list.append(emp.partner_id.id)
			rec.partners_ids = [(6,0,emp_list)]


		else:
			p_ids= (p_obj.search([])).ids
			rec.partners_ids = [(6,0,p_ids)]


    check_memo = fields.Char('Check Memo')
    Check_no = fields.Char('Check No',)
    Account_No = fields.Char(string='Account No')
    partner_bank_account = fields.Many2one('partner.bank.account', 'Partner Account', store=False)
    Bank_id = fields.Many2one('res.bank', string='Bank')
    check_date = fields.Date('Check Date', default=fields.Date.context_today)
    beneficiary_id = fields.Many2one('check.beneficiaries', string="Beneficiary", domain=[('id','not in',[-1])])

    b_name = fields.Char(related="beneficiary_id.name", string='Name', store=True)
    b_code = fields.Char(related="beneficiary_id.code",string='Code', index=True, readonly=True,copy=False, store=True)
    b_address = fields.Char(related="beneficiary_id.address",string="Address", store=True)
    b_phone = fields.Char(related="beneficiary_id.phone",string="Phone", store=True)
    b_mobile = fields.Char(related="beneficiary_id.mobile",string="Mobile", store=True)
    b_number = fields.Char(related="beneficiary_id.number",string="Number", store=True)
    b_email = fields.Char(related="beneficiary_id.email",string="Email", store=True)
    b_sort_code = fields.Char(related="beneficiary_id.sort_code",string="Sort Code", store=True)
    b_beneficiary_account = fields.Char(related="beneficiary_id.beneficiary_account", string="beneficiary Account", store=True)
    bb_partner_ids = fields.Many2many('res.partner','pb_p_rel','bpartner','bpayement', related="beneficiary_id.partner_ids", string='Partners', store=True)
    b_bank_account_ids = fields.One2many('check.beneficiaries.line','bpayment_line_id', related="beneficiary_id.bank_account_ids", store=True, string='Beneficiaries Details')
    partners_ids= fields.Many2many('res.partner', 'p_p_rel', 'p_ids', 'pa_ids', compute='get_partners', string='Partners')
	


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    bank_type = fields.Selection([('check', 'Check'),('transformation', 'Transformation'),('master_card', 'Master Card'),('visa', 'Visa'),('span', 'SPAN'),('other', 'Others')], default='check', string='Bank Type',track_visibility='onchange')
    beneficiary_id = fields.Many2one('check.beneficiaries', string="Beneficiary", domain=[('id','not in',[-1])])
    notee = fields.Char('Note',track_visibility='onchange')
    b_name = fields.Char(related="beneficiary_id.name", string='Name', store=True)
    b_code = fields.Char(related="beneficiary_id.code",string='Code', index=True, readonly=True,copy=False, store=True)
    b_address = fields.Char(related="beneficiary_id.address",string="Address", store=True)
    b_phone = fields.Char(related="beneficiary_id.phone",string="Phone", store=True)
    b_mobile = fields.Char(related="beneficiary_id.mobile",string="Mobile", store=True)
    b_number = fields.Char(related="beneficiary_id.number",string="Number", store=True)
    b_email = fields.Char(related="beneficiary_id.email",string="Email", store=True)
    b_sort_code = fields.Char(related="beneficiary_id.sort_code",string="Sort Code", store=True)
    b_beneficiary_account = fields.Char(related="beneficiary_id.beneficiary_account", string="beneficiary Account", store=True)
    b_partner_ids = fields.Many2many('res.partner','p_p_rel','partner','payement', related="beneficiary_id.partner_ids", string='Partners', store=True)
    b_bank_account_ids = fields.One2many('check.beneficiaries.line','bpayment_id', related="beneficiary_id.bank_account_ids", store=True, string='Beneficiaries Details')
    check_check_no= fields.Boolean('Check The No',track_visibility='onchange')
    beneficiary_type= fields.Selection([('one','One'),('multi','Multi')], default="multi", string='Beneficiary Type')
    credit_debit_parts= fields.Boolean('Credit/Debits Parts?')
    spartner_id = fields.Many2one('res.partner', 'Partner Line', related='line_ids.partner_id')
    saccount_id = fields.Many2one('account.account', 'Account', related='sec_line_ids.account_id')
    #slabel = fields.Char(string='Label', related='line_ids.label')
    #sCheck_no = fields.Char(string='Check No', related='line_ids.Check_no')

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
	c= 0
	ss = False
	for d in domain:
		if d[0] == 'beneficiary_id':
			ss= d[2]
			domain.pop(c)
		c += 1
	if ss :
		l_ids= self.env['account.payment.line'].search([('beneficiary_id.name','ilike',ss),('payment_id.payment_type', '=', 'outbound'), ('payment_id.multi_supplier', '=', True)])
		domain.append(('id','in',[x.payment_id.id for x in l_ids]))
	#print domain," beneficiary_id  &&&&&&&&&&   ",fields
        return super(AccountPayment, self).search_read(domain, fields, offset, limit, order)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
	c= 0
	ss = False
	for d in domain:
		if d[0] == 'beneficiary_id':
			ss= d[2]
			domain.pop(c)
		c += 1
	if ss :
		l_ids= self.env['account.payment.line'].search([('beneficiary_id.name','ilike',ss),('payment_id.payment_type', '=', 'outbound'), ('payment_id.multi_supplier', '=', True)])
		domain.append(('id','in',[x.payment_id.id for x in l_ids]))
        return super(AccountPayment, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)

    #@api.model
    #def search(self, args, offset=0, limit=None, order=None, count=False):
    #    context = self._context or {}
    #	print context," beneficiary_id  ##########   ",args
        #if context.get('journal_id'):
         #   journal = self.env['account.journal'].browse(context.get('journal_id'))
         #   if journal.type in ('sale', 'purchase'):
	#args += [('line_ids.beneficiary_id', 'ilike', journal.type)]
   #     return super(AccountPayment, self).search(args, offset, limit, order, count=count)

    @api.multi
    @api.constrains('journal_id','Check_no','payment_type','multi_supplier','check_check_no')
    def constrain_check_no_check(self):
	for rec in self:
		if rec.journal_id and rec.Check_no and (rec.payment_type == 'outbound') and  (rec.journal_id.type == 'bank') and (rec.multi_supplier ) and (rec.check_check_no):
			n_ids= self.env['manual.invoice.number'].search([('range_from','<=',int(rec.Check_no)),('range_to','>=',int(rec.Check_no)),('journal_id','<=',rec.journal_id.id)])
			#if not n_ids:
				#raise ValidationError('Error, Check Number!')
    @api.multi
    @api.constrains('journal_id','Check_no','payment_type','multi_supplier','check_check_no','sec_line_ids','line_ids','bank_type','payment_to')
    def constrain_check_no_line(self):
	for rec in self:
		if rec.journal_id and rec.Check_no and (rec.payment_type == 'outbound') and  (rec.journal_id.type == 'bank') and (rec.multi_supplier ) and (rec.check_check_no) and (rec.line_ids) and (rec.bank_type == 'check') and (rec.payment_to != 'account'):
			n_obj=self.env['manual.invoice.number']
			for line in rec.line_ids:
				nn_ids= n_obj.search([('journal_id','=',rec.journal_id.id)])
				if (not line.Check_no) or (line.Check_no == '/') or (not (line.Check_no).isdigit()) or (not nn_ids):
					continue
				n_ids= n_obj.search([('range_from','<=',int(line.Check_no)),('range_to','>=',int(line.Check_no)),('journal_id','<=',rec.journal_id.id)])
				if not n_ids:
					raise ValidationError(_('Error, Check Number!'))
		elif rec.journal_id and rec.Check_no and (rec.payment_type == 'outbound') and  (rec.journal_id.type == 'bank') and (rec.multi_supplier ) and (rec.check_check_no) and (rec.sec_line_ids) and (rec.bank_type == 'check') and (rec.payment_to == 'account'):
			n_obj=self.env['manual.invoice.number']
			for line in rec.sec_line_ids:
				nn_ids= n_obj.search([('journal_id','=',rec.journal_id.id)])
				if (not line.Check_no) or (line.Check_no == '/') or (not (line.Check_no).isdigit()) or (not nn_ids):
					continue
				n_ids= n_obj.search([('range_from','<=',int(line.Check_no)),('range_to','>=',int(line.Check_no)),('journal_id','<=',rec.journal_id.id)])
				if not n_ids:
					raise ValidationError(_('Error, Check Number!'))

    @api.multi
    @api.onchange('journal_id','payment_type')
    def onchange_get_payment_method_company(self):
	for rec in self:
		if (rec.payment_type == 'outbound') and (rec.journal_id.type == 'bank') and self._context.get('default_multi_supplier',False):
			#print self.env.user.company_id.payment_method_id.name, "CCC    CC  1111 ", self._context
			rec.payment_method_id= self.env.user.company_id.payment_method_id.id


