# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Partners Mail",
    'version': '1.0',
    'summary': 'send emails to partners',
    'author': 'IES Team',
    'sequence': 1,
    'description': '',
    'category': 'Custom',
    'website': '',
    'depends': ['mail','contacts'],

    'data': [
	'security/ir.model.access.csv',
        'data/email_template.xml',
        'views/res_partner_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
