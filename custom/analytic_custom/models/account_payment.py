# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    analytic_plan_id = fields.Many2one(
        'account.analytic.plan', string='Analytic Plan')
    
    def _get_shared_move_line_vals(self, debit, credit, amount_currency, move_id, invoice_id=False):
        """ Returns values common to both move lines (except for debit, credit and amount_currency which are reversed)
        """
        returned_dict = {
            'partner_id': self.payment_type in ('inbound', 'outbound') and self.env['res.partner']._find_accounting_partner(self.partner_id).id or False,
            'invoice_id': invoice_id and invoice_id.id or False,
            'move_id': move_id,
            'debit': debit,
            'credit': credit,
            'amount_currency': amount_currency or False,
        }
        if self.analytic_plan_id:
            returned_dict['analytic_plan_id'] = self.analytic_plan_id.id
        return returned_dict
