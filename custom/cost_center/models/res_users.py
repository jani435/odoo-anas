# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ResUsers(models.Model):
    _inherit = 'res.users'

    user_areas_ids = fields.Many2many('cost_center.areas', 'areas_user_rel', 'user_id', 'area_id', 'Allowed Areas')
    user_default_area = fields.Many2one('cost_center.areas', string='Default Area')

    user_branches_ids = fields.Many2many('cost_center.branches', 'branches_user_rel', 'user_id', 'branch_id',
                                         'Allowed Branches')
    user_default_branch = fields.Many2one('cost_center.branches', string='Default Branch')

    user_cars_ids = fields.Many2many('cost_center.cars', 'cars_user_rel', 'user_id', 'car_id', 'Allowed Cars')
    user_default_car = fields.Many2one('cost_center.cars', string='Default Car')

    user_cost_centers_ids = fields.Many2many('cost_center.cost_center', 'cost_centers_user_rel', 'user_id',
                                             'cost_center_id', 'Allowed Cost Centers')
    user_default_cost_center = fields.Many2one('cost_center.cost_center', string='Default Cost Center')

    user_departments_ids = fields.Many2many('cost_center.department', 'departments_user_rel', 'user_id',
                                            'department_id', 'Allowed Departments')
    user_default_department = fields.Many2one('cost_center.department', string='Default Department')

    user_projects_ids = fields.Many2many('cost_center.project', 'projects_user_rel', 'user_id', 'project_id',
                                         'Allowed Projects')
    user_default_project = fields.Many2one('cost_center.project', string='Default Project')
