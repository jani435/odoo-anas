
from odoo import api, fields, models, _
from datetime import date,datetime
from odoo.exceptions import ValidationError
from dateutil.parser import parse
from odoo import tools


def get_date(str):
    return datetime.strptime(str, "%Y-%m-%d").date()

class AccountAccountNew(models.Model):
    _name = 'account.account.new'
    _auto = False

    name = fields.Char('name')
    code = fields.Char('code')
    all_debit = fields.Float('debit')
    all_credit = fields.Float('credit')
    balance_today = fields.Float('Total Balance')
    
    def _get_account_balane(self):
        balance_date = datetime.now().date()
        print "***************************",balance_date
        query_str = """
                        select 
                        acc.id as id,
                        acc.name as name,
                        acc.code as code, 
                        round(sum(m.debit),2) as all_debit,  
                        round(sum(m.credit),2) as all_credit, 
                        round(sum(m.debit) - sum(m.credit) ,2) as balance_today  
                        from account_move_line m , account_move v, account_account acc 
                        where m.move_id = v.id 
                        and v.date <= '%s' 
                        and v.state = 'posted' 
                        and m.account_id in (select id from account_account)
                        and m.account_id = acc.id 
                        group by 
                        acc.id,
                        acc.name, 
                        acc.code
                    """ %(balance_date)
        return query_str

    @api.model_cr
    def init(self):
        print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            )""" % (self._table, self._get_account_balane()))

class ResPartnerSupplier(models.Model):
    _name = 'res.partner.supplier'
    _auto = False

    name = fields.Char('name')
    code = fields.Char('code')
    all_debit = fields.Float('debit')
    all_credit = fields.Float('credit')
    balance_today = fields.Float('Total Balance')
    
    def _get_supplier_balance(self):
        balance_date = datetime.now().date()
        print "LLLLLLLLLLLLLLLLLLLLLLLLLL",balance_date
        query_str = """
                        select 
                        res_pa.id as id,
                        res_pa.name as name,
                        res_pa.code as code, 
                        round(sum(m.debit),2) as all_debit,  
                        round(sum(m.credit),2) as all_credit, 
                        round(sum(m.debit) - sum(m.credit) ,2) as balance_today  
                        from account_move_line m , account_move v, res_partner res_pa
                        where m.move_id = v.id 
                        and v.date <= '%s' 
                        and v.state = 'posted' 
                        and m.account_id = res_pa.account_vendor 
                        and m.partner_id = res_pa.id 
                        group by 
                        res_pa.id,
                        res_pa.name, 
                        res_pa.code
                    """ %(balance_date)
        return query_str
    
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            )""" % (self._table, self._get_supplier_balance()))

class ResPartnerCustomer(models.Model):
    _name = 'res.partner.customer'
    _auto = False

    name = fields.Char('name')
    code = fields.Char('code')
    all_debit = fields.Float('debit')
    all_credit = fields.Float('credit')
    balance_today = fields.Float('Total Balance')
    
    def _get_customer_balance(self):
        balance_date = datetime.now().date()
        query_str = """
                        select 
                        res_pa.id as id,
                        res_pa.name as name,
                        res_pa.code as code, 
                        round(sum(m.debit),2) as all_debit,  
                        round(sum(m.credit),2) as all_credit, 
                        round(sum(m.debit) - sum(m.credit) ,2) as balance_today  
                        from account_move_line m , account_move v,res_partner res_pa
                        where m.move_id = v.id 
                        and v.date <= '%s' 
                        and v.state = 'posted' 
                        and m.account_id = res_pa.account_customer  
                        and m.partner_id = res_pa.id 
                        group by 
                        res_pa.id,
                        res_pa.name, 
                        res_pa.code
                    """ %(balance_date)
        return query_str
    
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            )""" % (self._table, self._get_customer_balance()))