# -*- coding: utf-8 -*-

from odoo import fields, api, models
from odoo import _
from odoo.exceptions import ValidationError
import dateutil.parser



class ProductTemplate(models.Model):

    _inherit = 'product.template'


    @api.model
    def create(self, vals):
	if not vals.get('taxes_id',False):
		if not vals.get('supplier_taxes_id',False) :
			raise ValidationError(_('Sorry .. You Must Enter The Taxes '))
		else :
			raise ValidationError(_('Sorry .. You Must Enter The Customer Taxes '))
	else :

		if not vals.get('supplier_taxes_id',False) :
			raise ValidationError(_('Sorry .. You Must Enter The Vendor Taxes '))


        return super(ProductTemplate, self).create(vals)
