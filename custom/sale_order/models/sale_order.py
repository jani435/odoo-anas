# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
import dateutil.parser
from odoo.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic
from odoo.addons.check_followups.models.money_to_text_en import amount_to_text
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare, float_round
from dateutil.relativedelta import relativedelta


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _get_default_location(self):

        if self.env.user.user_default_warehouse:
            return self.env['stock.location'].search(
                [('location_id', '=', self.env.user.user_default_warehouse.view_location_id.id)], limit=1).id
        else:
            return False

    @api.multi
    @api.depends('date_order')
    def _get_computed_date_order(self):
        for rec in self:
            if rec.date_order:
                rec.computed_date_order = dateutil.parser.parse(rec.date_order).date()

    @api.multi
    @api.depends('amount_total')
    def _get_total_amount_in_words(self):
        for rec in self:
            if rec.amount_total:
                rec.total_words_ar = amount_to_text_arabic(rec.amount_total, 'SAR')
                rec.total_words_en = amount_to_text(rec.amount_total, 'SAR')

    computed_date_order = fields.Date(string='Computed Order Date', compute="_get_computed_date_order", store=True)
    location_id = fields.Many2one('stock.location', string='Location', default=_get_default_location)
    invoice_number = fields.Char('Invoice Number')
    manual_inv_num = fields.Char('Manual Number')
    view_type = fields.Char('', default='default')
    total_words_ar = fields.Char('Total Words ar', compute="_get_total_amount_in_words")
    total_words_en = fields.Char('Total Words en', compute="_get_total_amount_in_words")

    @api.onchange('warehouse_id')
    def _onchange_warehouse_id(self):
        res = dict()
        if self.warehouse_id:
            location_ids = self.env['stock.location'].search(
                [('location_id', '=', self.warehouse_id.view_location_id.id)]).ids

            res['domain'] = {'location_id': [('id', 'in', location_ids)]}
        return res

    @api.constrains('manual_inv_num')
    def _check_manual_inv_num(self):

        invoice_numbers = self.env['manual.invoice.number'].sudo().search([('type', '=', 'sale')])

        if self.manual_inv_num and len(invoice_numbers) > 0:

            if len(self.env['sale.order'].sudo().search(
                    [('manual_inv_num', '=', self.manual_inv_num), ('state', '!=', 'cancel')])) > 1:
                raise ValidationError(_('This number is reserved in another sale order..!'))

            invoice_number_found = False

            for invoice in invoice_numbers:
                if invoice.range_from <= int(self.manual_inv_num) <= invoice.range_to:
                    invoice_number_found = True
                    break
            if not invoice_number_found:
                raise ValidationError(_('Invalid invoice number, this number not found in sale orders numbers range.'))

    @api.multi
    def action_direct_invoice(self):
        self.action_invoice_create()
        return self.action_view_invoice()

    @api.multi
    def action_view_invoice(self):
        invoices = self.mapped('invoice_ids')
        action = self.env.ref('custom_account.custom_account_invoice_action').read()[0]
        if len(invoices) > 1:
            action['domain'] = [('id', 'in', invoices.ids)]
        elif len(invoices) == 1:
            action['views'] = [(self.env.ref('custom_account.custom_account_invoice_form').id, 'form')]
            action['res_id'] = invoices.ids[0]
            action['flags'] = {'options': {'mode': 'edit'}}
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    @api.multi
    def action_confirm(self):
        ctx = self.env.context.copy()
        ctx.update({
            'cr_location_id': self.location_id
        })
        self.env.context = ctx
        super(SaleOrder, self).action_confirm()

    @api.one
    def fetch_products(self):
        current_products = [x.product_id.id for x in self.order_line]
        vals = []
        if self.warehouse_id:
            if self.partner_id:
                if self.partner_id.product_location_ids:
                    conf_warehouses = filter(lambda x: x.location_id.id == self.warehouse_id.id,
                                             self.partner_id.product_location_ids) or []
                    for wh in conf_warehouses:
                        if wh.products_ids:
                            wh_products_ids = sorted(wh.products_ids, key=lambda l: l.sequence)
                            for line in wh_products_ids:
                                if line.product_id.id not in current_products:
                                    vals.append((0, False, {'product_id': line.product_id.id}))
        if vals:
            ctx = self.env.context.copy()
            ctx.update({
                'no_delete_line': True
            })
            self.env.context = ctx
            self.order_line = vals

    @api.model
    def create(self, vals):
        res = super(SaleOrder, self).create(vals)
        ctx = self.env.context.copy()
        if 'no_delete_line' not in ctx:
            for line in res.order_line:
                if line.new_quantity == 0:
                    line.unlink()
        return res

    @api.multi
    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        ctx = self.env.context.copy()
        if 'no_delete_line' not in ctx:
            for rec in self:
                for line in rec.order_line:
                    if line.new_quantity == 0:
                        line.unlink()
        return res

    @api.model
    def _prepare_procurement_group_by_line(self, line):
        vals = super(SaleOrder, self)._prepare_procurement_group_by_line(line)
        # for compatibility with sale_quotation_sourcing
        if line._get_procurement_group_key()[0] == 10:
            if line.warehouse_id:
                vals['name'] += '/' + line.warehouse_id.name
        return vals


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.depends('order_id.date_order')
    def _get_computed_date_order(self):
        for rec in self:
            rec.date_order = rec.order_id.date_order

    @api.depends('order_id.manual_inv_num')
    def _compute_manual_inv_num(self):
        for rec in self:
            if rec.order_id:
                rec.manual_inv_num = rec.order_id.manual_inv_num

    warehouse_id = fields.Many2one(
        'stock.warehouse',
        'Source Warehouse',
        readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        help="If a source warehouse is selected, "
             "it will be used to define the route. "
             "Otherwise, it will get the warehouse of "
             "the sale order")

    date_order = fields.Date('Order Date', compute='_get_computed_date_order')
    new_quantity = fields.Float('Quantity')
    new_qty_invoiced = fields.Float(string='Qty Inv', copy=False)
    new_price = fields.Float('Price')
    new_price_with_tax = fields.Float('Price Tax')
    new_price_total = fields.Float('Total', compute='_compute_total_price')
    tax_amount = fields.Float('Tax Amount', compute='_compute_tax_amount')
    new_weight = fields.Float('Weight', default=1)
    free_quantity = fields.Float('Free Quantity')
    manual_inv_num = fields.Char('Manual Number', compute=_compute_manual_inv_num)

    @api.onchange('new_price')
    def _onchange_new_price(self):
        if self.product_id.taxes_id:
            self.new_price_with_tax = self.new_price + ((self.new_price * self.product_id.taxes_id[0].amount) / 100)
        else:
            self.new_price_with_tax = self.new_price

    @api.onchange('new_price_with_tax')
    def _onchange_new_price_with_tax(self):
        if self.product_id.taxes_id:
            self.new_price = 100 * (self.new_price_with_tax / (100 + self.product_id.taxes_id[0].amount))
        else:
            self.new_price = self.new_price_with_tax

    @api.depends('new_price', 'new_quantity', 'tax_id')
    def _compute_total_price(self):
        for rec in self:
            rec.new_price_total = rec.new_quantity * rec.new_price + \
                                  (rec.new_quantity * rec.new_price) * rec.tax_id.amount / 100

    @api.depends('tax_id', 'product_uom_qty', 'price_unit')
    def _compute_tax_amount(self):
        for rec in self:
            if rec.tax_id:
                rec.tax_amount = (rec.product_uom_qty * rec.price_unit) * rec.tax_id.amount / 100

    @api.onchange('new_quantity', 'new_price', 'new_weight', 'free_quantity')
    def _onchange_new_quantity(self):
        product_quantity = (self.new_quantity + self.free_quantity) * self.new_weight

        if product_quantity > 0:
            price_unit = float((self.new_quantity * self.new_price) / product_quantity)
        else:
            price_unit = 0
        self.product_uom_qty = product_quantity
        self.price_unit = price_unit

    @api.multi
    def _prepare_order_line_procurement(self, group_id=False):

        result = super(SaleOrderLine, self)._prepare_order_line_procurement(group_id=group_id)
        result['new_quantity'] = self.new_quantity
        result['new_price'] = self.new_price
        result['new_weight'] = self.new_weight
        result['free_quantity'] = self.free_quantity
        if self.warehouse_id:
            result['warehouse_id'] = self.warehouse_id.id
        return result

    @api.multi
    def _prepare_invoice_line(self, qty):

        result = super(SaleOrderLine, self)._prepare_invoice_line(qty)

        result.update({
            'new_quantity': self.new_quantity,
            'new_price': self.new_price,
            'new_weight': self.new_weight,
            'free_quantity': self.free_quantity,
        })
        if self.warehouse_id:
            result.update({
                'warehouse_id': self.warehouse_id.id,
            })
        return result

    @api.multi
    def _get_procurement_group_key(self):
        """ Return a key with priority to be used to regroup lines in multiple
        procurement groups

        """
        priority = 10
        key = super(SaleOrderLine, self)._get_procurement_group_key()
        # Check priority
        if key[0] >= priority:
            return key
        wh_id = self.warehouse_id.id if self.warehouse_id else \
            self.order_id.warehouse_id.id
        return priority, wh_id


class ProcurementOrder(models.Model):
    _inherit = "procurement.order"

    new_quantity = fields.Float('Quantity')
    new_price = fields.Float('Price')
    new_weight = fields.Float('Weight', default=1)
    free_quantity = fields.Float('Free Quantity')

    def _get_stock_move_values(self):
        result = super(ProcurementOrder, self)._get_stock_move_values()

        result.update({
            'new_quantity': self.new_quantity,
            'new_price': self.new_price,
            'new_weight': self.new_weight,
            'free_quantity': self.free_quantity,
        })
        return result
