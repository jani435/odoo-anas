# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api


class SaleConfiguration(models.TransientModel):
    _inherit = 'sale.config.settings'

    # # ********************** Default screen configuration ***********************
    #
    # default_direct_invoice = fields.Boolean(related='company_id.direct_direct_invoice')
    # default_direct_receive = fields.Boolean(related='company_id.direct_direct_receive')


class ResCompany(models.Model):
    _inherit = "res.company"

