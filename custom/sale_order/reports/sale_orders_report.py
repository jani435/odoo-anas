# -*- coding: utf-8 -*-

import time
from odoo import api, models


class SaleOrders(models.AbstractModel):
    _name = 'report.sale_order.sale_order_report'

    @api.model
    def render_html(self, docids, data=None):
        docs = self.env['sale.order'].sudo().browse(docids)
        data = []
        for doc in docs:
            product_lines = {}
            for line in doc.order_line:
                if line.product_id.tag_id.id not in product_lines:
                    product_lines[line.product_id.tag_id.id] = [line]
                else:
                    product_lines[line.product_id.tag_id.id].append(line)
            data.append({
                'docs': doc,
                'order_lines': product_lines
            })
        docargs = {
            'docs': docs,
            'data': data,
        }
        return self.env['report'].render('sale_order.sale_order_report', docargs)
