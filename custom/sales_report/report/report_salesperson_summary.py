# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError
from datetime import datetime, timedelta
from dateutil import relativedelta


class ReportSalesperson_detailed(models.AbstractModel):
    _name = 'report.sales_report.report_salesperson_summary'

    def get_totals(self, docs,em_id):
        sales_records = []
        if (not docs.date_from) or (not docs.date_to):
            raise UserError("Please enter duration")
        #orders = self.env['vendors.invoice'].search(
        #    [('sales_man_id', '=', docs.salesperson_id.id)])
        orders = self.env['vendors.invoice'].search(
            [('sales_man_id', '=', em_id.id),('invoice_date','>=',docs.date_from),('invoice_date','<=',docs.date_to)])
        total = {'amount_total': 0.0, 'total_taxed_amount': 0.0, 'credit_custody': 0.0, 'cash_custody': 0.0,
                 'commision_total_amount': 0.0, 'credit_sales': 0.0, 'main_customer_total_untaxed': 0.0, }
        number_of_cards = {}

        total_taxed_amount=0

        for order in orders:
            customer_inv=self.env['account.invoice.line'].search(
            [('vendor_invoice_id.name','=',order.name),('partner_id','=',em_id.linked_customer.id)])
            for inv in customer_inv:
                total_taxed_amount+=inv.tax_amount

        if orders.ids:
            self._cr.execute("select sum(round(cast(vendors_invoice.amount_total as numeric), 2)) as amount_total," \
                            #"sum(round(cast(vendors_invoice.main_customer_total_amount-vendors_invoice.main_customer_total_untaxed as numeric), 2)) as total_taxed_amount," \
                            "sum(round(cast(vendors_invoice.credit_custody as numeric), 2)) as credit_custody, " \
                            "sum(round(cast(vendors_invoice.cash_custody as numeric), 2)) as cash_custody," \
                            "sum(round(cast(vendors_invoice.commision_total_amount as numeric), 2)) as commision_total_amount, " \
                            "sum(round(cast(vendors_invoice.credit_sales as numeric), 2)) as credit_sales," \
                            "sum(round(cast(vendors_invoice.main_customer_total_untaxed as numeric), 2)) as main_customer_total_untaxed " \
                            "from vendors_invoice "\
                            "left join vendors_custody_line on (vendors_custody_line.vendor_invoice_id = vendors_invoice.id and vendors_custody_line.is_complex is True) "\
                            "where  vendors_invoice.id in %s ", (tuple(orders.ids), ))
            res = self._cr.dictfetchall()
            total = {'amount_total': res[0]['amount_total'] is None and 0.0 or res[0]['amount_total'], 
                     'total_taxed_amount':total_taxed_amount, 
                     'credit_custody': res[0]['credit_custody'] is None and 0.0 or res[0]['credit_custody'], 
                     'cash_custody': res[0]['cash_custody'] is None and 0.0 or res[0]['cash_custody'],
                     'commision_total_amount': res[0]['commision_total_amount'] is None and 0.0 or res[0]['commision_total_amount'], 
                     'credit_sales': res[0]['credit_sales'] is None and 0.0 or res[0]['credit_sales'], 
                     'main_customer_total_untaxed': res[0]['main_customer_total_untaxed'] is None and 0.0 or res[0]['main_customer_total_untaxed'], }
            
            self._cr.execute("select vendors_custody_line.vendor_invoice_id, round((sum(vendors_custody_line.cash_amount + vendors_custody_line.credit_amount)/51.0) , 0) " \
                             "from vendors_custody_line "\
                             "where vendors_custody_line.is_complex is True and vendors_custody_line.vendor_invoice_id in %s "\
                             "group by vendors_custody_line.vendor_invoice_id ", (tuple(orders.ids), ))
            res2 = self._cr.fetchall()
            number_of_cards = dict(res2)


        '''if docs.date_from and docs.date_to:
            for order in orders:
                if parse(docs.date_from) <= parse(order.invoice_date) \
                        and parse(docs.date_to) >= parse(order.invoice_date):
                    sales_records.append(order)
                    total['amount_total'] += abs(
                        round(order.amount_total, 2))
                    total['total_taxed_amount'] += abs(
                        round(order.main_customer_total_amount - order.main_customer_total_untaxed, 2))
                    total['credit_custody'] += abs(
                        round(order.credit_custody, 2))
                    total['cash_custody'] += abs(
                        round(order.cash_custody, 2))
                    total['commision_total_amount'] += abs(
                        round(order.commision_total_amount, 2))
                    total['credit_sales'] += abs(
                        round(order.credit_sales, 2))
                    total['main_customer_total_untaxed'] += abs(
                        round(order.main_customer_total_untaxed, 2))
                    number_of_cards[order.id] = 0.0
                    for custody in order.custody_lines:
                        if custody.is_complex:
                            number_of_cards[order.id] += custody.cash_amount + \
                                custody.credit_amount
                    number_of_cards[order.id] = round(
                        number_of_cards[order.id] / 51.0, 0)

        else:
            raise UserError("Please enter duration")'''

        return total

    def get_opening(self, docs,em_id):
        orders = self.env['account.move.line'].search(
            [('account_id', '=', em_id.cash_account.id), ('date', '<', docs.date_from)], order='date')
        total = {'debit': 0.0, 'credit': 0.0, 'balance': 0.0}
       
        pre_total = {'debit': 0.0, 'credit': 0.0, 'balance': 0.0}
        is_pre_total = False
        balance = 0
        if orders.ids:
            self._cr.execute("select round(sum(abs(account_move_line.debit)),2) as debit," \
                            "round(sum(abs(account_move_line.credit)),2) as credit, " \
                            "round(sum(account_move_line.debit - account_move_line.credit),2) as balance " \
                            "from account_move_line "\
                            "where  account_move_line.id in %s ", (tuple(orders.ids), ))
            res = self._cr.dictfetchall()
            total = {'debit': res[0]['debit'] is None and 0.0 or res[0]['debit'], 
                     'credit': res[0]['credit'] is None and 0.0 or res[0]['credit'], 
                     'balance': res[0]['balance'] is None and 0.0 or res[0]['balance'], 
                     }
      
        '''for order in orders:
            total['debit'] += abs(
                round(order.debit, 2))
            total['credit'] += abs(
                round(order.credit, 2))
            total['balance'] += order.debit - order.credit'''
        return total

    def get_invoice_total(self, docs,em_id):
        total = 0.0
        #if docs.date_from and docs.date_to:
        if (not docs.date_from) or (not docs.date_to):
            raise UserError("Please enter duration")
        partners = self.env['res.partner'].search(
        [('property_account_payable_id', '=', em_id.cash_account.id)])
        #partners = [x.id for x in partners]

        #invoices = self.env['vendors.invoice'].search(
        #[('vendor_id', 'in', partners)])
        invoices = self.env['vendors.invoice'].search(
        [('vendor_id', 'in', partners.ids),('invoice_date','>=',docs.date_from),('invoice_date','<=',docs.date_to)])
        if invoices.ids:
            self._cr.execute("select round(sum(abs(vendors_invoice.net_total)),2) as net_total " \
                            "from vendors_invoice "\
                            "where vendors_invoice.id in %s ", (tuple(invoices.ids), ))
            res = self._cr.dictfetchall()
            if res[0]['net_total'] is None : res[0]['net_total'] = 0.0
            total = res[0]['net_total']

        '''for invoice in invoices:
            if parse(docs.date_from) <= parse(invoice.invoice_date) \
                    and parse(docs.date_to) >= parse(invoice.invoice_date):
                total += invoice.net_total

        else:
            raise UserError("Please enter duration")'''

        return total

    def get_payments(self, docs,em_id):
	posted_sum = 0.0
	credit_sum = 0.0
	debit_sum = 0.0
        if (not docs.date_from) or (not docs.date_to):
            raise UserError("Please enter duration")
        payments = self.env['account.move.line'].search(
            [('partner_id', '=', em_id.linked_customer.id),('date','>=',docs.date_from),('date','<=',docs.date_to),
             ('account_id', '=', docs.salesperson_id.linked_customer.property_account_receivable_id.id),('balance','>',0)], order='date')
        all_debit_data = []
	all_credit_data =[]
	for line in payments   :
		if  line.ref and (("C/" not in  line.ref[:2]) and  ("V/" not in  line.ref[:2])):
			
			all_debit_data.append(line)
			posted_sum -=line.debit
			debit_sum += line.debit
		elif not line.ref:
			all_debit_data.append(line)
			posted_sum -=line.debit
			debit_sum += line.debit


        payments = self.env['account.move.line'].search(
            [('partner_id', '=', em_id.linked_customer.id),('date','>=',docs.date_from),
             ('account_id', '=', docs.salesperson_id.linked_customer.property_account_receivable_id.id),('date','<=',docs.date_to),('balance','<',0)], order='date')
	for line in payments   :
		if  line.ref and (("C/" not in  line.ref[:2]) and  ("V/" not in  line.ref[:2])):
			
			all_credit_data.append(line)
			posted_sum +=line.credit
			credit_sum+=line.credit
		elif not line.ref:
			all_credit_data.append(line)
			posted_sum +=line.credit
			credit_sum+=line.credit



        return all_debit_data,all_credit_data,debit_sum,credit_sum,posted_sum

    def get_next_days(self,docs):
        dd = []
        if docs.date_from and docs.date_to:
            DATETIME_FORMAT = "%Y-%m-%d"
            date_from = datetime.strptime(docs.date_from, DATETIME_FORMAT)
            date_to = datetime.strptime(docs.date_to, DATETIME_FORMAT)
            dd = [date_from + timedelta(days=x) for x in range((date_to - date_from).days + 1)]
        return dd

    def get_label(self,partner,payment):
        payment_lines_label = self.env['account.payment.line'].search([('partner_id','=',partner),('payment_id','=',payment)]).label
        return payment_lines_label

    def get_emp(self,docs):
        dtt = {}
        dt_lst = []
        if docs.salesperson_id:
            dt_lst = [('id','=',docs.salesperson_id.id)]
        empl = self.env['hr.employee'].search(dt_lst)
        list_days = self.get_next_days(docs)
        for em in empl:
            debit_payments, credit_payments, debit_sum, credit_sum, posted_payments_sum = self.get_payments(docs,em)
            dtt[em] = dtt.get(em, {'data':[],
                                   'total': self.get_totals(docs,em),
                                   'invoices_total':self.get_invoice_total(docs,em),
                                   'opening' : self.get_opening(docs,em)['balance'],
                                   'get_debit_payments':debit_payments,
                                   'get_credit_payments': credit_payments,
                                   'debit_sum':debit_sum,
                                   'credit_sum':credit_sum,
                                   'posted_payments_sum' : posted_payments_sum
                                   })
            for day in list_days:
                docs.update({'date_to':day.date(),'date_from':day.date()})
                debit_paymentss, credit_paymentss,debit_summ, credit_summ, posted_payments_summ = self.get_payments(docs,em)
                dtt[em]['data'].append({'day':day.date(),
                                   'total': self.get_totals(docs,em),
                                   'invoices_total':self.get_invoice_total(docs,em),
                                   'opening' : self.get_opening(docs,em)['balance'],
                                   'debit_sum':debit_summ,
                                   'credit_sum':credit_summ,
                                   'posted_payments_sum' : posted_payments_summ
                                   })
        return dtt



    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        fill_data = self.get_emp(docs)
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'fill_data': fill_data,
	    'data':data,
        }
        return self.env['report'].render('sales_report.report_salesperson_summary', docargs)
