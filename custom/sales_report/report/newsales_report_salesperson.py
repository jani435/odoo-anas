# -*- coding: utf-8 -*-

import time
import datetime
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError
from datetime import date, timedelta
import collections


class newReportSalesperson(models.AbstractModel):
    _name = 'report.sales_report.report_newsalesperson'

    def get_data(self, date_from, date_to,vendor_id,salesperson_id):
        final_data ={}
        act_domain = ''
        invoice_obj = self.env['vendors.invoice']
        person_obj=self.env['hr.employee']
        vendors_obj=self.env['res.partner']
        act_domain = [('invoice_date','>=',date_from),('invoice_date',  '<=', date_to),('state', '=','posted')]
        result = invoice_obj.search(act_domain)
        if salesperson_id==False:
            persons_ids = person_obj.search([])
        else:
            persons_ids = person_obj.search([('id','=',salesperson_id[0])])
        inv_date=dates=invoice_dates=[]
        for line in result:
            dates.append(line.invoice_date)
        invoice_dates=collections.OrderedDict(zip(dates, dates)).values()
        inv_date=sorted(invoice_dates)
        for person in persons_ids:
            final_data[person] = final_data.get(person,{'data':[],'amount_total': 0.0, 'total_taxed_amount': 0.0, 'credit_custody': 0.0, 'cash_custody': 0.0,
                         'commision_total_amount': 0.0, 'credit_sales': 0.0, 'main_customer_total_untaxed': 0.0})
            tot_amount_total=tot_total_taxed_amount=tot_credit_custody=tot_credit_sales=tot_main_customer_total_untaxed=tot_cash_custody=tot_commision_total_amount=0.0
            for date in inv_date:
            #for target_vendor in vendors_ids:('vendor_id', '=', target_vendor.id),
                target_date=False
                inv_cards=amount_total=total_taxed_amount=credit_custody=commision_total_amount=credit_sales=main_customer_total_untaxed=number_of_cards=cash_custody=0.0
                if vendor_id ==False:
                    invoices=invoice_obj.search([('invoice_date','=',date),('state', '=','posted'),('sales_man_id', '=',person.id)])
                else:
                    invoices=invoice_obj.search([('vendor_id', '=', vendor_id[0]),('invoice_date','=',date),('state', '=','posted'),('sales_man_id', '=',person.id)])
                for invoice in invoices:
                    customer_inv=self.env['account.invoice.line'].search(
                    [('vendor_invoice_id.name','=',invoice.name),('partner_id','=',person.linked_customer.id)])
                    for inv in customer_inv:
                        total_taxed_amount+=inv.tax_amount
                    target_date=invoice.invoice_date
                    amount_total+=invoice.amount_total
                    #total_taxed_amount+=invoice.main_customer_total_amount-invoice.main_customer_total_untaxed
                    credit_custody+=invoice.credit_custody
                    cash_custody+=invoice.cash_custody
                    commision_total_amount+=invoice.commision_total_amount
                    credit_sales+=invoice.credit_sales
                    main_customer_total_untaxed+=invoice.main_customer_total_untaxed
                    for custody in invoice.custody_lines:
                        if custody.is_complex :
                            inv_cards = (custody.cash_amount + custody.credit_amount)/ 51.0                            
                    number_of_cards += int(inv_cards)
                tot_amount_total+=amount_total
                tot_total_taxed_amount+=total_taxed_amount
                tot_credit_custody+=credit_custody
                tot_cash_custody+=cash_custody
                tot_commision_total_amount+=commision_total_amount
                tot_credit_sales+=credit_sales
                tot_main_customer_total_untaxed+=main_customer_total_untaxed
                if target_date:
                    final_data[person]['data'].append({
                            'date': target_date,
                               'amount_total':amount_total,
                               'total_taxed_amount': total_taxed_amount,
                               'credit_custody':credit_custody,
                               'cash_custody':cash_custody,
                               'commision_total_amount':commision_total_amount,
                               'credit_sales':credit_sales,
                               'number_of_cards':number_of_cards,
                               'main_customer_total_untaxed':main_customer_total_untaxed,
                               
                               })
            final_data[person]['amount_total']=tot_amount_total
            final_data[person]['total_taxed_amount']=tot_total_taxed_amount
            final_data[person]['credit_custody']=tot_credit_custody
            final_data[person]['cash_custody']=tot_cash_custody
            final_data[person]['commision_total_amount']=tot_commision_total_amount
            final_data[person]['credit_sales']=tot_credit_sales
            final_data[person]['main_customer_total_untaxed']=tot_main_customer_total_untaxed
        return final_data

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        vendor_id = data['form'].get('vendor_id')
        salesperson_id = data['form'].get('salesperson_id')
        date_from=data['form'].get('date_from')
        date_to=data['form'].get('date_to')
        rm_act=self.with_context(data['form'].get('used_context',{}))
        data_res = rm_act.get_data(date_from, date_to,vendor_id,salesperson_id)
        docargs = {
            'doc_ids': docids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'final_data': data_res,
        }
        return self.env['report'].render('sales_report.report_newsalesperson', docargs)

