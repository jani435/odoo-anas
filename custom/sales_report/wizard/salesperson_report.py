# -*- coding: utf-8 -*-

from odoo import api, fields, models
from datetime import datetime

class SalespersonWizard(models.TransientModel):
    _name = "salesperson.wizard"
    _description = "Salesperson wizard"
    
    salesperson_ids = fields.Many2many('hr.employee', 'for_cust_inv_rep', string='Salepersons22',domain=[('is_market_employee', '=', True)])
    salesperson_id = fields.Many2one('hr.employee', string='Salesperson11',domain=[('is_market_employee', '=', True)])
    vendor_id = fields.Many2one('res.partner', string='Vendor Name', domain=['&',('supplier', '=', True),('is_market', '=', True)])
    date_from = fields.Date(string='Start Date',default=fields.Date.context_today)
    date_to = fields.Date(string='End Date',default=fields.Date.context_today)

    @api.multi
    def check_report(self, context=None):
        data = {}
        data['form'] = self.read(['salesperson_id', 'vendor_id', 'date_from', 'date_to'])[0]

        if context and 'detailed' in context and context['detailed']:
            return self._print_detailed_report(data)
	else:

		if data['form']['salesperson_id'] == False:
		    data = {
		        'ids': self.ids,
		        'model': 'vendors.invoice',
		        'form': self.read(['date_from', 'date_to','vendor_id'])[0]
		    }
		    return self.env['report'].get_action(self, 'sales_report.report_salesperson_all',data=data)
       
		else:
        		return self._print_report(data)

    def _print_report(self, data): 
        data['form'].update(self.read(['salesperson_id','vendor_id', 'date_from', 'date_to'])[0])
        return self.env['report'].get_action(self, 'sales_report.report_salesperson', data=data)
    
    def _print_detailed_report(self, data): 
        data['form'].update(self.read(['salesperson_id', 'date_from', 'date_to'])[0])
        return self.env['report'].get_action(self, 'sales_report.report_salesperson_detailed', data=data)
