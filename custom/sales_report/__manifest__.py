# -*- coding: utf-8 -*-
{
    'name' : 'Custom Sales Report',
    'version' : '1.0',
    'summary': 'Custom report for the company sales',
    'sequence': 16,
    'category': 'Sales',
    'description': """
Custom Sales Report
=====================================
Sales report that will generate a pdf report for a sales person for specific time duration.
    """,
    'category': 'Accounting',
    'website': 'http://www.surekhatech.com/blog',
    'images' : [],
    #'depends' : ['base_setup', 'report', 'sale'],
    'depends' : ['base_setup', 'report', 'sale', 'vegetables'],
    'data': [
	   'views/res_company_view.xml',
	#    'views/report_header_custom.xml',
       'security/groups.xml',
       'security/ir.model.access.csv',
       'wizard/salesperson_report_view.xml',
       'wizard/newsalesperson_report_view.xml',
       'wizard/salesperson_summary_report_view.xml',
       'views/report_salesperson.xml',
       'views/report_newsalesperson.xml',
       'views/report_salesperson_detailed.xml',
       'views/report_salesperson_summary.xml',
       #'views/report_salesperson_all.xml',


       'wizard/detailed_salesman_report.xml',
       'report/detailed_salesman_report.xml',
       'report/detailed_salesman_report_all.xml',



       'views/sales_report_report.xml',
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
