# -*- encoding: utf-8 -*-
from odoo import models, fields


class BaseConfiguration(models.TransientModel):
    _inherit = 'base.config.settings'

    default_uom_categ = fields.Many2one('product.uom.categ', string="Unit Category",
                                        related='company_id.default_uom_categ')


class ResCompany(models.Model):
    _inherit = "res.company"

    default_uom_categ = fields.Many2one('product.uom.categ', string="Unit Category")
