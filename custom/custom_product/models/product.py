# -*- coding: utf-8 -*-

import re
from odoo import api, fields, models, tools, _
from odoo.osv import expression


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    arabic_name = fields.Char('Arabic Name')

    type = fields.Selection(default='product')

    uom_categ_id = fields.Many2one('product.uom.categ', string="Unit Category",
                                   default=lambda self: self.env.user.company_id.default_uom_categ.id)

    uom_id = fields.Many2one(required=False, default=False)

    uom_po_id = fields.Many2one(required=False, default=False)

    references_ids = fields.One2many('product.references','product_ref_id','Product References')

    is_market_product = fields.Boolean(string='Agricultural Market', default=True)
    is_vegetable_product = fields.Boolean(string='Is Vegetable')

    _sql_constraints = [
        ('unique_name', 'unique(name, company_id)', 'Product name must be unique')
    ]
    #function to calculate internal reference from 
    #category code and padding sequentially
    @api.onchange('categ_id')
    def _on_change_categ_id(self):
        final_code=0
        if self.categ_id:
            categ_obj = self.env['product.category'].search([('id', '=', self.categ_id.id)])
            code_pading =int(categ_obj.padding)
            seq=str(categ_obj.sequence).zfill(code_pading)   
            final_code  = int(str(categ_obj.code) + str(seq))
        self.default_code=final_code

    @api.model
    def create(self, vals):
        if 'categ_id' in vals:
            categ_obj = self.env['product.category'].search([('id', '=', vals['categ_id'])])
            new_seq =int(categ_obj.sequence) + 1
            categ_obj.write({'sequence':new_seq})
        return super(ProductTemplate, self).create(vals)

    @api.onchange('uom_categ_id')
    def _on_change_uom_categ_id(self):
        if self.uom_categ_id:
            domain = {'uom_id': [('category_id', '=', self.uom_categ_id.id)],
                      'uom_po_id': [('category_id', '=', self.uom_categ_id.id)]}
        else:
            domain = {'uom_id': False, 'uom_po_id': []}
        return {'domain': domain}


class ProductReferences(models.Model):
    _name = 'product.references'

    product_ref_id = fields.Many2one('References ID')
    reference = fields.Char('Reference')


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.onchange('uom_categ_id')
    def _on_change_uom_categ_id(self):
        if self.uom_categ_id:
            domain = {'uom_id': [('category_id', '=', self.uom_categ_id.id)],
                      'uom_po_id': [('category_id', '=', self.uom_categ_id.id)]}
        else:
            domain = {'uom_id': False, 'uom_po_id': []}
        return {'domain': domain}

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', '|', ('name', operator, name), ('default_code', operator, name),
                      ('arabic_name', operator, name)]
        products = self.search(domain + args, limit=limit)
        return products.name_get()

    @api.multi
    def name_get(self):
        # TDE: this could be cleaned a bit I think

        def _name_get(d):
            name = d.get('name', '')
            code = self._context.get('display_default_code', True) and d.get('default_code', False) or False
            # if code:
            # name = '[%s] %s' % (code, name)
            return (d['id'], name)

        partner_id = self._context.get('partner_id')
        if partner_id:
            partner_ids = [partner_id, self.env['res.partner'].browse(partner_id).commercial_partner_id.id]
        else:
            partner_ids = []

        # all user don't have access to seller and partner
        # check access and use superuser
        self.check_access_rights("read")
        self.check_access_rule("read")

        result = []
        for product in self.sudo():
            # display only the attributes with multiple possible values on the template
            variable_attributes = product.attribute_line_ids.filtered(lambda l: len(l.value_ids) > 1).mapped(
                'attribute_id')
            variant = product.attribute_value_ids._variant_name(variable_attributes)

            name = variant and "%s (%s)" % (product.name, variant) or product.name
            sellers = []
            if partner_ids:
                sellers = [x for x in product.seller_ids if (x.name.id in partner_ids) and (x.product_id == product)]
                if not sellers:
                    sellers = [x for x in product.seller_ids if (x.name.id in partner_ids) and not x.product_id]
            if sellers:
                for s in sellers:
                    seller_variant = s.product_name and (
                        variant and "%s (%s)" % (s.product_name, variant) or s.product_name
                    ) or False
                    mydict = {
                        'id': product.id,
                        'name': seller_variant or name,
                        'default_code': s.product_code or product.default_code,
                    }
                    temp = _name_get(mydict)
                    if temp not in result:
                        result.append(temp)
            else:
                mydict = {
                    'id': product.id,
                    'name': name,
                    'default_code': product.default_code,
                }
                result.append(_name_get(mydict))
        return result


class ProductCategory(models.Model):
    _inherit = "product.category"

    padding = fields.Char('Padding')
    code = fields.Char('Code')
    sequence = fields.Char('Sequence')

    @api.multi
    def name_get(self):
        # def get_names(cat):
        #     """ Return the list [cat.name, cat.parent_id.name, ...] """
        #     res = []
        #     while cat:
        #         res.append(cat.name)
        #         cat = cat.parent_id
        #     return res
        # return [(cat.id, " / ".join(reversed(get_names(cat)))) for cat in self]

        result = []
        for record in self:
            result.append((record.id, record.name))
        return result

    _defaults={
     'sequence':1,
     'code':1,
     'padding':3

    }
