# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Custom Product",
    'version': '1.0',
    'summary': '',
    'sequence': 3,
    'description': '',
    'category': 'Custom',
    'website': '',
    'depends': ['base', 'product', 'account_accountant','company_types'],

    'data': [
        'security/product_security.xml',
        'security/ir.model.access.csv',
        'wizard/set_vat_for_product_view.xml',
        'views/base_config.xml',
        'views/product_template.xml',
        'views/res_user_view.xml',
        'views/cost_center_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
