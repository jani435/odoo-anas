# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError

class Set_Vat_For_Products_Wizard(models.TransientModel):
    _name = "set_vat_for_products.wizard"
    _description = "Set VAT For Products Wizard"

    set_by = fields.Selection([
        ('all', 'All'),
        ('group', 'Group'),('product', 'Product')], 'Update Type', default='group', required=True)

    tax_purchase_id = fields.Many2one('account.tax', string='Purchase Tax Name',
                                      domain=[('type_tax_use', '=', 'purchase')])
    tax_sales_id = fields.Many2one('account.tax', string='Sales Tax Name',
                                   domain=[('type_tax_use', '=', 'sale')])
    category_id = fields.Many2one('product.category', string='Group')
    product_id = fields.Many2one('product.product', string='Product')


    @api.multi
    def set(self):

        if self.set_by == 'group':
            if self.category_id:
                products = self.env['product.product'].search([('categ_id', '=', self.category_id.id)])
                for product in products:
                    if self.tax_sales_id:
                        product.taxes_id = {}
                        product.taxes_id =[(4, self.tax_sales_id.id)]
                    if self.tax_purchase_id:
                        product.supplier_taxes_id = {}
                        product.supplier_taxes_id = [(4, self.tax_purchase_id.id)]
            else:
                raise UserError(_('Select one Group'))
        else :
            if self.set_by == 'product':
                if self.product_id:
                    products = self.env['product.product'].search([('id', '=', self.product_id.id)])
                    for product in products:
                        if self.tax_sales_id:
                            product.taxes_id = {}
                            product.taxes_id = [(4, self.tax_sales_id.id)]
                        if self.tax_purchase_id:
                            product.supplier_taxes_id = {}
                            product.supplier_taxes_id = [(4, self.tax_purchase_id.id)]
                else:
                    raise UserError(_('Select One Product'))

            else :
                if self.set_by=='all':
                    products = self.env['product.product'].search([])
                    for product in products:
                        if self.tax_sales_id:
                            product.taxes_id = {}
                            product.taxes_id = [(4, self.tax_sales_id.id)]
                        if self.tax_purchase_id:
                            product.supplier_taxes_id = {}
                            product.supplier_taxes_id = [(4, self.tax_purchase_id.id)]

