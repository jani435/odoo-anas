# -*- encoding: utf-8 -*-
{
    'name': "Tree",
    'version': '10.0.0',
    'author': 'IES Team',
    "depends" : ['web'],
    'data': [
             'views/listview_templates.xml',
             ],
    "images": ["static/description/screen1.png"],
    'license': 'LGPL-3',
    'qweb': [
           'static/src/xml/base.xml',
            ],  
    
    'installable': True,
    'application'   : True,
    'auto_install'  : False,
}
