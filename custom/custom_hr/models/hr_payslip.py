
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import random
import time
from datetime import datetime, date, time, timedelta
import dateutil.parser




class HrPayslipWorkedDays(models.Model):
    _inherit = 'hr.payslip.worked_days'

    days = fields.Char(string='Days', readonly=1)
    hours = fields.Char(string='Hours', readonly=1)

    import_from_attendance = fields.Boolean(
        string='Imported From Attendance',
        default=False,
    )

class HrPayslip(models.Model):
    _inherit = "hr.payslip"

    @api.multi
    def button_import_attendance(self):
        for payslip in self:
            payslip._import_attendance()

    @api.multi
    def _import_attendance(self):

        
        self.ensure_one()
        wd_obj = self.env["hr.payslip.worked_days"]
        att_obj = self.env["hr.attendance"]

        date_from = self.date_from
        date_to = self.date_to

        criteria1 = [
            ("payslip_id", "=", self.id),
            ("import_from_attendance", "=", True),
        ]
        wd_obj.search(criteria1).unlink()

        res = {
            "name": _("Total Attendance"),
            "code": "ATTN",
            "number_of_days": 0.0,
            "number_of_hours": 0.0,
            "contract_id": self.contract_id.id,
            "import_from_attendance": True,
            "payslip_id": self.id,
        }

        criteria3 = [
            ("check_in", ">=", date_from),
            ("check_out", "<=", date_to),
            ("employee_id", "=", self.employee_id.id),
        ]

        for day in att_obj.search(criteria3):
            res["number_of_days"] += 1
            res["number_of_hours"] += day.worked_hours

        
        
        
        num_of_day = 0
        num_of_hours = 0
        ss = self.employee_id.calendar_id.attendance_ids
        for s in ss:
            day_name = dict(s._fields['dayofweek'].selection).get(s.dayofweek)
            _date_from = dateutil.parser.parse(date_from).date()
            _date_to = dateutil.parser.parse(date_to).date()
            while _date_from <= _date_to:
                if day_name == datetime.strptime(str(_date_from), DEFAULT_SERVER_DATE_FORMAT).strftime("%A"):
                    num_of_day += 1
                    num_of_hours += s.hour_to - s.hour_from
                _date_from = _date_from + timedelta(days=1)
        
        res['days'] = num_of_day
        res['hours'] = num_of_hours

        wd_obj.create(res)
        
    @api.multi
    def _check(self):
        return 5000.0

    @api.one
    def compute_total_paid(self):
        """This compute the total paid amount of Loan.
            """
        total = 0.0
        for line in self.loan_ids:
            if line.paid:
                total += line.amount
        self.total_paid = total

    loan_ids = fields.One2many('hr.loan.line', 'payslip_id', string="Loans")
    total_paid = fields.Float(string="Total Loan Amount", compute='compute_total_paid')

    @api.multi
    def get_loan(self):
        """This gives the installment lines of an employee where the state is not in paid.
            """
        loan_ids = self.env['hr.loan.line'].search([('employee_id', '=', self.employee_id.id),
            ('date', '>=', self.date_from),('date', '<=', self.date_to),
            ('paid', '=', False)])
        for loan in loan_ids:
            if loan.loan_id.state == 'approve':
                loan.write({'payslip_id': self.ids[0]})
        return True
