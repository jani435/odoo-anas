import time
import datetime
from dateutil.relativedelta import relativedelta

import odoo
from odoo import SUPERUSER_ID
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class company_inhert(models.Model):
    _inherit = 'res.company'

    id_days = fields.Integer(string='Identification Days', required=True, default=30)
    passport_days = fields.Integer(string='Passport Days', required=True, default=180)
    email_rec = fields.Char(string='E-Mail', required=True)

class HRConfigSettings(models.TransientModel):
    _name = 'hr.config.settings'
    _inherit = 'res.config.settings'

    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id)
    id_days = fields.Integer(string='Identification Days', required=True, default=lambda self: self.env.user.company_id.id_days)
    passport_days = fields.Integer(string='Passport Days', required=True, default=lambda self: self.env.user.company_id.passport_days)
    email_rec = fields.Char(string='E-Mail', required=True, default=lambda self: self.env.user.company_id.email_rec)

    

    @api.model
    def create(self, vals):
        
        res = super(HRConfigSettings, self).create(vals)
        res.company_id.write({'id_days':vals['id_days'],'passport_days':vals['passport_days'],'email_rec':vals['email_rec']})

        return res