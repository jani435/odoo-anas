import time
import datetime
from dateutil.relativedelta import relativedelta

import odoo
from odoo import SUPERUSER_ID
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo import api, fields, models, _
from odoo.exceptions import UserError

# class HrSalaryRule(models.Model):
#     _inherit = 'hr.rule.input'

#     in_contract = fields.Boolean(string='In Contract', default=False)


class HrContract(models.Model):
    _inherit = 'hr.contract'

    
    
    housing_allowance = fields.Float(string='Housing Allowance', required=True, default=0.0)
    cost_of_living_allowance = fields.Float(string='Cost Of Living Allowance', required=True, default=0.0)
    telephone_allowance = fields.Float(string='Telephone Allowance', required=True, default=0.0)
    Transfer_allowance = fields.Float(string='Transfer Allowance', required=True, default=0.0)
    nature_of_work_allowance = fields.Float(string='Nature Of Work Allowance', required=True, default=0.0)
    supervision_allowance = fields.Float(string='Supervision Allowance', required=True, default=0.0)
    externally_paid_allowance = fields.Float(string='Externally paid Allowance', required=True, default=0.0)
    other_allowance = fields.Float(string='Other Allowance', required=True, default=0.0)


    hr_contract_code = fields.Many2one('hr.contract.settings', 'code')
