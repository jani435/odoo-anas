# -*- coding: utf-8 -*-

from odoo import models, fields, api,exceptions,_
from datetime import datetime
from dateutil.relativedelta import relativedelta
from lxml import etree


class HrLoan(models.Model):
    _name = 'hr.loan'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Loan Request"

    @api.one
    def _compute_loan_amount(self):
        total_paid = 0.0
        for loan in self:
            for line in loan.loan_lines:
                if line.paid:
                    total_paid += line.amount
            balance_amount = loan.loan_amount - total_paid
            self.total_amount = loan.loan_amount
            self.balance_amount = balance_amount
            self.total_paid_amount = total_paid

    @api.model
    def get_defualt_employee(self):
        employee_obj = self.env['hr.employee']
        emp_id = False
        if not self.env.user.has_group('hr.group_hr_user') and not self.env.user.has_group('hr.group_hr_manager'):
            emp_id = employee_obj.search([('user_id.id', '=', self.env.user.id)])
        
        return emp_id and emp_id[0].id or False

    name = fields.Char(string="Loan Name", default="/", readonly=True)
    date = fields.Date(string="Loan received date", default=fields.Date.today())
    employee_id = fields.Many2one('hr.employee', string="Employee", required=True, default=get_defualt_employee)
    installment_type = fields.Selection([('end_date','With Installments End Date'),
                                        ('amount','With Installments Amount')],required=True,
                                        string="Type Of Installments", default='amount')
    installment = fields.Float(string="monthly installment")
    payment_date = fields.Date(string="Payment Start Date")
    payment_end_date = fields.Date(string="Payment End Date")
    loan_lines = fields.One2many('hr.loan.line', 'loan_id', string="Loan Line", index=True)
    emp_account_id = fields.Many2one('account.account', string="Loan Account")
    treasury_account_id = fields.Many2one('account.account', string="Treasury Account")
    journal_id = fields.Many2one('account.journal', string="Journal")
    company_id = fields.Many2one('res.company', 'Company',default=lambda self: self.env.user.company_id)
    loan_amount = fields.Float(string="Loan Amount", required=True)
    total_amount = fields.Float(string="Total Amount", readonly=True, compute='_compute_loan_amount')
    balance_amount = fields.Float(string="Balance Amount", compute='_compute_loan_amount')
    total_paid_amount = fields.Float(string="Total Paid Amount", compute='_compute_loan_amount')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting_approval_1', 'Submitted'),
        ('waiting_approval_2', 'Waiting Approval'),
        ('approve', 'Approved'),
        ('refuse', 'Refused'),
        ('cancel', 'Canceled'),
    ], string="State", default='draft', track_visibility='onchange', copy=False, )

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        employee_obj = self.env['hr.employee']
        if not self.env.user.has_group('hr.group_hr_user') and not self.env.user.has_group('hr.group_hr_manager'):
            emp_id = employee_obj.search([('user_id.id', '=', self.env.user.id)])
            args += [('employee_id','in',[emp_id.id])]
        return super(HrLoan, self).search(args=args, offset=offset, limit=limit, order=order, count=count)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(HrLoan, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        employee_obj = self.env['hr.employee']
        if view_type =='form':
            if not self.env.user.has_group('hr.group_hr_user') and not self.env.user.has_group('hr.group_hr_manager'):
                emp_id = employee_obj.search([('user_id.id', '=', self.env.user.id)])
                if emp_id:
                    doc = etree.XML(res['arch'])
                    for node in doc.xpath("//field[@name='employee_id']"):
                        domain = [('id', '=', emp_id[0].id)]
                        node.set('domain', str(domain))
                    res['arch'] = etree.tostring(doc)
                else:
                    doc = etree.XML(res['arch'])
                    for node in doc.xpath("//field[@name='employee_id']"):
                        domain = [('id', '=', -1)]
                        node.set('domain', str(domain))
                    res['arch'] = etree.tostring(doc)



        return res

    @api.constrains('payment_date', 'payment_end_date')
    def _check_date(self):
        for record in self:
            if record.installment_type == 'end_date':
                if record.payment_end_date < record.payment_date:
                    raise exceptions.ValidationError(_('"Payment End Date" cannot be earlier than "Payment Start Date".'))

    @api.constrains('loan_amount')
    def _check_amount(self):
        for record in self:
            if sum(line.amount for line in record.loan_lines) > record.loan_amount:
                raise exceptions.ValidationError(_('The installments amount can\'t be grate than Loan amount.'))

    @api.model
    def create(self, values):
        # loan_count = self.env['hr.loan'].search_count([('employee_id', '=', values['employee_id']), ('state', '=', 'approve'),
        #                                                ('balance_amount', '!=', 0)])
        # if loan_count:
        #     raise exceptions.ValidationError('Error! The employee has already a pending installment')
        # else:
        values['name'] = self.env['ir.sequence'].get('hr.loan.seq') or ' '
        res = super(HrLoan, self).create(values)
        return res

    @api.multi
    def action_refuse(self):
        return self.write({'state': 'refuse'})

    @api.multi
    def action_submit(self):
        self.write({'state': 'waiting_approval_1'})

    @api.multi
    def action_cancel(self):
        self.write({'state': 'cancel'})

    @api.multi
    def action_approve(self):
        self.write({'state': 'approve'})

    @api.multi
    def compute_installment(self):
        """
        This automatically create the installment the employee need to pay to
        company based on payment start date and the no of installments.
        """
        loan_line=self.env['hr.loan.line']
        for loan in self:
            if loan.loan_lines:
                if any(loan.paid for loan in loan.loan_lines):
                    raise exceptions.ValidationError(_('Error! This loan have alrady paid installments!'))
                loan.loan_lines.unlink()
            date_start = datetime.strptime(loan.payment_date, '%Y-%m-%d')
            if loan.installment_type == 'amount':
                amount = loan.loan_amount
                while amount > loan.installment:
                    amount-=loan.installment
                    loan_line.create({
                    'date': date_start,
                    'amount': loan.installment,
                    'employee_id': loan.employee_id.id,
                    'loan_id': loan.id})
                    date_start = date_start + relativedelta(months=1)
                if amount <= loan.installment:
                    loan_line.create({
                    'date': date_start,
                    'amount': amount,
                    'employee_id': loan.employee_id.id,
                    'loan_id': loan.id})
            else:
                date_end = datetime.strptime(loan.payment_end_date, '%Y-%m-%d')
                date_end_temp=date_end
                months=(date_end.year - date_start.year) * 12 + date_end.month - date_start.month
                installment=loan.loan_amount/(months+1)
                for month in range(1,months+2):
                    if date_end_temp.month == date_start.month and date_end_temp.year == date_start.year:
                        loan_line.create({
                        'date': date_start,
                        'amount': installment,
                        'employee_id': loan.employee_id.id,
                        'loan_id': loan.id})
                        break
                    loan_line.create({
                    'date': date_end_temp,
                    'amount': installment,
                    'employee_id': loan.employee_id.id,
                    'loan_id': loan.id})
                    date_end_temp = date_end + relativedelta(months =-month)
        return True


    @api.multi
    def recompute_installment(self,diff,change_line,loan):
        residual=0.0
        count=0
        for line in loan.loan_lines:
            if not line.paid and line != change_line:
                residual+=line.amount
                count+=1
        lines_amount=residual+diff
        amount=lines_amount/count
        for line in loan.loan_lines:
            if not line.paid and line.id != change_line.id:
                line.amount=amount
        return True



class InstallmentLine(models.Model):
    _name = "hr.loan.line"
    _description = "Installment Line"
    _order = "date asc"

    date = fields.Date(string="Payment Date", required=True)
    employee_id = fields.Many2one('hr.employee', string="Employee")
    amount = fields.Float(string="Amount", required=True)
    paid = fields.Boolean(string="Paid")
    loan_id = fields.Many2one('hr.loan', string="Loan Ref.")
    payslip_id = fields.Many2one('hr.payslip', string="Payslip Ref.")

    @api.constrains('amount')
    def _check_amount(self):
        for record in self:
            if sum(line.amount for line in record.loan_id.loan_lines) > record.loan_id.loan_amount:
                raise exceptions.ValidationError(_('The installments amount can\'t be grate than Loan amount.'))

    @api.multi
    def write(self, vals):
        for line in self:
            line_amount=line.amount
            res=super(InstallmentLine, self).write(vals)
            if 'amount' in vals:
                if line.payslip_id and (line.paid or ('paid' in vals and vals['paid'])):
                    diff=line_amount - vals['amount']
                    line.loan_id.recompute_installment(diff,line,line.loan_id)
                elif line.payslip_id and not (line.paid or ('paid' in vals and vals['paid'])):
                    raise exceptions.ValidationError(_('You can\'t update non paid installment that associated with payslip.'))
        return res

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    @api.one
    def _compute_employee_loans(self):
        """This compute the loan amount and total loans count of an employee.
            """
        self.loan_count = self.env['hr.loan'].search_count([('employee_id', '=', self.id)])

    loan_count = fields.Integer(string="Loan Count", compute='_compute_employee_loans')


