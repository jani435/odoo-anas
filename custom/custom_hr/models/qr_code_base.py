# -*- coding: utf-8 -*-
import qrcode
import base64
import cStringIO


def generate_qr_code(url):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=20,
            border=4,
            )

        qr.add_data(url)
        qr.make(fit=True)
        img = qr.make_image()
        buffer = cStringIO.StringIO()
        img.save(buffer, format="PNG")
        img_str = base64.b64encode(buffer.getvalue())
        
        return img_str
        
