# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from odoo import models, fields, _
from odoo import api
from odoo.exceptions import ValidationError, UserError, Warning
#from openerp.addons.custom_hr.models.qr_code_base import generate_qr_code
from odoo.http import request

GENDER_SELECTION = [('male', 'Male'),
                    ('female', 'Female'),
                    ('other', 'Other')]


class HrEmployeeContractName(models.Model):
    """This class is to add emergency contact table"""
    _name = 'hr.emergency.contact'
    _description = 'HR Emergency Contact'

    number = fields.Char(string='Number', help='Contact Number')
    relation = fields.Char(string='Contact', help='Relation with employee')
    employee_obj = fields.Many2one('hr.employee', invisible=1)


class HrEmployeeFamilyInfo(models.Model):
    """Table for keep employee family information"""
    _name = 'hr.employee.family'
    _description = 'HR Employee Family'

    member_name = fields.Char(string='Name', related='employee_ref.name', store=True)
    employee_ref = fields.Many2one(string="Is Employee",
                                   help='If family member currently is an employee of same company, '
                                        'then please tick this field',
                                   comodel_name='hr.employee')
    employee_id = fields.Many2one(string="Employee", help='Select corresponding Employee', comodel_name='hr.employee',
                                  invisible=1)
    relation = fields.Selection([('father', 'Father'),
                                 ('mother', 'Mother'),
                                 ('daughter', 'Daughter'),
                                 ('son', 'Son'),
                                 ('wife', 'Wife')], string='Relationship', help='Relation with employee')
    member_contact = fields.Char(string='Contact No', related='employee_ref.personal_mobile', store=True)


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    

    def mail_reminder(self):
        now = datetime.now() + timedelta(days=1)
        date_now = now.date()
        match = self.search([])
        for i in match:
            if i.id_expiry_date:
                exp_date = fields.Date.from_string(i.id_expiry_date) - timedelta(days=self.env.user.company_id.id_days)
                if date_now >= exp_date:
                    mail_content = "  Hello  " + i.name + ",<br>Your ID " + i.identification_id + " is going to expire on " + \
                                   str(i.id_expiry_date) + ". Please renew it before expiry date"
                    main_content = {
                        'subject': _('ID - %s Expired On %s') % (i.identification_id, i.id_expiry_date),
                        'author_id': self.env.user.partner_id.id,
                        'body_html': mail_content,
                        'email_to': self.env.user.company_id.email_rec or i.work_email,
                    }
                    self.env['mail.mail'].create(main_content).send()
        match1 = self.search([])
        for i in match1:
            if i.passport_expiry_date:
                exp_date1 = fields.Date.from_string(i.passport_expiry_date) - timedelta(days=self.env.user.company_id.passport_days)
                if date_now >= exp_date1:
                    mail_content = "  Hello  " + i.name + ",<br>Your Passport " + i.passport_id + " is going to expire on " + \
                                   str(i.passport_expiry_date) + ". Please renew it before expiry date"
                    main_content = {
                        'subject': _('Passport-%s Expired On %s') % (i.passport_id, i.passport_expiry_date),
                        'author_id': self.env.user.partner_id.id,
                        'body_html': mail_content,
                        'email_to': self.env.user.company_id.email_rec or i.work_email,
                    }
                    self.env['mail.mail'].create(main_content).send()
    personal_mobile = fields.Char(string='Mobile', related='address_home_id.mobile', store=True)
    english_name = fields.Char(string='English name', store=True)
    analytic_account_id = fields.Char(string='English name', store=True)
    emergency_contact = fields.One2many('hr.emergency.contact', 'employee_obj', string='Emergency Contact')
    joining_date = fields.Date(string='Joining Date')
    id_expiry_date = fields.Date(string='Expiry Date', help='Expiry date of Identification ID')
    passport_expiry_date = fields.Date(string='Expiry Date', help='Expiry date of Passport ID')
    id_attachment_id = fields.Many2many('ir.attachment', 'id_attachment_rel', 'id_ref', 'attach_ref',
                                        string="Attachment", help='You can attach the copy of your Id')
    passport_attachment_id = fields.Many2many('ir.attachment', 'passport_attachment_rel', 'passport_ref', 'attach_ref1',
                                              string="Attachment",
                                              help='You can attach the copy of Passport')
    fam_ids = fields.One2many('hr.employee.family', 'employee_id', string='Family', help='Family Information')
    employee_id = fields.Char(store=True, readonly=True)
    employee_qr_code = fields.Binary("QR Code")
    department_join_date = fields.Date('Department Join Date')
    company_join_date = fields.Date('Company Join Date')



    @api.model
    def create(self, vals):
        if self.identification_id:
            if len(vals['identification_id']) < 10 or len(vals['identification_id']) > 10:
                #raise UserError(_('Exists the discount limit'))
                raise ValidationError(_('The identification No. should be 10 digits'))

        sequ_id = self.env['ir.sequence'].next_by_code('hr.employee')
        vals['employee_id']= sequ_id 

        #res = super(HrEmployee, self).create(vals)
        vals['barcode']= sequ_id 

        # print res.department_id.name
        # base_url = res.name
        # base_url += '\n' + res.employee_id
        # if res.department_id:
        #     base_url += '\n' + res.department_id.name
        # if res.job_id:
        #     base_url += '\n' + res.job_id.name
        # if res.mobile_phone:
        #     base_url += '\n' + res.mobile_phone
        # if res.work_email:
        #     base_url += '\n' + res.work_email
        # if res.company_id:
        #     base_url += '\n' + res.company_id.name

        # res.employee_qr_code = generate_qr_code(base_url)
        res = super(HrEmployee, self).create(vals)

        return res


    @api.multi
    def write(self, values):
        #print self.employee_id
        if not self.employee_id:
            sequ_id = self.env['ir.sequence'].next_by_code('hr.employee')
            values['employee_id'] = sequ_id 
        values['barcode']= self.employee_id

        
        # super(HrEmployee, self).write(values)
        # base_url = self.name
        # base_url += '\n' + self.employee_id
        # if self.department_id:
        #     base_url += '\n' + self.department_id.name
        # if self.job_id:
        #     base_url += '\n' + self.job_id.name
        # if self.mobile_phone:
        #     base_url += '\n' + self.mobile_phone
        # if self.work_email:
        #     base_url += '\n' + self.work_email
        # if self.company_id:
        #     base_url += '\n' + self.company_id.name

        # values['employee_qr_code'] = generate_qr_code(base_url)
        
        
        return super(HrEmployee, self).write(values)


