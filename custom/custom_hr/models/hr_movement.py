# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class HrDepartmentMove(models.Model):

    _name = "hr.department.move"
    _description = "Department Move"
    _inherit = ['mail.thread']

    current_department_id = fields.Many2one('hr.department', string='Current Department',compute='_compute_current_department_id',store=True)
    new_department_id = fields.Many2one('hr.department', string='New Department',domain="[('id', '!=', current_department_id)]")
    date = fields.Date(readonly=True, states={'draft': [('readonly', False)]}, default=fields.Date.context_today, string="Execute Date")
    employee_id = fields.Many2one('hr.employee', string="Employee", required=True, readonly=True, states={'draft': [('readonly', False)]})
    description = fields.Text()
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
        ('refused', 'Refused')
        ], string='Status', readonly=True,default='draft',
        help="Status of Department Move.")
    num_of_days=fields.Integer("Current Department days",compute='_compute_number_of_days',store=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)


    @api.depends('employee_id')
    def _compute_current_department_id(self):
        for record in self:
        	record.current_department_id=record.employee_id.department_id.id
            

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = "%s / %s" % (record.employee_id.name, record.date)
            result.append((record.id, name))
        return result

    @api.depends('employee_id', 'date')
    def _compute_number_of_days(self):
    	if self.employee_id:
	    	date_from= self.employee_id.department_join_date or self.employee_id.company_join_date
	    	if not date_from:
	    	    raise ValidationError(_('Please Enter Employee Company Join Date.'))
	        from_dt = fields.Date.from_string(date_from)
	        to_dt = fields.Date.from_string(self.date)
	        time_delta = to_dt - from_dt
	        self.num_of_days=time_delta.days

    @api.multi
    def refuse(self):
        for record in self:
            record.write({'state': 'refused'})
        return True
    @api.multi
    def set_to_draft(self):
        for record in self:
            record.write({'state': 'draft'})
        return True
    @api.multi
    def done(self):
        for record in self:
            if not record.date:
                raise ValidationError(_('You must Enter Execute Date.'))
            record.write({'state': 'done'})
            record.employee_id.write({'department_join_date': record.date,'department_id':record.new_department_id.id})
        return True

class Employee(models.Model):

    _inherit = "hr.employee"

    dept_move_count = fields.Integer('Number of Department move', compute='_compute_dept_move')

    def _compute_dept_move(self):
        dept_move = self.env['hr.department.move'].sudo().read_group([('employee_id', 'in', self.ids)], ['employee_id'], ['employee_id'])
        result = dict((data['employee_id'][0], data['employee_id_count']) for data in dept_move)
        for employee in self:
            employee.dept_move_count = result.get(employee.id, 0)
