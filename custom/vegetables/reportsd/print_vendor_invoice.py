# -*- coding: utf-8 -*-

from odoo import api, models


class PrintVendorInvoice(models.AbstractModel):
    _name = 'report.vegetables.print_vendor_invoice_template'

    @api.model
    def render_html(self, docids, data=None):
        docs = self.env['vendors.invoice'].browse(docids)

        print '-*-**--*-*-*-*-**-*-*-*-*-*'
        invoices = []
        for _invoice_docs in docs:
            _invoice_line = []
            _custody_line = []
            _invoice_total = 0
            _custody_total = 0
            _tax_total = 0

            _invoice_data = []
            _invoice = []
            __page_num_total = 1
            _loop_last_paper = 0
            _page_num = 1

            for line in _invoice_docs.invoice_line:
                _invoice_line.append({
                    '_customer': line.customer_id.code,
                    '_purchase_price': line.purchase_price,
                    '_item': line.product_id.name,
                    '_quantity': line.product_qty,
                    '_price': line.unit_price,
                    'purchase_taxes': line.purchase_taxes,
                    '_total_price': round(line.purchase_price, 4) * round(line.product_qty, 4) + round(
                        line.purchase_taxes, 4)
                })
                _invoice_total = _invoice_total + (
                round(line.purchase_price, 4) * round(line.product_qty, 4) + round(line.purchase_taxes, 4))
                _tax_total = _tax_total + round(line.purchase_taxes, 4)

            ass = []
            for lines in _invoice_docs.custody_lines:
                ass.append({
                    'cus_name': lines.custody_name,
                    'amount': lines.cash_amount if lines.cash_amount > 0 else lines.credit_amount
                })
                _custody_total += float(lines.cash_amount) if float(lines.cash_amount) > 0 else float(
                    lines.credit_amount)

            print ass

            # if lines.cash_account:
            #     print lines.custody_name
            #     if lines.custody_name == 'ايجار':
            #         _cust_one = lines.cash_amount
            #         print _cust_one
            #     if lines.custody_name == 'حمالة':
            #         _cust_two = lines.cash_amount
            #     if (lines.custody_name == 'موقف'):
            #         _cust_three = lines.cash_amount
            #     if (lines.custody_name == 'اخرى'):
            #         _cust_four = lines.cash_amount
            #     if (lines.custody_name == 'ايجار 2'):
            #         _cust_five = lines.cash_amount
            #     if (lines.custody_name == 'كرت خروج'):
            #         _cust_six = lines.cash_amount
            #
            # if lines.credit_account:
            #     if(lines.custody_name == 'ايجار'):
            #         _cust_one = lines.credit_amount
            #     if (lines.custody_name == 'حمالة'):
            #         _cust_two = lines.credit_amount
            #     if (lines.custody_name == 'موقف'):
            #         _cust_three = lines.credit_amount
            #     if (lines.custody_name == 'اخرى'):
            #         _cust_four = lines.credit_amount
            #     if (lines.custody_name == 'ايجار 2'):
            #         _cust_five = lines.credit_amount
            #     if (lines.custody_name == 'كرت خروج'):
            #         _cust_six = lines.credit_amount



            if (len(_invoice_line) == 12):
                _loop = 11
            elif (len(_invoice_line) < 12):
                _loop = 7
            elif (len(_invoice_line) > 12):
                _loop = 12
                if (len(_invoice_line) % 12 == 0):
                    _loop_last_paper = 11
                else:
                    _loop_last_paper = 2

            _page_num_total = len(_invoice_line) / _loop
            if (len(_invoice_line) % _loop != 0):
                _page_num_total += 1

            count = 0
            for data in range(len(_invoice_line)):
                count += 1
                if count <= _loop:
                    _invoice.append(_invoice_line[data])
                else:
                    _invoice_data.append({
                        '_last_page': '-1',
                        '_invoice': _invoice,
                        '_page_num': _page_num
                    })
                    _page_num = _page_num + 1
                    __page_num_total += 1

                    if (__page_num_total == _page_num_total):
                        _loop = _loop_last_paper
                    _invoice = []
                    _invoice.append(_invoice_line[data])
                    count = 1

            # __page_num_total += 1
            _invoice_data.append({
                '_last_page': '1',
                '_invoice': _invoice,
                '_page_num': _page_num
            })

            docargs = {
                'doc_ids': docids,
                'doc_model': 'vendors.invoice',
                '_invoice_line': _invoice_line,
                '_invoice_total': _invoice_total,
                '_tax_total': _tax_total,
                '_custody_line': ass,
                '_custody_total': _custody_total,
                '_data': _invoice_data,
                'docs': _invoice_docs

            }
            invoices.append(docargs)
            print invoices

        return self.env['report'].render('vegetables.print_vendor_invoice_template', {'all_data': invoices})




























        # for lines in docs.custody_lines:
        #     if lines.cash_account.name:
        #         _invoice_line.append({
        #             '_customer': '',
        #             '_purchase_price': '',
        #             '_item': '',
        #             '_quantity': '',
        #             '_price': '',
        #             'purchase_taxes': '',
        #             '_total_price': '',
        #             '_custody': lines.custody_name,
        #             '_total_price': lines.cash_amount
        #         })
        #         _custody_total += int(lines.cash_amount)
        #
        #     if lines.credit_account.name:
        #         _invoice_line.append({
        #             '_customer': '',
        #             '_purchase_price': '',
        #             '_item': '',
        #             '_quantity': '',
        #             '_price': '',
        #             'purchase_taxes': '',
        #             '_total_price': '',
        #             '_custody': lines.custody_name,
        #             '_total_price': lines.credit_amount
        #         })
        #         _custody_total += int(lines.credit_amount)
        #
        # # print(len(_invoice_line) / 12)
        # # print (len(_invoice_line))
        #
        # if (len(_invoice_line) == 12):
        #     _loop = 11
        # elif (len(_invoice_line) < 12):
        #     _loop = 11
        # elif (len(_invoice_line) > 12):
        #     _loop = 12
        #     if (len(_invoice_line) % 12 == 0):
        #         _loop_last_paper = 11
        #     else:
        #         _loop_last_paper = 11
        #
        # _page_num_total = len(_invoice_line) / _loop
        #
        #
        # for data in range(len(_invoice_line)):
        #     count += 1
        #     if count <= _loop:
        #         _invoice.append(_invoice_line[data])
        #     else:
        #         _invoice_data.append({
        #             '_last_page': '-1',
        #             '_invoice': _invoice,
        #             '_page_num': _page_num
        #         })
        #         _page_num = _page_num + 1
        #         __page_num_total += 1
        #
        #         if (__page_num_total == _page_num_total):
        #             _loop = _loop_last_paper
        #         _invoice = []
        #         _invoice.append(_invoice_line[data])
        #         count = 1
        #
        # # __page_num_total += 1
        # _invoice_data.append({
        #     '_last_page': '1',
        #     '_invoice': _invoice,
        #     '_page_num': _page_num
        # })
        #
        #
        # print _invoice_data
        #
        #
        # docargs = {
        #     'doc_ids': docids,
        #     'doc_model': 'vendors.invoice',
        #     '_invoice_line': _invoice_line,
        #     '_invoice_total': _invoice_total,
        #     '_custody_line': _custody_line,
        #     '_custody_total': _custody_total,
        #     '_data':_invoice_data,
        #     'docs': docs
        #
        # }
        #
        # return self.env['report'].render('vegetables.print_vendor_invoice_template', docargs)
