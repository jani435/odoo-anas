# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError
from num2words import num2words
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic
from operator import itemgetter


class Reportall_payment(models.AbstractModel):
    _name = 'report.vegetables.report_daily_inv_report'

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))

        date_from = parse(docs.date_from).date()
        date_to = parse(docs.date_to).date()
        wanted_partner_id = docs.partner_id
        partners_static = self.env['res.partner'].search([])
        if wanted_partner_id:
            partners_static = [wanted_partner_id]
        partners = []
        all_vendor_total_amount = all_customer_total_amount = all_difference =0.0
        for p in partners_static:
            sum = 0.0
            for inv in p.vendors_invoice_line_ids:
                if parse(inv.vendor_invoice_id.invoice_date).date() >= date_from and parse(inv.vendor_invoice_id.invoice_date).date() <= date_to:
                    sum += inv.total_price
            vendor_total_amount = sum

            sum = 0.0
            for inv in p.customer_account_invoice_ids:
                if parse(inv.date_invoice).date() >= date_from and parse(inv.date_invoice).date() <= date_to:
                    if inv.type == 'out_invoice':
                        sum += inv.amount_total
                    if inv.type == 'out_refund':
                        sum -= inv.amount_total
            customer_total_amount = sum

            if vendor_total_amount != 0.0 or customer_total_amount != 0.0:
                vendor_total_amount = round(vendor_total_amount, 2)
                customer_total_amount = round(customer_total_amount, 2)
                all_vendor_total_amount += vendor_total_amount
                all_customer_total_amount += customer_total_amount
                all_difference += abs(vendor_total_amount - customer_total_amount)
                partners.append({
                    'name':p.name,
                    'vendor_total_amount':vendor_total_amount,
                    'customer_total_amount':customer_total_amount,
                    'difference':abs(vendor_total_amount - customer_total_amount)
                })
            

        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'all_data': partners,
            'all_vendor_total_amount':all_vendor_total_amount,
            'all_customer_total_amount':all_customer_total_amount,
            'all_difference':all_difference,
        }
        return self.env['report'].render('vegetables.report_daily_inv_report', docargs)
