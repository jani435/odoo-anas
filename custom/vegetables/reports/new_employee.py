# -*- coding: utf-8 -*-

from odoo import api, models


class NewEmployee(models.AbstractModel):
    _name = 'report.vegetables.new_employee_template'

    # @api.model
    # def render_html(self, docids, data=None):
    #     docs = self.env['hr.employee'].browse(docids)
    #     _invoice_data = []
    #     _invoice = []
    #     invoices = []
    #     for _invoice_docs in docs:
		# docargs = {
		#         'doc_ids': docids,
		#         'doc_model': 'hr.employe',
		#         '_data': _invoice_data,
		#         'docs': _invoice_docs
    #
		# }
    #
		# invoices.append(docargs)
    #
    #
    #
    #     return self.env['report'].render('vegetables.new_employee_template', {'all_data': invoices})

    @api.model
    def render_html(self, docids, data=None):
        payslips = self.env['hr.employee'].browse(docids)
        docargs = {
            'doc_ids': docids,
            'doc_model': 'hr.employee',
            'docs': payslips,
            'data': data,
        }
        return self.env['report'].render('vegetables.new_employee_template', docargs)


















