# -*- coding: utf-8 -*-

from odoo import api, models
from ..models.money_to_text_ar import amount_to_text_arabic


class PrintVendorInvoice(models.AbstractModel):
    _name = 'report.vegetables.print_vendor_invoice_template_a4'

    @api.model
    def render_html(self, docids, data=None):
        docs = self.env['vendors.invoice'].browse(docids)

        invoices = []
        for _invoice_docs in docs:
            _invoice_line = []
            _custody_line = []
            _invoice_total = 0
            _custody_total = 0
            _tax_total = 0

            _invoice_data = []
            _invoice = []
            __page_num_total = 1
            _loop_last_paper = 0
            _page_num = 1
            total_lines = 0.0
            total_lines_qty = 0

            for line in _invoice_docs.invoice_line:
                total_price_2 = round(line.product_qty * line.unit_price, 4)
                _invoice_line.append({
                    '_customer': line.customer_id.code,
                    '_purchase_price': line.purchase_price,
                    '_item': line.product_id.name,
                    '_quantity': line.product_qty,
                    '_price': line.unit_price,
                    'purchase_taxes': line.purchase_taxes,
                    'taxes_amount': line.taxes_amount,
                    'total_price_2': total_price_2,
                    '_total_price': round(line.purchase_price, 4) * round(line.product_qty, 4) + round(
                        line.purchase_taxes, 4)
                })
                total_lines_qty +=  line.product_qty
                total_lines += total_price_2
                _invoice_total = _invoice_total + (
                round(line.purchase_price, 4) * round(line.product_qty, 4) + round(line.purchase_taxes, 4))
                _tax_total = _tax_total + round(line.purchase_taxes, 4)

            ass = []
            for lines in _invoice_docs.custody_lines:
                ass.append({
                    'cus_name': lines.custody_name,
                    'amount': lines.cash_amount if lines.cash_amount > 0 else lines.credit_amount
                })
                _custody_total += float(lines.cash_amount) if float(lines.cash_amount) > 0 else float(
                    lines.credit_amount)


            



            if (len(_invoice_line) == 20):
                _loop = 19
            elif (len(_invoice_line) < 20):
                _loop = 15
            elif (len(_invoice_line) > 20):
                _loop = 20
                if (len(_invoice_line) % 20 == 0):
                    _loop_last_paper = 19
                else:
                    _loop_last_paper = 10

            _page_num_total = len(_invoice_line) / _loop
            if (len(_invoice_line) % _loop != 0):
                _page_num_total += 1

            count = 0
            for data in range(len(_invoice_line)):
                count += 1
                if count <= _loop:
                    _invoice.append(_invoice_line[data])
                else:
                    _invoice_data.append({
                        '_last_page': '-1',
                        '_invoice': _invoice,
                        '_page_num': _page_num
                    })
                    _page_num = _page_num + 1
                    __page_num_total += 1

                    if (__page_num_total == _page_num_total):
                        _loop = _loop_last_paper
                    _invoice = []
                    _invoice.append(_invoice_line[data])
                    count = 1

            # __page_num_total += 1
            _invoice_data.append({
                '_last_page': '1',
                '_invoice': _invoice,
                '_page_num': _page_num
            })
            amount = total_lines - (_custody_total + _invoice_docs.commision_total_amount )
            sign = ''
            if amount < 0 : 
                amount = -1 * amount
                sign = '-'
            total_in_word = amount_to_text_arabic(amount,_invoice_docs.currency_id.name)
            if sign : total_in_word = sign + ' ' + total_in_word

            docargs = {
                'doc_ids': docids,
                'doc_model': 'vendors.invoice',
                '_invoice_line': _invoice_line,
                '_invoice_total': _invoice_total,
                '_tax_total': _tax_total,
                '_custody_line': ass,
                '_custody_total': _custody_total,
                '_data': _invoice_data,
                'docs': _invoice_docs,
                'total_lines': total_lines,
                'total_lines_qty': total_lines_qty,
                'total_in_word': total_in_word,
                'amount': amount,

            }
            invoices.append(docargs)

        return self.env['report'].render('vegetables.print_vendor_invoice_template_a4', {'all_data': invoices})




