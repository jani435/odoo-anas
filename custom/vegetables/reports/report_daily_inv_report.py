# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError
from num2words import num2words
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic
from operator import itemgetter



class Reportall_payment(models.AbstractModel):
    _name = 'report.vegetables.report_daily_inv_report'


    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))

        print "active_model >> ",  self.model
        print "active_id >> ",  docs

        date_from = parse(docs.date_from).date()
        date_to = parse(docs.date_to).date()
        wanted_partner_id = docs.partner_id
        #partners_in_invoice = self.env['account.invoice'].search([('date_invoice','<=', date_from),('date_invoice', '>=', date_to)])
        partners_in_invoicesss = self.env['vendors.invoice'].search([('invoice_date','<=', date_to),('invoice_date', '>=', date_from)])

        print partners_in_invoicesss
        uniqueList = []
        for inv_line in partners_in_invoicesss:
            for partner in inv_line.invoice_line:
                if partner.customer_id.id not in uniqueList:
                    uniqueList.append(partner.customer_id.id)

      
        partners = []
        all_vendor_total_amount = all_customer_total_amount = all_difference = all_vendor_taxes = all_taxes_difference =all_customer_taxes = 0.0

        
        #for inv_line in partners_in_invoicesss:
        for partner in uniqueList:
            self._cr.execute("select "\
                            "res_partner.name as name , "\
                            "res_partner.code as code , "\
                            "round(sum((vendors_invoice_line.unit_price * vendors_invoice_line.product_qty)),4) as ven_total , " \
                            "round((sum(vendors_invoice_line.total_price) - sum((vendors_invoice_line.unit_price * vendors_invoice_line.product_qty))),4) as ven_tax " \
                            "from "\
                            "res_partner "\
                            "INNER JOIN vendors_invoice_line on res_partner.id = vendors_invoice_line.customer_id "
                            "INNER JOIN vendors_invoice on vendors_invoice_line.vendor_invoice_id = vendors_invoice.id "\
                            "where "\
                            "vendors_invoice_line.customer_id in (%s)"\
                            "and vendors_invoice.invoice_date BETWEEN '%s' AND '%s'"\
                            "GROUP BY "\
                            "res_partner.name,res_partner.code;" %(partner,date_from,date_to))
            res = self._cr.dictfetchall()

            self._cr.execute("select "\
                            "round(sum(amount_untaxed),4) as cus_total, "\
                            "round(sum(amount_total - amount_untaxed),4) as cus_tax "\
                            "from "\
                            "account_invoice "\
                            "where "\
                            "partner_id in (%s) "\
                            "and date BETWEEN '%s' AND '%s';" %(partner,date_from,date_to))
            res1 = self._cr.dictfetchall()

            #if res and res1:
            ven_total = res[0]['ven_total'] or 0.0
            ven_tax = res[0]['ven_tax'] or 0.0

            cus_total = res1[0]['cus_total'] or 0.0
            cus_tax = res1[0]['cus_tax'] or 0.0

            all_vendor_total_amount += ven_total
            all_vendor_taxes += ven_tax 
            
            all_customer_total_amount += cus_total
            all_customer_taxes += cus_tax

            all_difference += ( ven_total - cus_total )
            all_taxes_difference += ( ven_tax - cus_tax)


            partners.append({
                    'name':res[0]['name'],
                    'code':int(res[0]['code'] or 0),
                    'vendor_total_amount':ven_total ,
                    'vendor_taxes':ven_tax,
                    'customer_taxes' : cus_tax ,
                    'customer_total_amount':cus_total,
                    'difference':( ven_total  - cus_total ),
                    'vendor_taxes_difference':( ven_tax  - cus_tax  )
                })




            


        uniqueList = []
        
        # Iterate over the original list and for each element
        # add it to uniqueList, if its not already there.
        for elem in partners:
            if elem not in uniqueList:
                uniqueList.append(elem)


        #print "SSSSSSSSSSSSSSSSSSSSSSSSSSSS"
        partners = uniqueList
        #print "::::::::::::::",
        #partners = sort(partners, key=lambda k: k['code']) 
        partners.sort(key=lambda k: k['code']) 
        
        
        #partners = np.argsort(partners, axis=2)
        

        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'all_data': partners,
            'all_vendor_total_amount':all_vendor_total_amount,
	        'all_vendor_taxes':all_vendor_taxes,
	        'all_customer_taxes':all_customer_taxes,
            'all_customer_total_amount':all_customer_total_amount,
            'all_difference':all_difference,
	        'all_taxes_difference':all_taxes_difference
        }
        return self.env['report'].render('vegetables.report_daily_inv_report', docargs)

    