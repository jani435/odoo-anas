# -*- coding: utf-8 -*-

from odoo import api, models


class AdvanceRequest(models.AbstractModel):
    _name = 'report.vegetables.advance_request_template'

    # @api.model
    # def render_html(self, docids, data=None):
    #     docs = self.env['hr.loan'].browse(docids)
    #     _invoice_data = []
    #     _invoice = []
    #     invoices = []
    #     for _invoice_docs in docs:
		# docargs = {
		#         'doc_ids': docids,
		#         'doc_model': 'hr.loan',
		#         '_data': _invoice_data,
		#         'docs': docs
    #
		# }
    #
		# invoices.append(docargs)
    #
    #
    #     return self.env['report'].render('vegetables.advance_request_template', {'all_data': docs})

    @api.model
    def render_html(self, docids, data=None):
        payslips = self.env['hr.loan'].browse(docids)
        docargs = {
            'doc_ids': docids,
            'doc_model': 'hr.loan',
            'docs': payslips,
            'data': data,
        }
        return self.env['report'].render('vegetables.advance_request_template', docargs)


















