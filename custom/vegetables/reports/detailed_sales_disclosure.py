# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError
from num2words import num2words
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic
from operator import itemgetter






class detailed_sales_disclosure(models.AbstractModel):
    _name = 'report.vegetables.detailed_sales_disclosure'

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        date_from = parse(docs.date_from).date()
        date_to = parse(docs.date_to).date()
        partners = []

	total_invoice = 0.0
        customer_invoice = self.env['account.invoice'].sudo().search([('date_invoice', '>=', date_from), ('date_invoice', '<=', date_to),('partner_id', '=', docs.customer_id.id)])
	if customer_invoice : 


		if docs.vendor_id :
			for invoice in customer_invoice :
				for line in invoice.invoice_line_ids:
					if line.vendor_invoice_id.vendor_id == docs.vendor_id :
						partners.append({
						    'product_id':line.product_id.name,
						    'quantity':line.quantity,
						    'price_unit':line.price_unit,
						    'name':invoice.sequence_number,
						    'sales_man_id':line.sales_man_id.name,
						    'vendor':line.vendor_invoice_id.vendor_id.name,
						    'vendor_invoice_id':line.vendor_invoice_id.name,
						    'total':line.quantity *  line.price_unit,

						})
						total_invoice = total_invoice + (line.quantity *  line.price_unit)


		if not docs.vendor_id :
			for invoice in customer_invoice :
				for line in invoice.invoice_line_ids:

					partners.append({
					    'product_id':line.product_id.name,
					    'quantity':line.quantity,
					    'price_unit':line.price_unit,
					    'name':invoice.sequence_number,
					    'sales_man_id':line.sales_man_id.name,
					    'vendor':line.vendor_invoice_id.vendor_id.name,
					    'vendor_invoice_id':line.vendor_invoice_id.name,
					    'total':line.quantity *  line.price_unit,

					})
					total_invoice = total_invoice + (line.quantity *  line.price_unit)






        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'all_data': partners,
	    'total_invoice' : total_invoice,

        }
        return self.env['report'].render('vegetables.detailed_sales_disclosure', docargs)

