# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import date,datetime
from odoo.exceptions import ValidationError
from dateutil.parser import parse

def get_date(str):
    return datetime.strptime(str, "%Y-%m-%d").date()

class EditTracking(models.Model):
    _name = 'edit.tracking'

    partner = fields.Char(string='Partner')
    date = fields.Date(string='Date')
    time = fields.Char(string='Time')
    ref = fields.Char(string='Record Name')
    field = fields.Char(string='Field')
    old_value = fields.Char(string='Old Value')
    new_value = fields.Char(string='New Value')
    user_name = fields.Char(string='User Name')

    # #self.get_date()

    # @api.one
    # def get_date(self):
    #     print 'FFFFFFFFFFFFFFFFFFFFFFFFFGGGGGGGGGGGGGGGGGGGGGG'
    #     for rec in self:
    #         self._cr.execute("select mm.record_name,tr.* from mail_tracking_value tr "\
    #                          "inner join mail_message mm on mm.id = tr.mail_message_id "\
    #                          "where tr.mail_message_id in (select id from mail_message where model = 'vendors.invoice') "\
    #                          "and field_desc != 'Version Number' and field_desc != 'C-Code' and "\
    #                          "field_desc != 'مرات التحديث' and field_desc != 'العميل' "\
    #                          "and field_desc != 'Vendor Sequence' and field_desc != 'تسلسل فواتير المورد'" %())
            
    #         res = self._cr.dictfetchall()
            
    #         for rr in res:
    #             rec.record_name = record_name
    #             print rr