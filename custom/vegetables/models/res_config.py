from odoo import models, fields


class ResCompany(models.Model):
    _inherit = "res.company"

    default_department_id = fields.Many2one('stock.warehouse', string='Department')