# -*- coding: utf-8 -*-

from odoo import api, fields, models
from dateutil.parser import parse
from datetime import datetime


class daily_inv_reportWizard(models.TransientModel):
    _name = "daily_inv_report.wizard"
    _description = "daily_inv_report wizard"

    date_from = fields.Date(string='Start Date',default=fields.Date.context_today)
    date_to = fields.Date(string='End Date',default=fields.Date.context_today)

    partner_id = fields.Many2one('res.partner', string='Partner',domain=[('is_market', '=', True)])

    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['date_from', 'date_to', 'partner_id'])[0]
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['date_from', 'date_to', 'partner_id'])[0])
        return self.env['report'].get_action(self, 'vegetables.report_daily_inv_report', data=data)

    def get_data(self, data):
        date_from = parse(self.date_from).date()
        date_to = parse(self.date_to).date()

        wanted_partner_id = self.partner_id
        partners = self.env['res.partner'].search([])
        if wanted_partner_id:
            partners = [wanted_partner_id]

        partners_ids = []
        for p in partners:
            vendor_total_amount = 0.0
            for inv in p.vendors_invoice_line_ids:
                if parse(inv.vendor_invoice_id.invoice_date).date() >= date_from and parse(inv.vendor_invoice_id.invoice_date).date() <= date_to:
                    vendor_total_amount += inv.total_price
            vendor_total_amount = round(vendor_total_amount, 2)
            p.vendor_total_amount = vendor_total_amount

            customer_total_amount = 0.0
            for inv in p.customer_account_invoice_ids:
                if parse(inv.date_invoice).date() >= date_from and parse(inv.date_invoice).date() <= date_to:
                    if inv.type == 'out_invoice':
                        customer_total_amount += inv.amount_total
                    if inv.type == 'out_refund':
                        customer_total_amount -= inv.amount_total
            customer_total_amount = round(customer_total_amount, 2)
            p.customer_total_amount = customer_total_amount
            p.difference = abs(vendor_total_amount - customer_total_amount)
            

            if p.vendor_total_amount != 0.0 or p.customer_total_amount != 0.0:
                partners_ids.append(p.id)

        view = self.env.ref('vegetables.daily_inv_partners_tree')
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'res.partner',
            'view_mode': 'tree',
            'view_type': 'form',
            'domain': [('id','in',partners_ids)],
            'view_id': view.id,
            'target': 'current',
        }

def get_date(str):
    return datetime.strptime(str, "%Y-%m-%d").date()
