# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from datetime import date,datetime


class EditTrackingWiz(models.TransientModel):
	_name = 'edit.tracking.wiz'

	date_from  = fields.Date(string=' Date From')
	date_to = fields.Date(string=' Date To')
	type = fields.Selection(
        [('vendor_invoice', 'Vendor Invoice'), ('customer', 'Customer Invoices'),
		('payments', 'Payments')],
        string='Type', default='vendor_invoice')

	
	
	@api.multi
	def get_data(self):
		
		partner_obj = self.env["res.partner"]
		sales_man_obj = self.env["hr.employee"]
		account_obj = self.env['account.account']

		main_where = ''
		sec_where = ''
		third_where = ''
		query = ''
		self._cr.execute("delete from edit_tracking"%())
		if self.type == 'vendor_invoice':
			self._cr.execute("select xx.name partner, pp.name, mm.record_name, tr.* from mail_tracking_value tr "\
					"inner join mail_message mm on mm.id = tr.mail_message_id  "\
					"inner join res_users uu on uu.id = tr.create_uid  "\
					"inner join res_partner pp on pp.id = uu.partner_id "\
					"inner join vendors_invoice vv on vv.name = mm.record_name "\
					"inner join res_partner xx on  xx.id = vv.vendor_id "\
					"where tr.mail_message_id in (select id from mail_message where model = 'vendors.invoice')  "\
					"and field_desc != 'Version Number' and field_desc != 'C-Code' and   "\
					"field_desc != 'مرات التحديث' and field_desc != 'العميل' "\
					"and field_desc != 'Vendor Sequence' and field_desc != 'تسلسل فواتير المورد' "\
					"and( vv.invoice_date >= '%s' "\
					"and vv.invoice_date <= '%s')"%(self.date_from, self.date_to))
			


		if self.type == 'customer':
			self._cr.execute("select mm.res_id,pp.name, mm.record_name, tr.* from mail_tracking_value tr  "\
					"inner join mail_message mm on mm.id = tr.mail_message_id  "\
					"inner join res_users uu on uu.id = tr.create_uid  "\
					"inner join res_partner pp on pp.id = uu.partner_id "\
					"inner join account_invoice vv on vv.id = mm.res_id "\
					"where (tr.mail_message_id in (select id from mail_message where model = 'account.invoice')  "\
					"and ((record_name like '%فاتورة%' or record_name like '%Invoice%') "\
					"and field_desc != 'العملة' and field_desc != 'نوع' "\
					"and field_desc != 'شريك' and field_desc != 'الحاله' "\
					"and field_desc != 'Currency' and field_desc != 'Partner' "\
					"and field_desc != 'Type' and field_desc != 'Status' )) "\
					"and( vv.date_invoice > '%s' "\
					"and vv.date_invoice < '%s')"%(self.date_from, self.date_to))
		
		# if self.type == 'payments':
		# 	main_where = 'account.payment'
		# 	third_where = "and field_desc != 'العملة' and field_desc != 'نوع' and  "\
		# 				 "field_desc != 'شريك' and field_desc != 'الحاله' "\
		# 				 "and field_desc != 'Currency' and record_name != 'Draft Payment' "\
		# 				 "and field_desc != 'Type' and field_desc != 'Status' "


		
		
		# self._cr.execute("select pp.name,mm.record_name,tr.* from mail_tracking_value tr "\
		# 				 "inner join mail_message mm on mm.id = tr.mail_message_id "\
		# 				 "inner join res_users uu on uu.id = tr.create_uid "\
		# 				 "inner join res_partner pp on pp.id = uu.partner_id "\
		# 				 "inner join res_partner xx on  xx.id = vv.vendor_id "\
		# 				 "where tr.mail_message_id in (select id from mail_message where model = '%s' %s ) %s;" %(main_where, sec_where, third_where))

		
		res = self._cr.dictfetchall()
		#print res
		for rec in res:
			
			if self.date_from >= rec['create_date'] and self.date_to <= rec['create_date']:
				print rec['create_date']

			old_data = new_data = 'as'
			if rec["old_value_monetary"] is not None:
				old_data = rec["old_value_monetary"]
				new_data = rec["new_value_monetary"]

			if rec["old_value_text"] is not None:
					old_data = rec["old_value_text"]
					new_data = rec["new_value_text"]
			
			if rec["old_value_datetime"] is not None:
					old_data = rec["old_value_datetime"]
					new_data = rec["new_value_datetime"]
			
			if rec["old_value_char"] is not None:
					old_data = rec["old_value_char"]
					new_data = rec["new_value_char"]
			
			if rec["old_value_float"] is not None:
					old_data = rec["old_value_float"]
					new_data = rec["new_value_float"]
			
			if rec["old_value_integer"] is not None:
					old_data = rec["old_value_integer"]
					new_data = rec["new_value_integer"]
						

			if rec["field_desc"] == 'Vendor Name' or rec["field_desc"] == 'اسم المورد':
				try:
					old_data = partner_obj.browse(rec["old_value_integer"]).name
				except:
					old_data = ""
				
				try:
					new_data = partner_obj.browse(rec["new_value_integer"]).name
				except:
					new_data = ""
					
					
			
			if rec["field_desc"] == 'Sales Man' or rec["field_desc"] == 'المندوب':
					old_data = sales_man_obj.browse(rec["old_value_integer"]).name
					new_data = sales_man_obj.browse(rec["new_value_integer"]).name  
			
			if rec["field_desc"] == 'Cash Account': #or rec["field_desc"] == 'المندوب':
					old_data = account_obj.browse(rec["old_value_integer"]).name
					new_data = account_obj.browse(rec["new_value_integer"]).name  

			if rec["field_desc"] == 'Credit Account': #or rec["field_desc"] == 'المندوب':
				try:
					old_data = account_obj.browse(rec["old_value_integer"]).name
				except:
					old_data = ""
				
				try:
					new_data = account_obj.browse(rec["new_value_integer"]).name
				except:
					new_data = ""
									

			if self.type == 'vendor_invoice':
				self._cr.execute("insert into edit_tracking "\
							 "( date, time, field, ref, old_value, new_value, user_name, partner) VALUES "\
							 "( '%s', '%s', '%s', '%s' ,'%s', '%s', '%s', '%s') ;" 
							%(rec['create_date'], rec['create_date'], rec['field_desc'], rec['record_name'],
							 old_data, new_data, rec['name'], rec['partner'] ))
			


			if self.type == 'customer':
				self._cr.execute("insert into edit_tracking "\
								"( date, time, field, ref, old_value, new_value, user_name) VALUES "\
								"( '%s', '%s', '%s', '%s' ,'%s', '%s', '%s') ;" 
								%(rec['create_date'], rec['create_date'], rec['field_desc'], rec['record_name'],
								old_data, new_data, rec['name']))
		
			
		action = self.env.ref('vegetables.edit_tracking_action').read()[0]
		return action

	


