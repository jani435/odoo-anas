# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import api, fields, models


class customerInvoicesMovement(models.TransientModel):
    _name = "re_customer_invoices_movement.wizard"
    _description = "customer invoices movement wizard"

    date_from = fields.Date(string='Start Date', default=str(datetime.now().date()))
    date_to = fields.Date(string='End Date', default=str(datetime.now().date()))

    @api.multi
    def load_report(self):
        date = {'from': self.date_from, 'to': self.date_to}
        return self.env['report'].get_action(self, 'vegetables.re_customer_invoices_movementl_template', data=date)


class rePrintVendorInvoice(models.AbstractModel):
    _name = 'report.vegetables.re_customer_invoices_movementl_template'

    @api.model
    def render_html(self, docids, data=None):

        _filter = []
        _vendor_invoice = self.env['vendors.invoice.line'].search([])
        _customer_invoice = self.env['account.invoice'].search([])
        _customer_name = []
        _data = []

        # ** delete duplication in custome name
        seen = set()
        for x in _vendor_invoice:
            _customer_name.append(x.customer_id)
            for value in _customer_name:
                if value not in seen:
                    _filter.append(value)
                    seen.add(value)

        _customer_name = _filter
        # *********************************************************************

        _VSum = 0
        _CSum = 0
        _count = 0
        for x in _customer_name:
            for inv in _vendor_invoice:
                if inv.vendor_invoice_id.invoice_date >= data['from'] and inv.vendor_invoice_id.invoice_date <= data['to']:
                    if inv.customer_id == x:
                        _VSum = _VSum + inv.total_price
            for inc in _customer_invoice:
                if inc.date_invoice >= data['from'] and inc.date_invoice <= data['to']:
                    if inc.partner_id == x:
                        _CSum = _CSum + inc.amount_total

            _count += 1
            _data.append({
                '_count': _count,
                '_customer_name': x.name,
                '_vendor_invoice_amount': _VSum,
                '_customer_invoice_amount': _CSum,
                '_residual': round(_VSum,3) - round(_CSum,3)

            })
            _VSum = 0
            _CSum = 0

        docargs = {
            '_data': _data,
            '_date_from': data['from'],
            '_date_to': data['to'],
            '_date_now': datetime.now().date(),
            '_time_now': datetime.now().time()
        }



        print docargs

        return self.env['report'].render('vegetables.re_customer_invoices_movementl_template', docargs)
