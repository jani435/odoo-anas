# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError
from num2words import num2words
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic
from operator import itemgetter


class ReportAllPayment(models.AbstractModel):
    _name = 'report.custom_account.report_all_payment_report'

    @api.model
    def render_html(self, docids, data=None):

        print ":::::::::::::::::::::lllllllllllllllllllllllllll"
        pay = self.env['account.payment'].search(
            [('id', 'in', docids)])
        payments = self.env['account.payment.line'].search(
            [('payment_id.id', 'in', docids)])

        all_data = {}

        for payment in payments:
            all_data[payment.payment_id.journal_id] = all_data.get(payment.payment_id.journal_id,{'data':[],'sum':0.0})
            all_data[payment.payment_id.journal_id]['data'].append(payment)
            all_data[payment.payment_id.journal_id]['sum'] += payment.amount

        docargs = {
            'doc_ids': self.ids,
            'date_from': pay[0].payment_date,
            'date_to': pay[len(pay) -1].payment_date,
            'pay':pay,
            'journal_type':pay[0].journal_id.type,
            'payment_type':pay[0].payment_type,
            'all_data': all_data,
        }
        return self.env['report'].render('custom_account.report_all_payment_report', docargs)
