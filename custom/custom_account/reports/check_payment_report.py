# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError
from num2words import num2words
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic
from operator import itemgetter


class Reportcheck_payment(models.AbstractModel):
    _name = 'report.custom_account.report_check_payment_report'

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        move_lines = []
        payments = self.env['account.payment'].search(
            [], order='payment_date')
        #('payment_method_id', '!=', 0)
        all_data = {}
        all_data_sum = {}
        if docs.date_from and docs.date_to:
            for payment in payments:
                if parse(docs.date_from) <= parse(payment.payment_date) \
                        and parse(payment.payment_date) <= parse(docs.date_to) and payment.payment_method_id.name == 'Check Followup':
                    all_data[payment.journal_id] = all_data.get(payment.journal_id,{'data':[],'sum':0.0})
                    all_data[payment.journal_id]['data'].append(payment)
                    all_data[payment.journal_id]['sum'] += payment.amount

                    
        else:
            raise UserError("Please enter duration")
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'all_data': all_data,
        }
        return self.env['report'].render('custom_account.report_check_payment_report', docargs)
