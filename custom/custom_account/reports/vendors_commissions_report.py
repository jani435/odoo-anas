# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError
from num2words import num2words
from operator import itemgetter


class VendorsCommReport(models.AbstractModel):
    _name = 'report.custom_account.report_vendors_commissions'


    # Function to find commissions for each vendor or all vendors depend on user selection and 
    # and find due commissions and paid commissions and find differences of payments then print the report

    def get_data(self, date_from, date_to, partner_id):
        global totals
        totals = {
            'supply':0.00,
            'due':0.00,
            'paid':0.00,
            'diff':0.00,
        }
        tot_supply=tot_due=tot_paid=tot_diff=0.0
        report_data = []
        act_domain = ''
        invoice_obj = self.env['vendors.invoice']
        move_obj = self.env['account.move']
        vendors_obj=self.env['res.partner']
        if partner_id == False:
            act_domain = [('invoice_date','>=',date_from),('invoice_date',  '<=', date_to)]
        else:
            act_domain = [('vendor_id', '=', partner_id[0]),('invoice_date','>=',date_from),('invoice_date',  '<=', date_to)]#,('state', '=','posted')
        result = invoice_obj.search(act_domain)
        vendors_ids = vendors_obj.search([])

        for vendor_id in vendors_ids:
            vendor= False
            commission=supply=due=paid_amount=difference_amount=0.0
            for line in result:
                difference=0.0
                paid=0.0
                if vendor_id == line.vendor_id:
                    #move_id = move_obj.search([('ref','=',line.name)])
                    # for move in move_id.line_ids:
                    #     if move.name in ('Commission', 'العمولة'):
                    #         paid=move.credit

                    if line.state == 'posted':
                        paid += line.commision_total_amount

                    difference=abs(line.commision_total_amount - paid)
                    vendor = line.vendor_id.name
                    commission=line.commision
                    supply+=round(line.untaxes_total, 2)
                    due+=round(line.commision_total_amount, 2)
                    paid_amount+=round(paid,2)
                    difference_amount+=round(difference,2)
            tot_supply+=supply
            tot_due+=due
            tot_paid+=paid_amount
            tot_diff+=difference_amount
            if vendor:
                report_data.append({'vendor': vendor,
                                   'commission': commission,
                                   'supplyment':supply,
                                   'due':due,
                                   'paid':paid_amount,
                                   'difference':difference_amount,
                                   })

        totals['supply'] = tot_supply
        totals['due'] = tot_due
        totals['paid'] = tot_paid
        totals['diff'] = tot_diff
        return report_data

    def get_totals(self):
        result = []
        res = {}
        res = {
             'tot_supply': totals['supply'],
             'tot_due': totals['due'],
             'tot_paid': totals['paid'],
             'tot_diff':totals['diff'] ,
        }
        result.append(res)
        return result

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_ids',
                                                                []))
        partner_id = data['form'].get('partner_id')
        date_from=data['form'].get('date_from')
        date_to=data['form'].get('date_to')
        rm_act=self.with_context(data['form'].get('used_context',{}))
        data_res = rm_act.get_data(date_from, date_to, partner_id)
        totals_res = rm_act.get_totals()
        docargs = {
            'doc_ids': docids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'report_data': data_res,
            'totals': totals_res,
        }
        render_model = 'custom_account.report_vendors_commissions'
        return self.env['report'].render(render_model, docargs)
