from odoo import api, models
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic


class partner_noti_report_template(models.AbstractModel):
    _name = 'report.custom_account.print_payments_template'


    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')

        docs = self.env['account.payment'].browse(docids)
        #print '************************', docs.line_ids[0].partner_id.name

        docargs = {
            'doc_ids': docids,
            'doc_model': 'account.payment',
            'docs': docs,
            'total_words': amount_to_text_arabic(abs(docs.amount), 'SAR'),
        }

        return self.env['report'].render('custom_account.print_payments_template', docargs)
