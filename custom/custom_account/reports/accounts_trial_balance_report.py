# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError

class accounts_trial_balance(models.AbstractModel):
    _name = 'report.custom_account.accounts_trial_balance_report'

    @api.model
    def render_html(self, docids, data=None):
        docs = self.env['accounts.trial.balance'].browse(docids)
        docargs = {
            'doc_ids': docids,
            'doc_model': 'accounts.trial.balance',
            'docs': docs,
        }
 
        return self.env['report'].render('custom_account.accounts_trial_balance_report', docargs)
 