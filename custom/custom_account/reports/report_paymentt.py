#-*- coding:utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models


class PaymenttReport(models.AbstractModel):
    _name = 'report.custom_account.report_paymentt'

    @api.model
    def render_html(self, docids, data=None):
        payments = self.env['account.payment'].browse(docids)
        docargs = {
            'doc_ids': docids,
            'doc_model': 'account.payment',
            'docs': payments,
            'data': data,
        }
        return self.env['report'].render('custom_account.report_paymentt', docargs)
