# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import api, fields, models, _


class CustomerEmailsReport(models.Model):
    _name = 'customer.email.report'

    partner_id = fields.Many2one('res.partner', 'Partner')
    date_from = fields.Date('Date From',default=datetime.today())
    date_to = fields.Date('Date To', default=fields.Date.context_today)
    state = fields.Selection([('draft', 'Draft'), ('open', 'Open'), ('paid', 'Paid'), ('cancel', 'Cancelled')],
                             default='paid')
    email_status = fields.Selection([('sent', 'Sent'), ('not_sent', 'Not Sent')], 'Email Status', default='not_sent')

    except_date_from = fields.Date('Date From',default=datetime.today())
    except_date_to = fields.Date('Date To', default=fields.Date.context_today)
    except_state = fields.Selection([('draft', 'Draft'), ('open', 'Open'), ('paid', 'Paid'), ('cancel', 'Cancelled')],
                                    default='open')
    except_email_status = fields.Selection([('sent', 'Sent'), ('not_sent', 'Not Sent')], 'Email Status',
                                           default='not_sent')

    def get_invoices(self):
        customers = self.env['res.partner'].sudo().search([]).ids
        if self.partner_id:
            customers = self.env['res.partner'].sudo().search([('parent_id', '=', self.partner_id.id)]).ids
        all_invoices = self.env['account.invoice'].sudo().search(
            [('state', '=', self.state), ('email_status', '=', self.email_status),
             ('date_invoice', '>=', self.date_from),
             ('date_invoice', '<=', self.date_to),
             ('partner_id', 'in', customers)])

        excluded_invoices = self.env['account.invoice'].sudo().search(
            [('state', '=', self.except_state), ('email_status', '=', self.except_email_status),
             ('date_invoice', '>=', self.except_date_from),
             ('date_invoice', '<=', self.except_date_to),
             ('partner_id', 'in', customers)])

        action = self.env.ref('custom_account.custom_account_invoice_action').read()[0]
        action['domain'] = [
            ('id', 'in', all_invoices.filtered(lambda invoice: invoice.id not in excluded_invoices.ids).ids)]
        return action
