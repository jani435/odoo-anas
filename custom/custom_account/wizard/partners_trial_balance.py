# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import api, fields, models, _

class PartnersTrialBalance(models.Model):
    _name = 'partners.trial.balance'

    partner = fields.Char(string='Partner')

    OPB_D = fields.Float(string='OPB Debit')
    OPB_C = fields.Float(string='OPB Credt')
    OPB_D_Date = fields.Float(string='OPB Debit Date')
    OPB_C_Date = fields.Float(string='OPB Credit Date')

    opb_debit = fields.Float(string='debit pefor start date')
    opb_credit = fields.Float(string='credit pefor start date')
    sum_debit = fields.Float(string='Sum Depit')
    sum_credit = fields.Float(string='Sum Credits')
    balance = fields.Float(string='Balance')
    move_line_id = fields.Integer(string='Move Line')

class print_partners_trial_balance_report(models.TransientModel):

    _name = "partners.trial.balance.wizard"
    
    date_from = fields.Date(string='Start Date',default=datetime.today())
    date_to = fields.Date(string='End Date',default=datetime.today())
     

    customers = fields.Boolean('Customers')
    suppliers = fields.Boolean('Suppliers')
    partners_ids = fields.Many2many(
        'res.partner', 'partners_trial_balance_rep', string='Partners')

    check_balance = fields.Boolean('Balance not equals to zero ')
 

    def get_opb(self, partners_id,move_line_ids,date_from,account_ids):
        res=[]
        
        OPB_D=OPB_C=OPB_D_Date=OPB_C_Date=opb_debit=opb_credit=sum_debit=sum_credit=0
        move_line = self.env['account.move.line'].search([('partner_id','=',partners_id),('id','in',move_line_ids)])
        name=''
        #print partners_id,'kkk.......................................',move_line,move_line_ids
        for line in move_line: 
            #print'hooooo..........................',line.id
            sum_debit=sum_debit+line.debit
            sum_credit=sum_credit+line.credit
            name=line.partner_id.name
            #print'yes...............................',line.id
        move_line = self.env['account.move.line'].search([('partner_id','=',partners_id),('account_id','in',account_ids),('date', '<', date_from )])
        #print'baba.......................................',move_line
        for line in move_line:
            #haha=line.name[0:3]
            #print haha,'zzzz.........................',line.id
            if line.name[0:3] == 'OPB':
                #print'hoooooooooooooooooooooooooooooooooooooooooozzzzz..........'
                if line.debit:
                    OPB_D=OPB_D+line.debit
                else:
                    OPB_C=OPB_C+line.credit
            else:
                OPB_D_Date=OPB_D_Date+line.debit
                OPB_C_Date=OPB_C_Date+line.credit

        '''first_payment = self.env['account.first_payment'].search([('partner_id','=',partners_id)])
        #print'noooooooooooooooooooooooooo',first_payment
        for line in first_payment:
            #print'zzzz.........................',line.id
            if line.partner_type == 'supplier':
                opb_debit=opb_debit+line.amount
            else:
                opb_credit=opb_credit+line.amount
        '''
        opb_debit=OPB_D+OPB_D_Date
        opb_credit=OPB_C+OPB_C_Date



        res.append(opb_debit)
        res.append(opb_credit)
        res.append(sum_debit)
        res.append(sum_credit)
        balance=opb_debit+sum_debit-opb_credit-sum_credit
        res.append(balance)

        res.append(OPB_D)
        res.append(OPB_C)
        res.append(OPB_D_Date)
        res.append(OPB_C_Date)

        return res

    
    def get_data(self, data):

        domain=[]
        partners_id=[]
        account_ids=[]
        c=0
        s=0
        cm=0
        sm=0

        form_name='Partners Trial Balance from '+str(self.date_from)+' to '+str(self.date_to)
        if self.partners_ids:
            for x in self.partners_ids:
                partners_id.append(x.id)
                if x.supplier:
                    account_ids.append(x.property_account_payable_id.id)
                    s=s+1
                if x.customer:
                    c=c+1
                    account_ids.append(x.property_account_receivable_id.id)
            domain = [('partner_id','in',partners_id),('account_id','in',account_ids)]+domain

        else:
            all_partners = self.env['res.partner'].search([])
            for partner in all_partners:
                
                if self.customers and partner.customer:
                    c=c+1
                    partners_id.append(partner.id)
                    account_ids.append(partner.property_account_receivable_id.id)
                if self.suppliers and partner.supplier:
                    s=s+1
                    partners_id.append(partner.id)
                    account_ids.append(partner.property_account_payable_id.id)
            domain = [('partner_id','in',partners_id),('account_id','in',account_ids)]+domain




        if self.date_from:
            domain = [('date', '>=', self.date_from )]+domain
        if self.date_to:
            domain = [('date', '<=', self.date_to )]+domain

        #print c,'custmers................................supplieer',s
        date_opb=self.date_from

        
        move_line = self.env['account.move.line'].search(domain)

        move_line_ids = [x.id for x in move_line]

        #delete old data in partners.trial.balance
        old_data = self.env['partners.trial.balance'].search([])
        for x in old_data:
            x.unlink()

        for rec in partners_id:
            partner_name = self.env['res.partner'].search([('id', '=', rec )]).name
            #print'yes................................',partner_name
            res=self.get_opb(rec,move_line_ids,date_opb,account_ids)
            if self.check_balance:
                if res[4] != 0 :
                    apartner_trial_balance_id = self.env['partners.trial.balance'].sudo().create({
                            'partner': partner_name,
                            'opb_debit': res[0],
                            'opb_credit': res[1],
                            'sum_debit': res[2],
                            'sum_credit': res[3],
                            'balance': res[4],

                            'OPB_D': res[5],
                            'OPB_C': res[6],
                            'OPB_D_Date': res[7],
                            'OPB_C_Date': res[8],
                        })
            else:
                apartner_trial_balance_id = self.env['partners.trial.balance'].sudo().create({
                            'partner': partner_name,
                            'opb_debit': res[0],
                            'opb_credit': res[1],
                            'sum_debit': res[2],
                            'sum_credit': res[3],
                            'balance': res[4],

                            'OPB_D': res[5],
                            'OPB_C': res[6],
                            'OPB_D_Date': res[7],
                            'OPB_C_Date': res[8],
                        })
            
        
        return {
            'name':form_name,
            'type': 'ir.actions.act_window',
            'res_model': 'partners.trial.balance',
            'view_type': 'form',            
            'view_mode': 'tree,form,graph',
            'views': [(False, 'tree'), (False, 'form'),(False, 'graph')],
            #'domain': [('id','in',move_line_ids)],
            #'view_id': view.id,
            'target': 'current',
        }
