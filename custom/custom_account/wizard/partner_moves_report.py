# -*- coding: utf-8 -*-
#partner_moves_report_id
from datetime import datetime
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError, Warning


class partner_moves_reportWizard(models.TransientModel):
    _name = "partner_moves_report.wizard"
    _description = "partner_moves_report wizard"
    
    partner_ids = fields.Many2many('res.partner', string='Partner', required=False)
    partner_tags = fields.Many2many('res.partner.category', string='Partners Tags', required=False)
    date_from = fields.Date(string='Start Date',default=fields.Date.context_today,required=True)
    date_to = fields.Date(string='End Date',default=fields.Date.context_today,required=True)
    invoice_detail = fields.Boolean(string='With Invoice Detail')
    init_balance = fields.Boolean(string='With Initial Balance', default=True)
    sales_man_id = fields.Many2one('hr.employee', string='Sales Man',domain=[('is_sales_man', '=', True)])

    @api.multi
    def check_report(self):
        if not self.partner_ids and not self.partner_tags:
		    raise ValidationError(_('Please select partner or partner tag'))
        data = {}
        data['form'] = self.read(['partner_ids', 'date_from', 'date_to','invoice_detail','init_balance'])[0]
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['partner_ids', 'date_from', 'date_to','invoice_detail','init_balance'])[0])
	data['form'].update({'active_model1':"partner_moves_report.wizard",'active_id1':self.id})
        return self.env['report'].get_action(self, 'custom_account.report_partner_moves_report', data=data)


    