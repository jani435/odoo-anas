# -*- coding: utf-8 -*-
#
from odoo import SUPERUSER_ID
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
import time
from dateutil.parser import parse
from num2words import num2words
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic
from operator import itemgetter
from re import sub
from decimal import Decimal


class customers_supliers_balances(models.Model):
    
    _name = 'customers.suppliers'
    _rec_name = 'partner_name'
    _description = "Customer and Supplier Math Balance"

    partner_name = fields.Many2one('res.partner',string='Partner Name',domain="[('parent_id', '=', False)]",required=True)
    matching_date = fields.Date(string='Matching Date', required=True)
    balance = fields.Float(string='Balance', required=True)
    date_entry_mathcing = fields.Date(string='Date of entry of mathcing',default=fields.Date.context_today)
    responsible_person = fields.Many2one('sup.partner',string='Responsible Person',domain="[('partner_id', '=', partner_name)]",required=True)
    actual_balance = fields.Float(readonly=True,stote=True,string='Actual Balance',compute='abbb')
    note = fields.Char(string='Note')
    #pl = fields.Float('plan',compute='abbb')

    def get_actual_balance(self,partner,matching_date):
        #matching_date=check_date
        sum_of_balance=0
        wanted_partner=partner
        #print self.matching_date,'in onchange..................',self.partner_name.id
        if partner and matching_date:
            #print'matching_date........................',matching_date

            getObj= self.env['account.move']
            getAllIds = getObj.search([('partner_id','=',wanted_partner)])
            for purchase_line_id in getAllIds :
                wanted_partner_id=getObj.browse([purchase_line_id])
                #print wanted_partner_id.id.amount,'infor.........................',wanted_partner_id.id.date                
                if wanted_partner_id.id.date <= matching_date :
                    sum_of_balance=sum_of_balance+wanted_partner_id.id.amount
                    #print'in if ............................................',sum_of_balance

        #print'.........................??????????'     
        return sum_of_balance
    @api.one
    @api.depends('partner_name','date_entry_mathcing')
    def abbb(self):
        pl = 0
        gg = 0.00
        partner = self.partner_name.id
        if partner and self.date_entry_mathcing:
            move_line_ids = self.env['account.move.line'].search([('date','<=',self.date_entry_mathcing),('partner_id','=',partner),'|',('account_id','=',self.partner_name.property_account_receivable_id.id),('account_id','=',self.partner_name.property_account_payable_id.id)])
            #print "move line",move_line_ids
            for move_line in move_line_ids:
                dep =Decimal(sub(r'[^\d.]', '', str(move_line.debit)))
                cre =Decimal(sub(r'[^\d.]', '', str(move_line.credit)))
                pll = Decimal(sub(r'[^\d.]', '', str(self.partner_name.balance)))
                yy = dep - cre
                pls = float(pll)
                self.balance = pls * 1.0
                rr = float(yy)
                gg += rr * 1.0
            self.actual_balance = gg
        return self.actual_balance
    # @api.model
    # def create(self,vals):
    #     #print 'innn create.......................',vals
    #     vals['actual_balance'] = self.get_actual_balance(vals['partner_name'],vals['matching_date'])
    #     res = super(customers_supliers_balances, self).create(vals)
    #     return res
    #
    #
    # @api.multi
    # def write(self,vals):
    #     print vals,'in write.......................',self.matching_date
    #     #print'goaa...........................'
    #     flag=1
    #     if 'matching_date' not in vals and 'partner_name' not in vals:
    #         flag=0
    #     if 'matching_date' not in vals:
    #         vals['matching_date']=self.matching_date
    #     if 'partner_name' not in vals:
    #         vals['partner_name']=self.partner_name.id
    #     if flag :
    #         vals['actual_balance'] = self.get_actual_balance(vals['partner_name'],vals['matching_date'])
    #     res = super(customers_supliers_balances, self).write(vals)
    #     return res
    #
    #
    # @api.multi
    # def update_balances(self):
    #     #print 'in update_balances.......................',self
    #     #print'goaa...........................',self.actual_balance
    #     context = self._context
    #     active_ids = context.get('active_ids')
    #     for a_id in active_ids:
    #         customers_supplieres_brw = self.env['customers.suppliers'].browse(a_id)
    #         #print'matching date.........................',customers_supplieres_brw.actual_balance
    #         matching_date=customers_supplieres_brw.matching_date
    #         partner_name=customers_supplieres_brw.partner_name.id
    #         customers_supplieres_brw.actual_balance = self.get_actual_balance(partner_name,matching_date)
                


# new model parent from partner
class sub_partner(models.Model):
    _name = 'sup.partner'
    _description = 'Sub Partner'
    name =fields.Char('Name', required=True)
    partner_id = fields.Many2one('res.partner',string='Partner Name',domain="[('parent_id', '=', False)]",required=True)
    phone = fields.Char('Telephone')
    email = fields.Char('Email')
    job = fields.Char('Job')


# new model parent from partner
class edit_res_partner(models.Model):
    _inherit = 'res.partner'
    sub_partner_ids =fields.One2many('sup.partner','partner_id',string='Sub Partner')
