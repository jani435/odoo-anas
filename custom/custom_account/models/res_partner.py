# -*- coding: utf-8 -*-

from odoo import fields, models,api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    invoice_sequence = fields.Integer('Invoice Sequence')
    account_customer = fields.Many2one('account.account', string="account customer")
    account_vendor = fields.Many2one('account.account', string="account vendor")

    partner_type_id = fields.Many2many('company.types', 'partner_type_rel', 'partner_id' , 'type_id' , 'Type', required=False)


    @api.onchange('property_account_receivable_id')
    def onchange_property_account_receivable(self):
        self.account_customer = self.property_account_receivable_id
    
    @api.onchange('property_account_payable_id')
    def onchange_property_account_payable(self):
        self.account_vendor = self.property_account_payable_id