# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import datetime


class AccountMove(models.Model):
    _inherit = 'account.move'

    @api.multi
    @api.depends('line_ids.debit', 'line_ids.credit')
    def _credit_amount_compute(self):
        for move in self:
            total = 0.0
            for line in move.line_ids:
                total += line.credit
            move.credit_amount = total

    custom_view = fields.Char(default='default')
    account_id = fields.Many2one('account.account', string='Account')
    balance = fields.Float()
    credit_amount = fields.Monetary(compute='_credit_amount_compute', store=True, string='Amount')
    journal_id = fields.Many2one(required=True)
    company_id = fields.Many2one(readonly=False, related='')
    reversed = fields.Boolean(default=False)
    partner_payed = fields.Many2one('res.partner', string='Partner')


    @api.multi
    def post(self):
        invoice = self._context.get('invoice', False)
        self._post_validate()
        for move in self:
            move.line_ids.create_analytic_lines()
            if move.name == '/':
                new_name = False

                if invoice and invoice.move_name and invoice.move_name != '/':
                    new_name = invoice.move_name
                else:
                    # Generate sequences
                    if self.custom_view == 'custom_payment_cash':
                        new_name = self.env['ir.sequence'].next_by_code('payment.cash')
                    elif self.custom_view == 'custom_payment_bank':
                        new_name = self.env['ir.sequence'].next_by_code('payment.bank')
                    elif self.custom_view == 'custom_receive_cash':
                        new_name = self.env['ir.sequence'].next_by_code('receive.cash')
                    elif self.custom_view == 'custom_receive_bank':
                        new_name = self.env['ir.sequence'].next_by_code('receive.bank')
                    elif self.custom_view == 'custom_open_balance':
                        new_name = self.env['ir.sequence'].next_by_code('open.balance')
                    else:
                        # new_name = self.env['ir.sequence'].next_by_code('account.move')
                        if move.journal_id.sequence_id:
                            new_name = move.journal_id.sequence_id.with_context(ir_sequence_date=move.date).next_by_id()
                if new_name:
                    move.name = new_name
        return self.write({'state': 'posted'})

    @api.multi
    def _post_validate(self):
        for move in self:
            
            if move.line_ids:
                if not all([x.company_id.id == move.company_id.id for x in move.line_ids]):
                    raise UserError(_("Cannot create moves for different companies."))
        if self.custom_view == 'default' or self.custom_view == 'custom_open_balance':
            self.assert_balanced()
        return self._check_lock_date()

    @api.multi
    def assert_balanced(self):
        if not self.ids:
            return True

        prec = self.env['decimal.precision'].precision_get('Account')

        self._cr.execute("""\
                    SELECT      move_id
                    FROM        account_move_line
                    WHERE       move_id in %s
                    GROUP BY    move_id
                    HAVING      abs(sum(debit) - sum(credit)) > %s
                    """, (tuple(self.ids), 10 ** (-max(5, prec))))
        if len(self._cr.fetchall()) != 0:
            if self.custom_view != 'default' and self.custom_view != 'custom_open_balance':
                if self.custom_view == 'custom_payment_cash' or self.custom_view == 'custom_payment_bank':
                    total_debit = 0.0
                    for line in self.line_ids:
                        total_debit += line.debit

                    self.line_ids.create({
                        'move_id': self.id,
                        'name': 'auto_credit',
                        'account_id': self.account_id.id,
                        'credit': total_debit,
                        'partner_id' : self.partner_payed.id,
                        'date_maturity': datetime.now(),
                        'active': True

                    })
                elif self.custom_view == 'custom_receive_cash' or self.custom_view == 'custom_receive_bank':
                    total_credit = 0.0
                    for line in self.line_ids:
                        total_credit += line.credit

                    self.line_ids.create({
                        'move_id': self.id,
                        'name': 'auto_debit',
                        'account_id': self.account_id.id,
                        'debit': total_credit,
                        'partner_id': self.partner_payed.id,
                        'date_maturity': datetime.now(),
                        'active': True
                    })
            else:
                raise UserError(_("Cannot create unbalanced journal entry."))
        return True

    @api.multi
    def print_entry(self):
        return self.env['report'].get_action(self, 'custom_account.cash_payment_print_template')

    # @api.model
    # def hide_reverse_moves_button(self):
    #     if self.env.ref('account.action_account_move_reversal'):
    #         self.env.ref('account.action_account_move_reversal').unlink()

    @api.multi
    @api.onchange('account_id')
    def onchange_account_id(self):

        movements = self.search([('state', '=', 'posted')])
        total_balance = 0
        for move in movements:
            if self.account_id:
                self.env.cr.execute(
                    "SELECT account_id,credit,debit FROM account_move_line WHERE account_id = %s and move_id = %s;" %
                    (self.account_id.id, move.id))
                query_results = self.env.cr.dictfetchall()
                for row in query_results:
                    total_balance += row['debit'] - row['credit']
                self.balance = total_balance

    @api.multi
    @api.onchange('partner_payed')
    def onchange_partner_payed(self):
        if self.partner_payed.property_account_receivable_id:
            self.account_id = self.partner_payed.property_account_receivable_id.id
        else:
            if self.partner_payed.property_account_payable_id:
                self.account_id = self.partner_payed.property_account_payable_id.id


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"
    _order = 'line_no'

    @api.multi
    def _update_check(self):
        """ Raise Warning to cause rollback if the move is posted, some entries are reconciled or the move is older than the lock date"""
        move_ids = set()
        for line in self:
            err_msg = _('Move name (id): %s (%s)') % (line.move_id.name, str(line.move_id.id))
            # if line.move_id.state != 'draft':
            #     raise UserError(_(
            #         'You cannot do this modification on a posted journal entry, you can just change some non legal fields. You must revert the journal entry to cancel it.\n%s.') % err_msg)
            if line.reconciled and not (line.debit == 0 and line.credit == 0):
                raise UserError(_(
                    'You cannot do this modification on a reconciled entry. You can just change some non legal fields or you must unreconcile first.\n%s.') % err_msg)
            if line.move_id.id not in move_ids:
                move_ids.add(line.move_id.id)
            self.env['account.move'].browse(list(move_ids))._check_lock_date()
        return True

    def _get_line_numbers(self):
        line_num = 1
        if self.ids:
            first_line_rec = self.browse(self.ids[0])
            for line_rec in first_line_rec.move_id.line_ids:
                line_rec.line_no = line_num
                line_num += 1

    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if self.partner_id.property_account_receivable_id:
            self.account_id=self.partner_id.property_account_receivable_id.id
        else :
            if self.partner_id.property_account_payable_id:
                self.account_id= self.partner_id.property_account_payable_id.id



    active = fields.Boolean(default=True)
    name = fields.Char(required=False)
    line_no = fields.Integer(compute='_get_line_numbers', string='SN', default=0)
