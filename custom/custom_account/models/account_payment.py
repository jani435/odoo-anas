# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    @api.model
    def _get_default_journal_id(self):
        return self.env['account.payment'].sudo().search([('create_uid', '=', self.env.user.id)], order='id desc',
                                                         limit=1).journal_id.id



    payment_type = fields.Selection([('outbound', 'Credit'), (
        'inbound', 'Debit')], string='Payment Type', required=True)


    bill_payment = fields.Boolean('Bill Payment', default=False)
    payment_to = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor'), ('account', 'Direct Account')],
                                  string='Payment To', default='customer')#, ('employee', 'Employee')
    # partner_type = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor')])
    payment_account_id = fields.Many2one('account.account', string='Payment Account')
    # Money flows from the journal_id's default_debit_account_id or default_credit_account_id to the destination_account_id
    destination_account_id = fields.Many2one('account.account', compute='_compute_destination_account_id',
                                             readonly=True)
    #journal_id = fields.Many2one(default=_get_default_journal_id, domain=lambda self: self._get_journal_domain())


    @api.onchange('payment_type')
    def _onchange_payment_type(self):
        if not self.invoice_ids:
            # Set default partner type for the payment type
            if self.payment_type == 'inbound':
                self.partner_type = 'customer'
            elif self.payment_type == 'outbound':
                self.partner_type = 'supplier'
        # Set payment method domain
        res = self._onchange_journal()
        if not res.get('domain', {}):
            res['domain'] = {}
        res['domain']['journal_id'] = self.payment_type == 'inbound' and [('at_least_one_inbound', '=', True)] or [('at_least_one_outbound', '=', True)]
        context = self._context
        if 'default_journal_type' in context:
            res['domain']['journal_id'].append(('type','=', context['default_journal_type']))
            
        else:
            res['domain']['journal_id'].append(('type', 'in', ('bank', 'cash')))
        #res['domain']['journal_id'].append(('type', 'in', ('bank', 'cash')))
        return res


    @api.one
    @api.depends('invoice_ids', 'payment_type', 'partner_type', 'partner_id', 'payment_account_id')
    def _compute_destination_account_id(self):
        if self.invoice_ids:
            self.destination_account_id = self.invoice_ids[0].account_id.id
        elif self.payment_type == 'transfer':
            if not self.company_id.transfer_account_id.id:
                raise UserError(_('Transfer account not defined on the company.'))
            self.destination_account_id = self.company_id.transfer_account_id.id
        elif self.partner_id:
            if self.partner_type == 'customer':
                self.destination_account_id = self.partner_id.property_account_receivable_id.id
            else:
                self.destination_account_id = self.partner_id.property_account_payable_id.id
        elif self.payment_account_id:
            self.destination_account_id = self.payment_account_id.id
        

    @api.onchange('payment_account_id')
    def _onchange_payment_account_id(self):
        if self.payment_to == 'account' and self.payment_account_id:
            # property_account_payable_id
            partners = self.env['res.partner'].search(
                [('property_account_payable_id', '=', self.payment_account_id.id)])
            if partners:
                self.partner_id = partners[0].id

    @api.onchange('partner_type')
    def _onchange_partner_type(self):
        # Set partner_id domain
        if self.partner_type:
            self.partner_id = []
            if self.partner_type == 'customer':
                return {'domain': {'partner_id': [('customer', '=', True)]}}
            elif self.partner_type == 'supplier':
                return {'domain': {'partner_id': [('supplier', '=', True)]}}

    @api.onchange('payment_to')
    def _onchange_payment_to(self):
        # Set partner_id domain
        if self.payment_to:
            self.partner_id = []
            if self.payment_to == 'customer':
                # self.partner_type = 'customer'
                return {'value': {'partner_type': 'customer','payment_account_id': False}}
            elif self.payment_to == 'supplier':
                # self.partner_type = 'supplier'
                return {'value': {'partner_type': 'supplier', 'payment_account_id': False}}
            elif self.payment_to == 'employee':
                return {'value': {'partner_type': 'supplier', 'payment_account_id': False}}
            else:
                return {'value': {'partner_type': False, 'partner_id': False, 'payment_account_id': False}}

    @api.multi
    def post_for_invoices(self):
        if self.bill_payment and self.partner_type == 'customer':
            invoices = self.env['account.invoice'].search([('partner_id', '=', self.partner_id.id),
                                                           ('state', '=', 'open')], order='date_invoice asc')
            all_invoices = []
            total = 0
            found_invoice = False
            for inv in invoices:
                total += inv.residual
                if total <= self.amount:
                    found_invoice = True
                    all_invoices.append(inv)
                elif total > self.amount:
                    all_invoices.append(inv)
                    break

            if all_invoices:
                if self.invoice_ids:
                    self.invoice_ids = [(6, 0, [])]
                for line in all_invoices:
                    self.invoice_ids = [(4, line.id, None)]

        return self.post()

    @api.multi
    def post(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconciliable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconciliable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        for rec in self:

            if rec.state != 'draft':
                raise UserError(
                    _("Only a draft payment can be posted. Trying to post a payment in state %s.") % rec.state)
            if rec.bill_payment:
                if any(inv.state != 'open' for inv in rec.invoice_ids):
                    raise ValidationError(_("The payment cannot be processed because the invoice is not open!"))

            # Use the right sequence to set the name
            if rec.payment_type == 'transfer':
                sequence_code = 'account.payment.transfer'
            else:
                if rec.payment_to == 'account':
                    sequence_code = 'account.payment.direct'
                else:
                    if rec.partner_type == 'customer':
                        if rec.payment_type == 'inbound':
                            sequence_code = 'account.payment.customer.invoice'
                        if rec.payment_type == 'outbound':
                            sequence_code = 'account.payment.customer.refund'
                    if rec.partner_type == 'supplier':
                        if rec.payment_type == 'inbound':
                            sequence_code = 'account.payment.supplier.refund'
                        if rec.payment_type == 'outbound':
                            sequence_code = 'account.payment.supplier.invoice'
            if 'Draft' in rec.name:
                rec.name = self.env['ir.sequence'].with_context(ir_sequence_date=rec.payment_date).next_by_code(
                    sequence_code)

            # Create the journal entry
            amount = rec.amount * (rec.payment_type in ('outbound', 'transfer') and 1 or -1)
            move = rec._create_payment_entry(amount)

            # In case of a transfer, the first journal entry created debited the source liquidity account and credited
            # the transfer account. Now we debit the transfer account and credit the destination liquidity account.
            if rec.payment_type == 'transfer':
                transfer_credit_aml = move.line_ids.filtered(
                    lambda r: r.account_id == rec.company_id.transfer_account_id)
                transfer_debit_aml = rec._create_transfer_entry(amount)
                (transfer_credit_aml + transfer_debit_aml).reconcile()

            rec.write({'state': 'posted', 'move_name': move.name})

    def _create_payment_entry(self, amount):
        """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
            Return the journal entry.
        """
        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        invoice_currency = False
        if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
            # if all the invoices selected share the same currency, record the paiement in that currency too
            invoice_currency = self.invoice_ids[0].currency_id
        debit, credit, amount_currency, currency_id = aml_obj.with_context(
            date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id,
                                                          invoice_currency)

        move = self.env['account.move'].create(self._get_move_vals())
        # Write line corresponding to invoice payment
        counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, move.id, False)
        counterpart_aml_dict.update(self._get_counterpart_move_line_vals(self.invoice_ids))
        counterpart_aml_dict.update({'currency_id': currency_id})
        counterpart_aml = aml_obj.create(counterpart_aml_dict)
        if self.bill_payment:
            # Reconcile with the invoices
            if self.payment_difference_handling == 'reconcile' and self.payment_difference:
                writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
                amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(
                    self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
                # the writeoff debit and credit must be computed from the invoice residual in company currency
                # minus the payment amount in company currency, and not from the payment difference in the payment currency
                # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
                total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
                total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(
                    self.amount, self.company_id.currency_id)
                if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                    amount_wo = total_payment_company_signed - total_residual_company_signed
                else:
                    amount_wo = total_residual_company_signed - total_payment_company_signed
                # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
                # amount in the company currency
                if amount_wo > 0:
                    debit_wo = amount_wo
                    credit_wo = 0.0
                    amount_currency_wo = abs(amount_currency_wo)
                else:
                    debit_wo = 0.0
                    credit_wo = -amount_wo
                    amount_currency_wo = -abs(amount_currency_wo)
                writeoff_line['name'] = _('Counterpart')
                writeoff_line['account_id'] = self.writeoff_account_id.id
                writeoff_line['debit'] = debit_wo
                writeoff_line['credit'] = credit_wo
                writeoff_line['amount_currency'] = amount_currency_wo
                writeoff_line['currency_id'] = currency_id
                writeoff_line = aml_obj.create(writeoff_line)
                if counterpart_aml['debit']:
                    counterpart_aml['debit'] += credit_wo - debit_wo
                if counterpart_aml['credit']:
                    counterpart_aml['credit'] += debit_wo - credit_wo
                counterpart_aml['amount_currency'] -= amount_currency_wo
            self.invoice_ids.register_payment(counterpart_aml)

        # Write counterpart lines
        if not self.currency_id != self.company_id.currency_id:
            amount_currency = 0
        liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
        liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
        aml_obj.create(liquidity_aml_dict)

        move.post()
        return move

    def _get_counterpart_move_line_vals(self, invoice=False):
        if self.payment_type == 'transfer':
            name = self.name
        else:
            if self.bill_payment:
                name = ''
                if self.partner_type == 'customer':
                    if self.payment_type == 'inbound':
                        name += _("Customer Payment")
                    elif self.payment_type == 'outbound':
                        name += _("Customer Refund")
                elif self.partner_type == 'supplier':
                    if self.payment_type == 'inbound':
                        name += _("Vendor Refund")
                    elif self.payment_type == 'outbound':
                        name += _("Vendor Payment")
                if invoice:
                    name += ': '
                    for inv in invoice:
                        if inv.move_id:
                            name += inv.number + ', '
                    name = name[:len(name) - 2]
            else:
                name = _('سداد دفعة من الحساب')
        return {
            'name': name,
            'account_id': self.destination_account_id.id,
            'journal_id': self.journal_id.id,
            'currency_id': self.currency_id != self.company_id.currency_id and self.currency_id.id or False,
            'payment_id': self.id,
        }

    def _get_liquidity_move_line_vals(self, amount):
        vals = super(AccountPayment, self)._get_liquidity_move_line_vals(amount)
        vals.update({
            'partner_id': self.journal_id.partner_id and self.journal_id.partner_id.id or False,
        })

        return vals

    def _get_shared_move_line_vals(self, debit, credit, amount_currency, move_id, invoice_id=False):
        """ Returns values common to both move lines (except for debit, credit and amount_currency which are reversed)
        """
        vals = super(AccountPayment, self)._get_shared_move_line_vals(debit, credit, amount_currency, move_id,
                                                                      invoice_id)
        if self.payment_type == 'transfer':
            partner_id = self.journal_id.partner_id and self.journal_id.partner_id.id or False
            vals.update({
                'partner_id': self.journal_id.partner_id and self.journal_id.partner_id.id or False,
            })

        return vals

    def _create_transfer_entry(self, amount):
        """ Create the journal entry corresponding to the 'incoming money' part of an internal transfer, return the reconciliable move line
        """
        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        debit, credit, amount_currency, dummy = aml_obj.with_context(date=self.payment_date).compute_amount_fields(
            amount, self.currency_id, self.company_id.currency_id)
        amount_currency = self.destination_journal_id.currency_id and self.currency_id.with_context(
            date=self.payment_date).compute(amount, self.destination_journal_id.currency_id) or 0

        dst_move = self.env['account.move'].create(self._get_move_vals(self.destination_journal_id))

        dst_liquidity_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, dst_move.id)
        dst_liquidity_aml_dict.update({
            'name': _('Transfer from %s') % self.journal_id.name,
            'account_id': self.destination_journal_id.default_credit_account_id.id,
            'currency_id': self.destination_journal_id.currency_id.id,
            'payment_id': self.id,
            'journal_id': self.destination_journal_id.id,
            'partner_id': self.destination_journal_id.partner_id and self.destination_journal_id.partner_id.id or False})
        aml_obj.create(dst_liquidity_aml_dict)

        transfer_debit_aml_dict = self._get_shared_move_line_vals(credit, debit, 0, dst_move.id)
        transfer_debit_aml_dict.update({
            'name': self.name,
            'payment_id': self.id,
            'account_id': self.company_id.transfer_account_id.id,
            'journal_id': self.destination_journal_id.id,
            'partner_id': self.destination_journal_id.partner_id and self.destination_journal_id.partner_id.id or False})
        if self.currency_id != self.company_id.currency_id:
            transfer_debit_aml_dict.update({
                'currency_id': self.currency_id.id,
                'amount_currency': -self.amount,
            })
        transfer_debit_aml = aml_obj.create(transfer_debit_aml_dict)
        dst_move.post()
        return transfer_debit_aml

