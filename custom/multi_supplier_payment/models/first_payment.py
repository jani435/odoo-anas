# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError


class first_payment(models.Model):
    _inherit = "account.first_payment"
    _description = "first payment"
    _order = "payment_date desc, name desc"

    payment_ids = fields.One2many('account.payment', 'first_payment_id', string='Payments', copy=False)

    invoice_ids = fields.One2many('account.invoice', 'first_payment_id', string='Ivoices', copy=False)

    line_ids = fields.One2many('account.first_payment.line', 'payment_id', string='Opening Balance Details')
    sec_line_ids = fields.One2many('account.first_payment.line', 'payment_id', string='Opening Balance Details')
    multi_supplier = fields.Boolean('Multi Supplier Payment', default=False)

    journal_id = fields.Many2one('account.journal', string='Payment Journal', compute='get_journal',store=True)
    partner_type = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor'),('account', 'Account')]) ###
    note = fields.Text('Note')
    move_line_id = fields.Char(string='Move id')

    @api.one
    @api.depends('payment_type')
    def get_journal(self):
        jour = self.env['account.journal'].search([('code','=','OPB'),('type','=','general')])
        self.journal_id = jour.id
        return self.journal_id

    @api.one
    @api.constrains('amount')
    def _check_amount(self):
        if not self.amount > 0.0:
            raise ValidationError(
                _('The payment amount must be strictly positive.'))

    @api.onchange('journal_id')
    def _onchange_journal(self):
        if self.journal_id:
            self.currency_id = self.journal_id.currency_id or self.company_id.currency_id
            # Set default payment method (we consider the first to be the
            # default one)
            payment_methods = self.payment_type == 'inbound' and self.journal_id.inbound_payment_method_ids or self.journal_id.outbound_payment_method_ids
            self.payment_method_id = payment_methods and payment_methods[0] or False
            # Set payment method domain (restrict to methods enabled for the
            # journal and to selected payment type)
            payment_type = self.payment_type in (
                'outbound', 'transfer') and 'outbound' or 'inbound'
            return {'domain': {'payment_method_id': [('payment_type', '=', payment_type), ('id', 'in', payment_methods.ids)]}}
        return {}

    @api.onchange('partner_type')
    def _onchange_partner_type(self):
        # Set partner_id domain
        if self.partner_type:
            return {'domain': {'partner_id': [(self.partner_type, '=', True)]}, 'value': {'line_ids': False}}

    @api.onchange('payment_type')
    def _onchange_payment_type(self):
        if self.payment_type == 'inbound':
            self.partner_type = 'customer'
        elif self.payment_type == 'outbound':
            self.partner_type = 'supplier'
        res = self._onchange_journal()
        if not res.get('domain', {}):
            res['domain'] = {}
        
        return res
    
    @api.multi
    def multi_to_draft(self):
        for rec in self:
            # if rec.payment_ids:
            #     for payment in rec.payment_ids:
            #         payment.cancel()
            #         payment.move_name = ''
            #         payment.unlink()
            # if rec.invoice_ids:
            #     now = datetime.now().date()
            #     for invoice in rec.invoice_ids:
            #         #move_id = invoice.move_id
            #         #invoice.write({'state': 'cancel', 'move_id': False})
            #         #move_id.button_cancel()
            #         #move_id.unlink()
            #         invoice.action_invoice_cancel()
            #         invoice.action_invoice_draft()
            #         invoice.write({'state': 'draft','move_name':False})
            #         invoice.unlink()
            rec.account_move = self.env['account.move'].search([('id','=',rec.move_line_id)])
            rec.account_move.unlink()
            rec.write({'state': 'draft'})

    @api.one
    @api.multi
    def _check_lines(self):
        if self.multi_supplier:
            if not self.line_ids:
                raise ValidationError(_('You can not confirm first payment without first payment details'))

            total_amount = 0.0
            for line in  self.line_ids:
                total_amount += line.amount
                if (line.amount < 0.0):
                    raise ValidationError(_('The payment amount for the partner %s must be strictly positive.')%(line.partner_id.name))
            

            self.amount = total_amount


    @api.model
    def create(self,vals):
        jour = self.env['account.journal'].search([('code','=','OPB'),('type','=','general')])
        vals['name']= jour and jour.sequence_id.next_by_id() or False
        return super(first_payment,self).create(vals)

    @api.multi
    def multi_post(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconciliable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconciliable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        for rec in self:
            # to check payment amount in lines
            rec._check_lines()

            if rec.state != 'draft':
                raise UserError(
                    _("Only a draft payment can be posted. Trying to post a payment in state %s.") % rec.state)

            else:
                data_move = []
                account = 0
                am_obj = self.env['account.move']
                if rec.partner_type == 'account':
                    if rec.payment_type == 'inbound': ##credit
                        for line in rec.sec_line_ids:
                            account = line.account_id.id
                            data_move.append((0, 0, {
                                    'account_id': account,
                                    'name': line.note,
                                    'credit': line.amount,
                                }))
                    
                        data_move.append((0, 0, {
                            'account_id': rec.journal_id.default_debit_account_id.id,
                            'name': rec.note,
                            'debit': rec.amount,
                        }))
                            
                    if rec.payment_type == 'outbound': ##credit
                        for line in rec.line_ids:
                            account = line.account_id.id
                            data_move.append((0, 0, {
                                    'account_id': account,
                                    'name': line.note,
                                    'debit': line.amount,
                                    }))
                
                        data_move.append((0, 0, {
                            'account_id': rec.journal_id.default_debit_account_id.id,
                            'name': rec.note,
                            'credit': rec.amount,
                        }))
                else:
                    if rec.payment_type == 'inbound': ##credit
                        for line in rec.line_ids:
                            if rec.partner_type == 'customer':
                                account = line.partner_id.property_account_receivable_id.id
                            elif rec.partner_type == 'supplier':
                                account = line.partner_id.property_account_payable_id.id
                            data_move.append((0, 0, {
                                    'account_id': account,
                                    'name': line.note,
                                    'credit': line.amount,
                                    'partner_id': line.partner_id.id,
                                }))
                    
                        data_move.append((0, 0, {
                            'account_id': rec.journal_id.default_debit_account_id.id,
                            'name': rec.note,
                            'debit': rec.amount,
                        }))
                            
                    if rec.payment_type == 'outbound': ##credit
                        for line in rec.line_ids:
                            if rec.partner_type == 'customer':
                                account = line.partner_id.property_account_receivable_id.id
                            if rec.partner_type == 'supplier':
                                account = line.partner_id.property_account_payable_id.id
                            data_move.append((0, 0, {
                                    'account_id': account,
                                    'name': line.note,
                                    'debit': line.amount,
                                    'partner_id': line.partner_id.id,
                                    }))
                
                        data_move.append((0, 0, {
                            'account_id': rec.journal_id.default_debit_account_id.id,
                            'name': rec.note,
                            'credit': rec.amount,
                        }))
                
                
                account_move_id = am_obj.sudo().create({
                'date': rec.payment_date,
                'journal_id': rec.journal_id.id,
                'name': rec.name,
                'ref': rec.name,
                'state': 'draft',
                'line_ids': data_move,
                })

                # post journal entry
                account_move_id.post()

                #print account_move_id.name

                rec.write({'state': 'posted', 'move_line_id': account_move_id.id})
                #rec.write({'state': 'posted'})




    @api.multi
    def button_multi_payment(self):
        return {
            'name': _('Payments'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.payment',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('first_payment_id', 'in', self.ids)],
        }

    
    @api.multi
    def button_multi_invoice(self):
        ir_model_data = self.env['ir.model.data']
        return {
            'name': _('invoices'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('first_payment_id', 'in', self.ids)],
	    'target':'current',
	    'views':[(ir_model_data.get_object_reference('account', 'invoice_tree')[1],'tree'),(ir_model_data.get_object_reference('vegetables', 'vegetables_invoice_customer_form')[1],'form')] or [],
        }


class AccountPaymentLine(models.Model):
    _name = 'account.first_payment.line'


    @api.depends('payment_id.line_ids')
    def _check(self):
        for rec in self:
            rec.check = False
            if rec.id and rec.linked_with_invoices:
                rec.check = True

    @api.depends('payment_invoice_ids', 'payment_invoice_ids.amount')
    def _total_lines_amount(self):
        amount = 0
        for rec in self:
            amount = 0
            for line in rec.payment_invoice_ids:
                amount += line.amount
            rec.lines_amount = amount
       

    payment_id = fields.Many2one('account.first_payment',string='Opening Balance')
    partner_id = fields.Many2one('res.partner',string='Partner')
    account_id = fields.Many2one('account.account',string='Account')
    amount = fields.Monetary(string='Payment Amount')
    company_id = fields.Many2one('res.company', related='payment_id.company_id', string='Company', readonly=True)
    currency_id = fields.Many2one('res.currency',related='payment_id.currency_id', string='Currency', readonly=True)
    partner_type = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor'),('account', 'Account')], string='Partner Type')
    note = fields.Char('Note And Other Details')
    state = fields.Selection(selection=[
        ('draft', 'Draft'),
        ('posted', 'Posted')], related='payment_id.state', string="State")

    @api.multi
    def action_delete(self):
	for rec in self:
		rec.unlink()

    @api.one
    @api.constrains('amount','payment_id.amount')
    def _check_amount(self):
        
        if (self.amount < 0.0) or (not self.amount > 0.0 and self.payment_id.state != 'draft'):
            raise ValidationError('The payment amount for the partner %s must be strictly positive.',(self.partner_id.name))

    # @api.onchange('amount','payment_id')
    # def _onchange_amount(self):
    #     print self.payment_id.sec_line_ids 
    #     for rec in self.payment_id.sec_line_ids:
    #         amount = 0
    #         print '************************ ',rec.amount
    #         #self.payment_id.write({'amount': rec.amount})

    @api.onchange('partner_type')
    def _onchange_partner_type(self):
        # Set partner_id domain
        vals = {}
        if self.partner_type:
            self.partner_id = []
            if self.partner_type == 'customer':
                return {'domain': {'partner_id': [('customer', '=', True)]}}
            if self.partner_type == 'supplier':
                return {'domain': {'partner_id': [('supplier', '=', True)]}}


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    first_payment_id = fields.Many2one('account.first_payment', string='Payment', copy=False)


class account_invoice(models.Model):
    _inherit = "account.invoice"  

    first_payment_id = fields.Many2one('account.first_payment', string='Payment', copy=False)

    
