# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from lxml import etree
import time
from odoo.osv.orm import setup_modifiers

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(AccountPayment, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
	account_ids = []
	partner_ids= self.env['res.partner'].search([])
	for partner in partner_ids:
		if partner.property_account_payable_id and partner.property_account_payable_id.id not in account_ids:
			account_ids.append(partner.property_account_payable_id.id)
		if partner.property_account_receivable_id and partner.property_account_receivable_id.id not in account_ids:
			account_ids.append(partner.property_account_receivable_id.id)
	if view_type =='form':
		if res['fields'].get('sec_line_ids',False):
			doc = etree.XML(res['fields']['sec_line_ids']['views']['tree']['arch'])
			for node in doc.xpath("//field[@name='account_id']"):
				domain = [('id', 'not in', account_ids)]
				node.set('domain', str(domain))
			res['fields']['sec_line_ids']['views']['tree']['arch'] = etree.tostring(doc)
		if res['fields'].get('part_ids',False):
			doc = etree.XML(res['fields']['part_ids']['views']['tree']['arch'])
			for node in doc.xpath("//field[@name='account_id']"):
				domain = [('id', 'not in', account_ids)]
				node.set('domain', str(domain))
			res['fields']['part_ids']['views']['tree']['arch'] = etree.tostring(doc)



		#emp_list = []
		#employee_ids = self.env['hr.employee'].search([])
		#for emp in employee_ids:
		#	if emp.partner_id and emp.partner_id.id not in emp_list:
		#		emp_list.append(emp.partner_id.id)
		#if self.payment_to == "employee":
		#	if res['fields'].get('line_ids',False):
		#		doc = etree.XML(res['fields']['line_ids']['views']['tree']['arch'])
		#		for node in doc.xpath("//field[@name='partner_id']"):
		#			domain = [('id', 'in', emp_list)]
		#			node.set('domain', str(domain))
		#		res['fields']['line_ids']['views']['tree']['arch'] = etree.tostring(doc)

		#else:
		#	if res['fields'].get('line_ids',False):
		#		doc = etree.XML(res['fields']['line_ids']['views']['tree']['arch'])
		#		for node in doc.xpath("//field[@name='partner_id']"):
		#			domain = [('id', 'not in', emp_list)]
		#			node.set('domain', str(domain))
		#		res['fields']['line_ids']['views']['tree']['arch'] = etree.tostring(doc)
	if view_type =='form':
		if not self.env.user.has_group('vegetables.history_access_group'):
			doc = etree.XML(res['arch'])
			for node in doc.xpath("//field[@name='message_ids']"):
				node.set('invisible', "1")
				setup_modifiers(node, res['fields']['message_ids'])
			for node in doc.xpath("//field[@name='message_follower_ids']"):
				node.set('invisible', "1")
				setup_modifiers(node, res['fields']['message_follower_ids'])
			res['arch'] = etree.tostring(doc)


        return res

class DepitCreditPart(models.Model):
    _inherit = 'account.payment.part'


    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
	if self.partner_id:
		if self.partner_id.property_account_receivable_id and (self.partner_id.customer) and (not self.partner_id.supplier):
		    self.account_id=self.partner_id.property_account_receivable_id.id
		elif self.partner_id.property_account_payable_id and (not self.partner_id.customer) and (self.partner_id.supplier):
		        self.account_id= self.partner_id.property_account_payable_id.id
		else:
			self.account_id= False
	else:
		self.account_id= False

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
	if self.partner_id:
		if self.partner_id.property_account_receivable_id and (self.partner_id.customer) and (not self.partner_id.supplier):
		    self.account_id=self.partner_id.property_account_receivable_id.id
		elif self.partner_id.property_account_payable_id and (not self.partner_id.customer) and (self.partner_id.supplier):
		        self.account_id= self.partner_id.property_account_payable_id.id
		else:
			self.account_id= False
	else:
		self.account_id= False

class AccountMove(models.Model):
    _inherit = 'account.move'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(AccountMove, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
	account_ids = []
	partner_ids= self.env['res.partner'].search([])
	for partner in partner_ids:
		if partner.property_account_payable_id and partner.property_account_payable_id.id not in account_ids:
			account_ids.append(partner.property_account_payable_id.id)
		if partner.property_account_receivable_id and partner.property_account_receivable_id.id not in account_ids:
			account_ids.append(partner.property_account_receivable_id.id)
	if view_type =='form':
		if res['fields'].get('line_ids',False):
			doc = etree.XML(res['fields']['line_ids']['views']['tree']['arch'])
			for node in doc.xpath("//field[@name='account_id']"):
				domain = [('id', 'not in', account_ids)]
				node.set('domain', str(domain))
			res['fields']['line_ids']['views']['tree']['arch'] = etree.tostring(doc)
        return res


