# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime

statee =[('customer', 'Customer'), ('supplier', 'Vendor'), ('account', 'Direct Account')]#, ('employee', 'Employee')
class inheritpartner_rel(models.Model):
    _inherit = 'res.partner'

    journal_in_bnk_id = fields.Many2one('account.journal', string='Payment in bank Journal', domain=[('type', '=', 'bank')])
    journal_in_csh_id = fields.Many2one('account.journal', string='Payment in cash Journal',domain=[('type', '=', 'cash')])
    journal_out_bnk_id = fields.Many2one('account.journal', string='Payment out bank Journal',domain=[('type', '=', 'bank')])
    journal_out_csh_id = fields.Many2one('account.journal', string='Payment out cash Journal',domain=[('type', '=', 'cash')])
    payment_to_in_bnk = fields.Selection(statee,string='Payment To in bank', default='customer')
    payment_to_in_cash = fields.Selection(statee, string='Payment To in cash', default='customer')
    payment_to_out_bnk = fields.Selection(statee, string='Payment To in bank', default='customer')
    payment_to_out_cash = fields.Selection(statee, string='Payment To in cash', default='customer')
    payment_method_bnk_out_id = fields.Many2one('account.payment.method', string='Payment Method Type bank out', oldname="payment_method")


class DepitCreditPart(models.Model):
    _name = 'account.payment.part'

    @api.multi
    def write(self, vals):
	mess_obj= self.env['mail.message']
	a_obj= self.env['account.account']
	e_obj= self.env['hr.employee']
	partner_obj= self.env['res.partner']
        message = _("This Line has been modified: <a href=# data-oe-model=account.payment.part data-oe-id=%d>%s</a>") % (self.id, self.id)
	#print "   EEEE   ",vals
        if vals:
		mess_ids= mess_obj.search([('model','=','account.payment'),('res_id','=',self.payment_id.id),('parent_id','=',False)],limit=1)
		dct = {'body': message, 'model': 'account.payment', 'attachment_ids': [], 'res_id': self.payment_id.id, 'parent_id': mess_ids and mess_ids[0].id or False, 'subtype_id': mess_ids and mess_ids[0].subtype_id.id or False, 'author_id': self.env.user.partner_id.id, 'message_type': 'notification', 'partner_ids': [], 'subject': False}
		lst = []
		if vals.get('account_id',False):
			nproduct= a_obj.browse(vals.get('account_id',False))
			lst.append([0, 0, {'field': 'account_id', 'field_desc': u'Account', 'field_type': 'many2one', 'new_value_char': nproduct.display_name, 'old_value_char': self.account_id.display_name, 'new_value_integer': vals.get('account_id',False), 'old_value_integer': self.account_id.id}])
		if vals.get('partner_id',False):
			ncustomer= partner_obj.browse(vals.get('partner_id',False))
			lst.append([0, 0, {'field': 'partner_id', 'field_desc': u'Partner', 'field_type': 'many2one', 'new_value_char': ncustomer.display_name, 'old_value_char': self.partner_id.display_name, 'new_value_integer': vals.get('partner_id',False), 'old_value_integer': self.partner_id.id}])
		if vals.get('label',False):
			lst.append([0, 0, {'field': 'label', 'field_desc': u'Label', 'field_type': 'char', 'new_value_char': vals.get('label',False), 'old_value_char': self.label}])
		if vals.get('amount',False):
			lst.append([0, 0, {'field': 'amount', 'field_desc': u'Payment Amount', 'field_type': 'monetary', 'new_value_monetary': vals.get('amount',False), 'old_value_monetary': self.amount}])
		if vals.get('part_type'):
			lst.append([0, 0, {'field': 'part_type', 'field_desc': u'Type', 'field_type': 'selection', 'new_value_char': vals.get('part_type',False), 'old_value_char': self.part_type}])
		if lst:
			dct.update({'tracking_value_ids':lst})
			mess_obj.create(dct)
			mess_obj.create({'body': message, 'model': 'account.payment', 'attachment_ids': [], 'res_id': self.payment_id.id, 'parent_id': mess_ids and mess_ids[0].id or False, 'subtype_id': mess_ids and mess_ids[0].subtype_id.id or False, 'author_id': self.env.user.partner_id.id, 'message_type': 'notification', 'partner_ids': [], 'subject': False})
        res = super(DepitCreditPart, self).write(vals)		        
        return res
       
    

    payment_id = fields.Many2one('account.payment',string='Payment')
    partner_id = fields.Many2one('res.partner',string='Partner')
    account_id = fields.Many2one('account.account', string='Account')
    part_type = fields.Selection([('debit','Debit Part'),('credit','Credit Part')], string='Type')
    currency_id = fields.Many2one('res.currency',related='payment_id.currency_id', string='Currency', readonly=True)
    amount = fields.Monetary(string='Amount')
    #payment_to = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor'), ('account', 'Direct Account')],
    #                              string='Payment To')
    label = fields.Char(string='Label')


    
class AccountPaymentInvoice(models.Model):
    _name = 'account.payment.line.invoice'


    @api.one
    @api.depends('invoice_id', 'amount', 'payment_line_id.payment_id.payment_date', 'currency_id')
    def _compute_payment_difference(self):
        if not self.invoice_id:
            return
        if self.invoice_id.type in ['in_invoice', 'out_refund']:
            self.payment_difference = self.amount - self._compute_total_invoices_amount()
        else:
            self.payment_difference = self._compute_total_invoices_amount() - self.amount



    def _compute_total_invoices_amount(self):
        """ Compute the sum of the residual of invoices, expressed in the payment currency """
        payment_currency = self.payment_line_id.currency_id or self.payment_line_id.payment_id.journal_id.currency_id or self.payment_line_id.payment_id.journal_id.company_id.currency_id or self.env.user.company_id.currency_id
        #invoices = self._get_invoices()

        if self.invoice_id.currency_id == payment_currency:
            total = self.invoice_id.residual_signed
        else:
            total = 0
            #for inv in invoices:
            if self.invoice_id.company_currency_id != payment_currency:
                total += self.invoice_id.company_currency_id.with_context(date=self.payment_line_id.payment_id.payment_date).compute(self.invoice_id.residual_company_signed, payment_currency)
            else:
                total += self.invoice_id.residual_company_signed
        return abs(total)

    

    payment_line_id = fields.Many2one('account.payment.line',string='Payment Line', ondelete='cascade')
    payment_id = fields.Many2one('account.payment',string='Payment')
    invoice_id = fields.Many2one('account.invoice',string='Invoice')
    partner_id = fields.Many2one('res.partner',string='Partner')
    #account_id = fields.Many2one('account.account', string='Payment Account')
    #partner_id = fields.Many2one('res.partner',related='invoice_id.partner_id', string='Partner', store=True)
    currency_id = fields.Many2one('res.currency',related='payment_line_id.currency_id', string='Currency', readonly=True)
    amount = fields.Monetary(string='Payment Amount', readonly=True)
    #residual = fields.Monetary(string='Invoice Residual',related='invoice_id.residual_signed', readonly=True, store=True)
    residual = fields.Monetary(string='Invoice Residual', readonly=True)
    payment_difference = fields.Monetary(compute='_compute_payment_difference', readonly=True, store=True)
    payment_difference_handling = fields.Selection([('open', 'Keep open'), ('reconcile', 'Mark invoice as fully paid')], default='open', string="Payment Difference", copy=False)
    writeoff_account_id = fields.Many2one('account.account', string="Difference Account", domain=[('deprecated', '=', False)], copy=False)
    linked_with_invoices = fields.Boolean(string='Payment Linked With Invoices', default=False)
    


    @api.onchange('invoice_id')
    def _onchange_invoice_id(self):
        # Set partner_id domain
        self.residual = 0.0
        if self.invoice_id:
            self.residual = self.invoice_id.residual_signed
            self.amount = self.invoice_id.residual_signed

    @api.multi
    def write(self,vals):
        #res = super(AccountPaymentInvoice,self).write(vals)
        for rec in self:
            invoice_id = 'invoice_id' in vals and vals['invoice_id'] or rec.invoice_id.id
            vals['residual'] = self.env['account.invoice'].browse(invoice_id).residual_signed
            res = super(AccountPaymentInvoice,self).write(vals)

        return True

    @api.multi
    def create(self, vals):
        vals['residual'] =  self.env['account.invoice'].browse(vals['invoice_id']).residual_signed

        res = super(AccountPaymentInvoice,self).create(vals)



class AccountPaymentLine(models.Model):
    _name = 'account.payment.line'
    _order = 'sequence,id'


    @api.multi
    def action_delete(self):
	for rec in self:
		rec.unlink()

    @api.depends('payment_id.line_ids')
    def _check(self):
        for rec in self:
            rec.check = False
            if rec.id and rec.linked_with_invoices:
                rec.check = True

    @api.depends('payment_invoice_ids', 'payment_invoice_ids.amount')
    def _total_lines_amount(self):
        amount = 0
        for rec in self:
            amount = 0
            for line in rec.payment_invoice_ids:
                amount += line.amount
            rec.lines_amount = amount

    @api.depends('payment_to', 'partner_id', 'payment_id.payment_type', 'account_id')
    def _account_balance(self):
        amount = 0
        for rec in self:
            amount = 0.0
            account = False
            if rec.payment_to == 'customer':
                account = rec.partner_id and rec.partner_id.property_account_receivable_id.id or False
            elif rec.payment_to == 'supplier' or rec.payment_to == 'employee':
                account = rec.partner_id and rec.partner_id.property_account_payable_id.id or False
            else:
                account = rec.account_id and rec.account_id.id or False
            where = ' m.account_id = %s' % str(account)
            if rec.partner_id:
                where += ' and m.partner_id = %s' % str(rec.partner_id.id)
            if account :
                self._cr.execute(''' select round(sum(m.debit),2) as debit, 
                                        round(sum(m.credit),2) as credit, 
                                        round(sum(m.debit) - sum(m.credit) ,2) as balance 
                                    from account_move_line m , account_move v
                                where m.move_id = v.id and v.state = 'posted' and ''' + where )
                res =  self._cr.dictfetchall()
                if res :
                    amount = res[0]['balance']
            rec.account_amount = amount

    @api.multi
    @api.depends('payment_id','payment_id.payment_to')
    def get_partners(self):
	p_obj= self.env['res.partner']
	for rec in self:
		if rec.payment_id.payment_to == 'supplier':
			p_ids= (p_obj.search([('supplier','=',True)])).ids
			rec.partners_ids = [(6,0,p_ids)]
		elif rec.payment_id.payment_to == 'customer':
			p_ids= (p_obj.search([('customer','=',True)])).ids
			rec.partners_ids = [(6,0,p_ids)]
		elif rec.payment_id.payment_to == 'employee':
			emp_list = []
			employee_ids = self.env['hr.employee'].search([])
			for emp in employee_ids:
				if emp.partner_id and emp.partner_id.id not in emp_list:
					emp_list.append(emp.partner_id.id)
			rec.partners_ids = [(6,0,emp_list)]


		else:
			p_ids= (p_obj.search([])).ids
			rec.partners_ids = [(6,0,p_ids)]


    @api.multi
    def write(self, vals):
	mess_obj= self.env['mail.message']
	a_obj= self.env['account.account']
	e_obj= self.env['hr.employee']
	partner_obj= self.env['res.partner']
        message = _("This Line has been modified: <a href=# data-oe-model=account.payment.line data-oe-id=%d>%s</a>") % (self.id, self.id)
	#print "   EEEE   ",vals
        if vals:
		mess_ids= mess_obj.search([('model','=','account.payment'),('res_id','=',self.payment_id.id),('parent_id','=',False)],limit=1)
		dct = {'body': message, 'model': 'account.payment', 'attachment_ids': [], 'res_id': self.payment_id.id, 'parent_id': mess_ids and mess_ids[0].id or False, 'subtype_id': mess_ids and mess_ids[0].subtype_id.id or False, 'author_id': self.env.user.partner_id.id, 'message_type': 'notification', 'partner_ids': [], 'subject': False}
		lst = []
		if vals.get('account_id',False):
			nproduct= a_obj.browse(vals.get('account_id',False))
			lst.append([0, 0, {'field': 'account_id', 'field_desc': u'Account', 'field_type': 'many2one', 'new_value_char': nproduct.display_name, 'old_value_char': self.account_id.display_name, 'new_value_integer': vals.get('account_id',False), 'old_value_integer': self.account_id.id}])
		if vals.get('partner_id',False):
			ncustomer= partner_obj.browse(vals.get('partner_id',False))
			lst.append([0, 0, {'field': 'partner_id', 'field_desc': u'Partner', 'field_type': 'many2one', 'new_value_char': ncustomer.display_name, 'old_value_char': self.partner_id.display_name, 'new_value_integer': vals.get('partner_id',False), 'old_value_integer': self.partner_id.id}])
		if vals.get('sales_man_id',False):
			ncustomer= e_obj.browse(vals.get('sales_man_id',False))
			lst.append([0, 0, {'field': 'sales_man_id', 'field_desc': u'Partner', 'field_type': 'many2one', 'new_value_char': ncustomer.name, 'old_value_char': self.sales_man_id.name, 'new_value_integer': vals.get('sales_man_id',False), 'old_value_integer': self.sales_man_id.id}])
		if vals.get('linked_with_invoices'):
			lst.append([0, 0, {'field': 'linked_with_invoices', 'field_desc': u'Payment Linked With Invoices', 'field_type': 'boolean', 'new_value_boolean': vals.get('lablinked_with_invoicesel',False), 'old_value_boolean': self.linked_with_invoices}])

		if vals.get('label',False):
			lst.append([0, 0, {'field': 'label', 'field_desc': u'Label', 'field_type': 'char', 'new_value_char': vals.get('label',False), 'old_value_char': self.label}])
		if vals.get('amount',False):
			lst.append([0, 0, {'field': 'amount', 'field_desc': u'Payment Amount', 'field_type': 'monetary', 'new_value_monetary': vals.get('amount',False), 'old_value_monetary': self.amount}])
		if vals.get('Check_no',False):
			lst.append([0, 0, {'field': 'Check_no', 'field_desc': u'Check No', 'field_type': 'char', 'new_value_char': vals.get('Check_no',False), 'old_value_char': self.Check_no}])
		if vals.get('check_memo',False):
			lst.append([0, 0, {'field': 'check_memo', 'field_desc': u'Check Memo', 'field_type': 'char', 'new_value_char': vals.get('check_memo',False), 'old_value_char': self.check_memo}])
		if vals.get('check_date',False):
			lst.append([0, 0, {'field': 'check_memo', 'field_desc': u'Check Date', 'field_type': 'date', 'new_value_datetime': vals.get('check_date',False), 'old_value_datetime': self.check_date}])
		if vals.get('beneficiary_id',False):
			be_obj= self.env['check.beneficiaries']
			nproduct= be_obj.browse(vals.get('beneficiary_id',False))
			lst.append([0, 0, {'field': 'beneficiary_id', 'field_desc': u'Beneficiary', 'field_type': 'many2one', 'new_value_char': nproduct.name, 'old_value_char': self.beneficiary_id.name, 'new_value_integer': vals.get('beneficiary_id',False), 'old_value_integer': self.beneficiary_id.id}])

		if lst:
			dct.update({'tracking_value_ids':lst})
			mess_obj.create(dct)
			mess_obj.create({'body': message, 'model': 'account.payment', 'attachment_ids': [], 'res_id': self.payment_id.id, 'parent_id': mess_ids and mess_ids[0].id or False, 'subtype_id': mess_ids and mess_ids[0].subtype_id.id or False, 'author_id': self.env.user.partner_id.id, 'message_type': 'notification', 'partner_ids': [], 'subject': False})
        res = super(AccountPaymentLine, self).write(vals)		        
        return res
       

    partners_ids= fields.Many2many('res.partner', 'p_p_rel', 'p_ids', 'pa_ids', compute='get_partners', string='Partners')
    payment_id = fields.Many2one('account.payment',string='Payment')
    partner_id = fields.Many2one('res.partner',string='Partner')
    account_id = fields.Many2one('account.account', string='Payment Account')
    amount = fields.Monetary(string='Payment Amount')
    company_id = fields.Many2one('res.company', related='payment_id.company_id', string='Company', readonly=True)
    currency_id = fields.Many2one('res.currency',related='payment_id.currency_id', string='Currency', readonly=True)
    linked_with_invoices = fields.Boolean(string='Payment Linked With Invoices')
    payment_to = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor'), ('account', 'Direct Account')],
                                  string='Payment To')
    payment_invoice_ids = fields.One2many('account.payment.line.invoice','payment_line_id', string='Invoice details')
    state = fields.Selection(selection=[
        ('draft', 'Draft'),
        ('posted', 'Posted'),
        ('sent', 'Sent'),
        ('reconciled', 'Reconciled')], related='payment_id.state', string="State")
    lines_amount = fields.Monetary(string='Total Invoice Amount', compute=_total_lines_amount)

    check = fields.Boolean(string='Check', compute=_check)
    label = fields.Char(string='Label')
    account_amount = fields.Monetary(default=0.0, string='Account Balance', compute='_account_balance', store=True)
    sequence = fields.Integer(default=10,
        help="Gives the sequence of this line when displaying the payment.")

    @api.multi
    @api.onchange('payment_to')
    def onchange_payment_to_employee(self):
	emp_list = []
	employee_ids = self.env['hr.employee'].search([])
	# for emp in employee_ids:
	# 	if emp.partner_id and emp.partner_id.id not in emp_list:
	# 		emp_list.append(emp.partner_id.id)
	for rec in self:
		if rec.payment_to == "employee":
			return {'value':{'partner_id':False}}
		else:
			return {'value':{'partner_id':False}} 
	


    @api.one
    @api.constrains('amount','payment_id.state')
    def _check_amount(self):
        if (self.amount < 0.0) or (not self.amount > 0.0 and self.payment_id.state != 'draft'):
            raise ValidationError('The payment amount for the partner %s must be strictly positive.',(self.partner_id.name))

    @api.onchange('linked_with_invoices')
    def _onchange_linked_with_invoices(self):
        # Set partner_id domain
        if not self.linked_with_invoices:
            self.payment_invoice_ids = False

    @api.onchange('payment_to')
    def _onchange_payment_to(self):
        # Set partner_id domain
        vals = {}
        domain = {}
        context = self._context
        if self.payment_to:
            self.partner_id = []
            if self.payment_to == 'customer':
                domain = {'partner_id': [('customer', '=', True)]}
                if 'payment_type' in  context and context['payment_type'] == 'inbound':
                    vals = {'check': True}
                #return {'domain': {'partner_id': [('customer', '=', True)]}}
            if self.payment_to == 'supplier' or self.payment_to == 'employee':
                domain = {'partner_id': [('supplier', '=', True)]}
                if 'payment_type' in  context and context['payment_type'] == 'outbound':
                    vals = {'check': True}
                #return {'domain': {'partner_id': [('supplier', '=', True)]}}

        return {'value': vals}

        
        #if self.payment_id and self.payment_id.pa

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        # Set partner_id domain
        return {'value':{'payment_invoice_ids': False}}
        

    @api.multi
    def create_payment_invoice_line(self):
        # Set partner_id domain
        if self.linked_with_invoices and self.partner_id:
            invoice_ids = self.env['account.invoice'].search([('state','=','open'),('type','=','out_invoice'),('partner_id','=',self.partner_id.id),('company_id','=',self.company_id.id)])
            payment_invoice_ids = []
            for invoice in invoice_ids:
                vals = {
                    'invoice_id': invoice.id,
                    'payment_line_id': self.ids[0],
                    'residual': invoice.residual_signed,
                    'amount': 0.0,
                    #'payment_difference_handling':

                }
                self.env['account.payment.line.invoice'].create(vals)


    @api.multi
    def show_details(self):
        # TDE FIXME: does not seem to be used
        view_id = self.env.ref('multi_supplier_payment.payment_line_form').id
        if self.payment_id.payment_type == 'outbound':
            view_id = self.env.ref('multi_supplier_payment.pay_payment_line_form').id
        #if not self.payment_invoice_ids:
        #    self.create_payment_invoice_line()
        return {
            'name': _('Invoice Payment Details'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.payment.line',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'res_id': self.ids[0],
            'context': self.env.context}

    @api.multi
    def save(self):
        # TDE FIXME: does not seem to be used -> actually, it does
        # TDE FIXME: move me somewhere else, because the return indicated a wizard, in pack op, it is quite strange
        # HINT: 4. How to manage lots of identical products?
        # Create a picking and click on the Mark as TODO button to display the Lot Split icon. A window will pop-up. Click on Add an item and fill in the serial numbers and click on save button
        """for pack in self:
            if pack.product_id.tracking != 'none':
                pack.write({'qty_done': sum(pack.pack_lot_ids.mapped('qty'))})"""
        return {'type': 'ir.actions.act_window_close'}


class AccountPayment(models.Model):
    _name = 'account.payment'
    _inherit = ['account.payment','mail.thread']

    @api.depends('payment_type', 'journal_id')
    def _journal_balance(self):
        amount = 0
        for rec in self:
            amount = 0.0
            account = rec.payment_type in ('outbound','transfer') and (rec.journal_id.default_debit_account_id and rec.journal_id.default_debit_account_id.id or False) \
                     or (rec.journal_id.default_credit_account_id and rec.journal_id.default_credit_account_id.id or False)
            
            where = ' m.account_id = %s' % str(account)
            
            if account :
                self._cr.execute(''' select round(sum(m.debit),2) as debit, 
                                        round(sum(m.credit),2) as credit, 
                                        round(sum(m.debit) - sum(m.credit) ,2) as balance 
                                    from account_move_line m , account_move v
                                where m.move_id = v.id and v.state = 'posted' and ''' + where )
                res =  self._cr.dictfetchall()
                if res :
                    amount = res[0]['balance']
            rec.journal_amount = amount

    @api.multi
    @api.depends('amount')
    def _get_amount_1(self):
        for rec in self:
            rec.amount1 = rec.amount or 0.00

    @api.model
    def get_default_auType(self):
        print "LLLLLLLL***************************LLL"
        return 'supplier'

    def _default_get(self, cr, uid, context=None):
        print " This function called before new record create " 
        res = 'supplier'
        return res 

    _defaults = {
    'payment_to': _default_get, 
    }

    #bill_payment = fields.Boolean('Bill Payment', default=False)
    #payment_to = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor'),('account', 'Direct Account')], string='Payment To', default='customer')
    #partner_type = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor')])
    #payment_account_id = fields.Many2one('account.account', string='Payment Account')
    # Money flows from the journal_id's default_debit_account_id or default_credit_account_id to the destination_account_id
    #destination_account_id = fields.Many2one('account.account', compute='_compute_destination_account_id', readonly=True)
    multi_supplier = fields.Boolean('Multi Supplier Payment', default=False)
    line_ids = fields.One2many('account.payment.line', 'payment_id', string='Payment Details')
    sec_line_ids = fields.One2many('account.payment.line', 'payment_id', string='Payment Details')
    sales_man_id = fields.Many2one('hr.employee', string='Sales Man',
                                   domain=[('is_sales_man', '=', True)],track_visibility='onchange')
    ben_id = fields.Many2one('beneficiaries.bank.account', string='beneficiary',track_visibility='onchange')
    notee = fields.Text('Note',track_visibility='onchange')
    type = fields.Selection([('bank','Bank'),('cash','cash')],string='Type Journal',track_visibility='onchange')
    payment_to = fields.Selection(
        [('customer', 'Customer'), ('supplier', 'Vendor'), ('account', 'Direct Account')],
        string='Payment To',track_visibility='onchange')#, ('employee', 'Employee')

    part_ids = fields.One2many('account.payment.part', 'payment_id', string='Debit / Credit Part')

    journal_amount = fields.Monetary(default=0.0, string='Balance', compute='_journal_balance', store=True,track_visibility='onchange')
    credit_debit_parts= fields.Boolean('Credit/Debits Parts?',track_visibility='onchange')
    amount1 = fields.Monetary(string='Payment Amount', compute='_get_amount_1', store=True,track_visibility='onchange')
    spartner_id = fields.Many2one('res.partner', 'Partner Line', related='line_ids.partner_id')
    saccount_id = fields.Many2one('account.account', 'Account', related='sec_line_ids.account_id')
    journal_id = fields.Many2one(track_visibility='onchange')
    state = fields.Selection(track_visibility='onchange')
    payment_date = fields.Date(track_visibility='onchange')

    
        

    @api.onchange('sec_line_ids','line_ids','part_ids','payment_type','payment_to')
    def _onchange_payment_amount(self):
        total_amount_details = 0
        total_amount_parts = 0
        total_amount_parts_debit=0
        total_amount_parts_credit=0
        total_amount=0

	if self.payment_to and self.payment_to != 'account':
		#lines = self.line_ids + self.sec_line_ids
		for line_detail in self.line_ids:
		    total_amount_details += line_detail.amount

		for line_part in self.part_ids:
		    if line_part.part_type == 'debit':
		        total_amount_parts_debit += line_part.amount
		    if line_part.part_type == 'credit':
		        total_amount_parts_credit += line_part.amount

		if self.payment_type == 'inbound':
		    total_amount_parts= total_amount_parts_debit -  total_amount_parts_credit
		    total_amount=total_amount_details-total_amount_parts
		    self.amount=total_amount 

		elif self.payment_type == 'outbound':
		    total_amount_parts= total_amount_parts_debit -  total_amount_parts_credit
		    total_amount=total_amount_details+total_amount_parts
		    self.amount=total_amount 
	elif self.payment_to == 'account':   
		#lines = self.sec_line_ids + self.sec_line_ids
		for line_detail in self.sec_line_ids:
		    total_amount_details += line_detail.amount

		for line_part in self.part_ids:
		    if line_part.part_type == 'debit':
		        total_amount_parts_debit += line_part.amount
		    if line_part.part_type == 'credit':
		        total_amount_parts_credit += line_part.amount

		if self.payment_type == 'inbound':
		    total_amount_parts= total_amount_parts_debit -  total_amount_parts_credit
		    total_amount=total_amount_details-total_amount_parts
		    self.amount=total_amount 

		elif self.payment_type == 'outbound':
		    total_amount_parts= total_amount_parts_debit -  total_amount_parts_credit
		    total_amount=total_amount_details+total_amount_parts
		    self.amount=total_amount 
             
        self.amount=total_amount 

    """
    @api.one
    @api.depends('type','payment_type')
    def get_default_data(self):
        if self.type == 'bank':
            if self.payment_type == 'outbound':
                self.payment_method_id = self.env.user.partner_id.payment_method_bnk_out_id.id
                self.journal_id = self.env.user.partner_id.journal_out_bnk_id.id
                self.payment_to = self.env.user.partner_id.payment_to_out_bnk
            elif self.payment_type == 'inbound':
                self.journal_id = self.env.user.partner_id.journal_in_bnk_id.id
                self.payment_to = self.env.user.partner_id.payment_to_in_bnk
        if self.type == 'cash':
            if self.payment_type == 'inbound':
                self.journal_id = self.env.user.partner_id.journal_in_csh_id.id
                self.payment_to = self.env.user.partner_id.payment_to_in_cash
            elif self.payment_type == 'outbound':
                self.journal_id = self.env.user.partner_id.journal_out_csh_id.id
                self.payment_to = self.env.user.partner_id.payment_to_out_cash
    """

    @api.model
    def create(self, vals):
        result = super(AccountPayment, self).create(vals)
        if result.type == 'bank' and result.payment_type == 'outbound':
            if vals.get('payment_method_id'):
                self.env.user.partner_id.write({'payment_method_bnk_out_id': vals.get('payment_method_id')})
            if vals.get('journal_id'):
                self.env.user.partner_id.write({'journal_out_bnk_id': vals.get('journal_id')})
            if vals.get('payment_to'):
                self.env.user.partner_id.write({'payment_to_out_bnk': vals.get('payment_to')})
        if result.type == 'bank' and result.payment_type == 'inbound':
            if vals.get('journal_id'):
                self.env.user.partner_id.write({'journal_in_bnk_id': vals.get('journal_id')})
            if vals.get('payment_to'):
                self.env.user.partner_id.write({'payment_to_in_bnk': vals.get('payment_to')})
        if result.type == 'cash' and result.payment_type  == 'outbound':
            if vals.get('journal_id'):
                self.env.user.partner_id.write({'journal_out_csh_id': vals.get('journal_id')})
            if vals.get('payment_to'):
                self.env.user.partner_id.write({'payment_to_out_cash': vals.get('payment_to')})
        if result.type == 'cash' and result.payment_type == 'inbound':
            if vals.get('journal_id'):
                self.env.user.partner_id.write({'journal_in_csh_id': vals.get('journal_id')})
            if vals.get('payment_to'):
                self.env.user.partner_id.write({'payment_to_in_cash': vals.get('payment_to')})
        return result

    @api.onchange('payment_to')
    def _onchange_payment_to(self):
        # Set partner_id domain
        vals = {}
        if self.payment_to:
            self.partner_id = []
            if self.line_ids:
                vals['line_ids'] = False
            if self.sec_line_ids:
                vals['sec_line_ids'] = False
            if self.payment_to == 'customer':
                # self.partner_type = 'customer'
                vals['partner_type'] = 'customer'
            elif self.payment_to == 'supplier' or self.payment_to == 'employee':
                # self.partner_type = 'supplier'
                vals['partner_type'] = 'supplier'
            else:
                vals['partner_type'] = False
        return {'value': vals}

    @api.one
    @api.multi
    def _check_lines(self):
        if self.multi_supplier:
            #if self.state != 'draft':
            if not self.line_ids:
                raise ValidationError(_('You can not confirm payment without payment details'))

            total_amount = 0
            lines = self.line_ids + self.sec_line_ids
            for line in  self.line_ids:
                total_amount += line.amount
                if (line.amount <= 0.0):
                    if line.payment_to != 'account':
                        raise ValidationError(_('The payment amount for the partner %s must be strictly positive.')%(line.partner_id.name))
                    if line.payment_to == 'account':
                        raise ValidationError(_('The payment amount for the account %s must be strictly positive.')%(line.account_id.name))
                if line.linked_with_invoices and not line.payment_invoice_ids:
                    raise ValidationError(_('Please select partner invoices for the partner %s')%(line.partner_id.name))
                if line.linked_with_invoices and line.amount != line.lines_amount:
                    raise ValidationError(_('The payment amount for the partner %s should be equal to the total payment amount of the selected invoices')%(line.partner_id.name))
            amount = self.amount
            self._cr.execute(''' select sum(account_payment_line.amount) as total_amount, account_payment.amount as amount from account_payment_line, account_payment  
                        where account_payment_line.payment_id = %s and account_payment.id = %s
                        group by account_payment.id 
                ''', (self.ids[0], self.ids[0]))
            res = self._cr.dictfetchall()
            for part in self.part_ids:
                if (part.amount <= 0.0):
                    raise ValidationError(_('The amount for the partner %s in credit / debit parts must be strictly positive.')%(line.partner_id.name))
            #if self.amount != total_amount:
            #if res[0]['amount'] != res[0]['total_amount']:
            #    raise ValidationError(_('The payment amount must be equal to total amount in payment details'))



    
    @api.multi
    def post_multi_partner(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconciliable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconciliable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        for rec in self:
            rec._check_lines()
            if rec.state != 'draft':
                raise UserError(_("Only a draft payment can be posted. Trying to post a payment in state %s.") % rec.state)
            if rec.bill_payment:
                if any(inv.state != 'open' for inv in rec.invoice_ids):
                    raise ValidationError(_("The payment cannot be processed because the invoice is not open!"))

            # Use the right sequence to set the name
        #     if rec.payment_type == 'transfer':
        #         sequence_code = 'account.payment.transfer'
        #     else:
        #         if rec.payment_to == 'account':
        #             sequence_code = 'account.payment.direct'
        #         else:
        #             if rec.partner_type == 'customer':
        #                 if rec.payment_type == 'inbound':
        #                     sequence_code = 'account.payment.customer.invoice'
        #                 if rec.payment_type == 'outbound':
        #                     sequence_code = 'account.payment.customer.refund'
        #             if rec.partner_type == 'supplier':
        #                 if rec.payment_type == 'inbound':
        #                     sequence_code = 'account.payment.supplier.refund'
        #                 if rec.payment_type == 'outbound':
        #                     sequence_code = 'account.payment.supplier.invoice'
            
            if rec.payment_type == 'transfer':
                sequence_code = 'account.payment.transfer'
            else:
                # if rec.payment_to == 'account':
                #     sequence_code = 'account.payment.direct'
                # else:
                if rec.type == 'bank':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'receive.bank'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'payment.bank'
                if rec.type == 'cash':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'receive.cash'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'payment.cash'
            if 'Draft' in rec.name:
                rec.name = self.env['ir.sequence'].with_context(ir_sequence_date=rec.payment_date).next_by_code(sequence_code)

            # Create the journal entry
            amount = rec.amount * (rec.payment_type in ('outbound', 'transfer') and 1 or -1)
            move = rec._create_multi_payment_entry(amount)

            # In case of a transfer, the first journal entry created debited the source liquidity account and credited
            # the transfer account. Now we debit the transfer account and credit the destination liquidity account.
            if rec.payment_type == 'transfer':
                transfer_credit_aml = move.line_ids.filtered(lambda r: r.account_id == rec.company_id.transfer_account_id)
                transfer_debit_aml = rec._create_transfer_entry(amount)
                (transfer_credit_aml + transfer_debit_aml).reconcile()

            rec.write({'state': 'posted', 'move_name': move.name})



    def _create_multi_payment_entry(self, amount):
        """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
            Return the journal entry.
        """
        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        invoice_currency = False
        if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
            #if all the invoices selected share the same currency, record the paiement in that currency too
            invoice_currency = self.invoice_ids[0].currency_id
        debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)
        move_dict = self._get_move_vals()
        move_dict.update({'ref':self.name})
	#print "  777777777777    44 ",move_dict


	self._cr.execute("insert into account_move "\
		    "( create_uid, name, state, ref, company_id, journal_id, "\
		    "  currency_id, amount, matched_percentage, write_date, date, "\
		    "  create_date, write_uid, reversed,custom_view ) VALUES "\
		    "( %s, '%s', '%s', '%s' ,%s, %s, "\
		    " %s, %s, %s, '%s', '%s', "\
		    " '%s', %s, '%s', '%s') ;" 
		    %(self.env.user.id, move_dict.get('name', False) or '/', 'draft', move_dict.get('ref', False) or '/', self.env.user.company_id.id, move_dict.get('journal_id', False) or 'null',
		    self.env.user.company_id.currency_id.id, 0.00, 0.0, datetime.now(), move_dict.get('date', False) or 'null',
		    datetime.now(), self.env.user.id, 'f', 'default'))


	self._cr.execute("select id from account_move where ref = '%s';" %(self.name))
	res = self._cr.dictfetchall()
    	print res
	account_move_id_new = res[0]['id']

        move = self.env['account.move'].browse(account_move_id_new)
        #move = self.env['account.move'].create(move_dict)
	account_obj = self.env['account.account']
        if self.multi_supplier:
            
            for line in self.line_ids:
                line._check_amount()
                if line.linked_with_invoices:
                    for invoice_line in line.payment_invoice_ids:
                        invoice_amount = invoice_line.amount * (self.payment_type in ('outbound', 'transfer') and 1 or -1)
                        invoice_currency2 = invoice_line.invoice_id.currency_id
                        debit2, credit2, amount_currency2, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(invoice_amount, self.currency_id, self.company_id.currency_id, invoice_currency)
                        counterpart_aml_dict = {
                                    'partner_id': line.partner_id and line.partner_id.id or False,
                                    'invoice_id': invoice_line.invoice_id and invoice_line.invoice_id.id or False,
                                    'move_id': move.id,
                                    'debit': debit2,
                                    'credit': credit2,
                                    'amount_currency': False,
                                }
                        # Write line corresponding to invoice payment
                        counterpart_aml_dict.update(self._multi_get_counterpart_move_line_vals(invoice_line.invoice_id,line))
                        counterpart_aml_dict.update({'currency_id': currency_id})

                        if counterpart_aml_dict.get('credit', False):
                            _balance = counterpart_aml_dict.get('credit', False) * -1
                        if counterpart_aml_dict.get('debit', False):
                            _balance = counterpart_aml_dict.get('debit', False)

                        self._cr.execute("insert into account_move_line "\
					    "( create_date, create_uid, company_id, write_uid, write_date, user_type_id, "\
					    " journal_id, date_maturity, date, blocked, ref, tax_exigible, invoice_id, "\
					    " partner_id, account_id, currency_id, payment_id, move_id, "\
					    " debit, credit, name, reconciled, company_currency_id, active, "\
                        " balance) values "\
					    "( '%s',%s, %s, %s, '%s', %s, "\
					    "  %s, '%s', '%s', '%s', '%s','%s', %s, "\
					    "  %s, %s, %s, %s, %s, "\
					    " %s, %s, '%s', '%s', %s, '%s', "\
                        " %s );" 
			    %(datetime.now(), self.env.user.id, self.env.user.company_id.id, self.env.user.id, datetime.now(), account_obj.browse(counterpart_aml_dict.get('account_id', False)).user_type_id.id,
			    counterpart_aml_dict.get('journal_id', False) or 'null', self.payment_date, self.payment_date, 'f', self.name or '/', 't', counterpart_aml_dict.get('invoice_id', False) or 'null',
			    counterpart_aml_dict.get('partner_id', False) or 'null' or 'null', counterpart_aml_dict.get('account_id', False) or 'null', currency_id or 'null', self.id, move.id,
			    counterpart_aml_dict.get('debit', False) or 0.00, counterpart_aml_dict.get('credit', False) or 0.00, counterpart_aml_dict.get('name', False) or 'null', 'f', self.env.user.company_id.currency_id.id, 't',
                _balance ))

                        #counterpart_aml = aml_obj.create(counterpart_aml_dict)
                        if invoice_line.payment_difference_handling == 'reconcile' and invoice_line.payment_difference:
                            #Reconcile with the invoices
                            writeoff_line = {
                                    'partner_id': line.partner_id and line.partner_id.id or False,
                                    'invoice_id': invoice_line.invoice_id and invoice_line.invoice_id.id or False,
                                    'move_id': move.id,
                                    'debit': debit2,
                                    'credit': credit2,
                                    'amount_currency': False,
                                }
                            
                            amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(invoice_line.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
                            # the writeoff debit and credit must be computed from the invoice residual in company currency
                            # minus the payment amount in company currency, and not from the payment difference in the payment currency
                            # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
                            total_residual_company_signed = invoice_line.invoice_id.residual_company_signed
                            total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(invoice_line.amount, self.company_id.currency_id)
                            if invoice_line.invoice_id.type in ['in_invoice', 'out_refund']:
                                amount_wo = total_payment_company_signed - total_residual_company_signed
                            else:
                                amount_wo = total_residual_company_signed - total_payment_company_signed
                            # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
                            # amount in the company currency
                            if amount_wo > 0:
                                debit_wo = amount_wo
                                credit_wo = 0.0
                                amount_currency_wo = abs(amount_currency_wo)
                            else:
                                debit_wo = 0.0
                                credit_wo = -amount_wo
                                amount_currency_wo = -abs(amount_currency_wo)
                            writeoff_line['name'] = _('Counterpart')
                            writeoff_line['account_id'] = invoice_line.writeoff_account_id.id
                            writeoff_line['debit'] = debit_wo
                            writeoff_line['credit'] = credit_wo
                            writeoff_line['amount_currency'] = amount_currency_wo
                            writeoff_line['currency_id'] = currency_id
                            writeoff_line['payment_id'] = self.id


		            if writeoff_line.get('credit', False):
		                _balance = writeoff_line.get('credit', False) * -1
		            if writeoff_line.get('debit', False):
		                _balance = writeoff_line.get('debit', False)
                        
		            self._cr.execute("insert into account_move_line "\
					    "( create_date, create_uid, company_id, write_uid, write_date, user_type_id, "\
					    " journal_id, date_maturity, date, blocked, ref, tax_exigible, invoice_id, "\
					    " partner_id, account_id, currency_id, payment_id, move_id, "\
					    "  debit, credit, name, reconciled, company_currency_id, active, "\
                        " balance) values "\
					    "( '%s',%s, %s, %s, '%s', %s, "\
					    "  %s, '%s', '%s', '%s', '%s','%s', %s, "\
					    "  %s, %s, %s, %s, %s, "\
					    " %s, %s, '%s', '%s', %s, '%s', "\
                        " %s );" 
			    %(datetime.now(), self.env.user.id, self.env.user.company_id.id, self.env.user.id, datetime.now(), account_obj.browse(writeoff_line.get('account_id', False)).user_type_id.id,
			    writeoff_line.get('journal_id', False) or 'null', self.payment_date, self.payment_date, 'f', self.name or '/', 't', writeoff_line.get('invoice_id', False) or 'null',
			    writeoff_line.get('partner_id', False) or 'null' or 'null', writeoff_line.get('account_id', False) or 'null', currency_id or 'null', self.id, move.id,
			    writeoff_line.get('debit', False) or 0.00, writeoff_line.get('credit', False) or 0.00, writeoff_line.get('name', False) or 'null', 'f', self.env.user.company_id.currency_id.id, 't',
                _balance ))


                            #writeoff_line = aml_obj.create(writeoff_line)
                            if counterpart_aml['debit']:
                                counterpart_aml['debit'] += credit_wo - debit_wo
                            if counterpart_aml['credit']:
                                counterpart_aml['credit'] += debit_wo - credit_wo
                            counterpart_aml['amount_currency'] -= amount_currency_wo
                        invoice_line.invoice_id.register_payment(counterpart_aml)
                else:
                    line_amount = line.amount * (self.payment_type in ('outbound', 'transfer') and 1 or -1)
                    debit1, credit1, amount_currency1, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(line_amount, self.currency_id, self.company_id.currency_id, invoice_currency)
                    counterpart_aml_dict = {
                                'partner_id': line.partner_id and line.partner_id.id or False,
                                #'invoice_id': line.invoice_id and line.invoice_id.id or False,
                                'move_id': move.id,
                                'debit': debit1,
                                'credit': credit1,
                                'amount_currency': False,
                            }
                    #print '::::::::::::::::::::::::::::::',credit1
                    # Write line corresponding to invoice payment
                    counterpart_aml_dict.update(self._multi_get_counterpart_move_line_vals(False,line))
                    counterpart_aml_dict.update({'currency_id': currency_id})

                    #print '#####################',counterpart_aml_dict.get('credit', False) or 0.00 * -1
                    if counterpart_aml_dict.get('credit', False):
                        _balance = counterpart_aml_dict.get('credit', False) * -1
                    if counterpart_aml_dict.get('debit', False):
                        _balance = counterpart_aml_dict.get('debit', False)

                    self._cr.execute("insert into account_move_line "\
				    "( create_date, create_uid, company_id, write_uid, write_date, user_type_id, "\
				    " journal_id, date_maturity, date, blocked, ref, tax_exigible, invoice_id, "\
				    " partner_id, account_id, currency_id, payment_id, move_id, "\
				    "  debit, credit, name, reconciled, company_currency_id, active, "\
                    " balance) values "\
				    "( '%s',%s, %s, %s, '%s', %s, "\
				    "  %s, '%s', '%s', '%s', '%s','%s', %s, "\
				    "  %s, %s, %s, %s, %s, "\
				    " %s, %s, '%s', '%s', %s, '%s', "\
                    " %s );" 
		    %(datetime.now(), self.env.user.id, self.env.user.company_id.id, self.env.user.id, datetime.now(), account_obj.browse(counterpart_aml_dict.get('account_id', False)).user_type_id.id,
		    counterpart_aml_dict.get('journal_id', False) or 'null', self.payment_date, self.payment_date, 'f', self.name or '/', 't', counterpart_aml_dict.get('invoice_id', False) or 'null',
		    counterpart_aml_dict.get('partner_id', False) or 'null' or 'null', counterpart_aml_dict.get('account_id', False) or 'null', currency_id or 'null', self.id, move.id,
		    counterpart_aml_dict.get('debit', False) or 0.00, counterpart_aml_dict.get('credit', False) or 0.00, counterpart_aml_dict.get('name', False) or 'null', 'f', self.env.user.company_id.currency_id.id, 't',
            _balance ))



                    #counterpart_aml = aml_obj.create(counterpart_aml_dict)

            for part in self.part_ids:
                part_amount = part.amount * (part.part_type in ('debit') and 1 or -1)
                debit3, credit3, amount_currency3, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(part_amount, self.currency_id, self.company_id.currency_id, invoice_currency)
                name = ''
                if part.label:
                    name += part.label
                else:
                    if part.part_type == 'debit':
                        name += 'اطراف مدينة اخرى'
                    else:
                        name += 'اطراف دائنة اخرى'
		'''
                counterpart_aml_dict = {
                            'partner_id': part.partner_id and part.partner_id.id or False,
                            'move_id': move.id,
                            'debit': debit3,
                            'credit': credit3,
                            'amount_currency': False,
                            'name': name,
                            'account_id': part.account_id.id,
                            'journal_id': self.journal_id.id,
                            'currency_id': currency_id or False,
                            'payment_id': self.id,
                                        }
		'''

		#print "  QQQQQQQQQQQQQQQ   "
                if credit3:
                    _balance = credit3 * -1
                if debit3:
                    _balance = debit3

                self._cr.execute("insert into account_move_line "\
				    "( create_date, create_uid, company_id, write_uid, write_date, user_type_id, "\
				    " journal_id, date_maturity, date, blocked, ref, tax_exigible, "\
				    " partner_id, account_id, currency_id, payment_id, move_id, "\
				    "  debit, credit, name, reconciled, company_currency_id, active, "\
                    " balance) values "\
				    "( '%s',%s, %s, %s, '%s', %s, "\
				    "  %s, '%s', '%s', '%s', '%s','%s', "\
				    "  %s, %s, %s, %s, %s, "\
				    " %s, %s, '%s', '%s', %s, '%s', "\
                    " %s );" 
                    %(datetime.now(), self.env.user.id, self.env.user.company_id.id, self.env.user.id, datetime.now(), part.account_id.user_type_id.id,
		    self.journal_id.id, self.payment_date, self.payment_date, 'f', self.name or '/', 't', 
                    part.partner_id and part.partner_id.id or 'null', part.account_id.id, currency_id or 'null', self.id, move.id,
                    debit3 or 0.00, credit3 or 0.00, name, 'f', self.env.user.company_id.currency_id.id, 't',
                    _balance ))
		#print " YYEEESSSS  "
                #counterpart_aml = aml_obj.create(counterpart_aml_dict)

        #Write counterpart lines
        if not self.currency_id != self.company_id.currency_id:
            amount_currency = 0
        liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
        liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))

        if liquidity_aml_dict.get('credit', False):
            _balance = liquidity_aml_dict.get('credit', False) * -1
        if liquidity_aml_dict.get('debit', False):
            _balance = liquidity_aml_dict.get('debit', False)

        self._cr.execute("insert into account_move_line "\
			    "( create_date, create_uid, company_id, write_uid, write_date, user_type_id, "\
			    " journal_id, date_maturity, date, blocked, ref, tax_exigible, "\
			    " partner_id, account_id, currency_id, payment_id, move_id, "\
			    "  debit, credit, name, reconciled, company_currency_id, active, "\
                " balance) values "\
			    "( '%s',%s, %s, %s, '%s', %s, "\
			    "  %s, '%s', '%s', '%s', '%s','%s', "\
			    "  %s, %s, %s, %s, %s, "\
			    " %s, %s, '%s', '%s', %s, '%s', "\
                " %s );" 
            %(datetime.now(), self.env.user.id, self.env.user.company_id.id, self.env.user.id, datetime.now(), account_obj.browse(liquidity_aml_dict.get('account_id', False)).user_type_id.id,
	    liquidity_aml_dict.get('journal_id', False) or 'null', self.payment_date, self.payment_date, 'f', self.name or '/', 't', 
            liquidity_aml_dict.get('partner_id', False) or 'null' or 'null', liquidity_aml_dict.get('account_id', False) or 'null', currency_id or 'null', self.id, move.id,
            liquidity_aml_dict.get('debit', False) or 0.00, liquidity_aml_dict.get('credit', False) or 0.00, self.notee, 'f', self.env.user.company_id.currency_id.id, 't',
            _balance ))
	#print "  9999988888   ",liquidity_aml_dict
        #aml_obj.create(liquidity_aml_dict)

        move.post()
	aamount= 0
	for l in move.line_ids:
		aamount += (l.debit or 0.00)
	move.amount=aamount
        return move


    def _multi_get_counterpart_move_line_vals(self, invoice=False, line=False):
        if self.payment_type == 'transfer':
            name = self.name
        else:
            name = ''
            if line.label:
                name += line.label
            else:
                if self.partner_type == 'customer':
                    if self.payment_type == 'inbound':
                        name += _("Customer Payment")
                    elif self.payment_type == 'outbound':
                        name += _("Customer Refund")
                elif self.partner_type == 'supplier':
                    if self.payment_type == 'inbound':
                        name += _("Vendor Refund")
                    elif self.payment_type == 'outbound':
                        name += _("Vendor Payment")
                if invoice:
                    name += ': '
                    if invoice.move_id:
                        name += invoice.number + ', '
                    name = name[:len(name)-2] 
        if line.payment_to == 'customer':
            account = line.partner_id.property_account_receivable_id.id
        elif line.payment_to == 'supplier' or line.payment_to == 'employee':
            account = line.partner_id.property_account_payable_id.id
        else:
            account = line.account_id.id
        return {
            'ref': self.name,
            'name': name,
            'account_id': account,
            'journal_id': self.journal_id.id,
            'currency_id': self.currency_id != self.company_id.currency_id and self.currency_id.id or False,
            'payment_id': self.id,
        }


    @api.multi
    def cancel(self):
	#print "AAAAAAA   999999999"
        for rec in self:
            line_with_invoice = [line.id for line in rec.line_ids if line.linked_with_invoices]
            for move in rec.move_line_ids.mapped('move_id'):
                if rec.invoice_ids or line_with_invoice != []:
                    move.line_ids.remove_move_reconcile()
                move.button_cancel()
                #move.unlink()
		self._cr.execute("delete from account_move_line where move_id = %s;" %(move.id))
		self._cr.execute("delete from account_move where id = %s;" %(move.id))
            rec.state = 'draft'



class AccountInvoice(models.Model):
    _inherit = 'account.invoice'


    @api.model
    def name_search(self, name, args, operator='ilike', limit=100):
        """
        Inherit name_search method to add dynamic domain to budget_line and line_id fields
        """
        context = self._context
        if 'model' in context and context['model'] == 'account.payment.line.invoice':
            if 'line_ids' in context :
                ids = []
                for line in self.env['account.payment.line'].resolve_2many_commands('payment_invoice_ids', context['line_ids'],['invoice_id']):
                    if line.get('id'):
                        if line['id'] not in ids: ids.append(line['id'])
                    else:
                        if line.get('invoice_id') and line['invoice_id'] not in ids:
                            ids.append(line['invoice_id'])
                if ids:
                    args.append(('id','not in',ids))
                

        return super(AccountInvoice, self).name_search(name, args, operator, limit)
