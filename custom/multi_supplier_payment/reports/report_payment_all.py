#-*- coding:utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models
from num2words import num2words
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic



class PaymentallReport(models.AbstractModel):
    _name = 'report.multi_supplier_payment.report_payment_all'

    @api.model
    def render_html(self, docids, data=None):
        payments = self.env['account.payment'].browse(docids)
        credit_lines=debit_lines=[]
        credit=debit=False
        for line in payments.part_ids:

            if line.part_type=='creadit':
                credit=True
                credit_lines.append({'partner_id':line.partner_id,'account_id':line.account_id,
                    'label':line.label,'amount':line.amount})
            else:
                debit=True
                debit_lines.append({'partner_id':line.partner_id,'account_id':line.account_id,
                    'label':line.label,'amount':line.amount})

        docargs = {
            'doc_ids': docids,
            'doc_model': 'account.payment',
            'docs': payments,
            'data': data,
            'debits': debit_lines,
            'credits': credit_lines,
            'credit':credit,
            'debit':debit,
            'total_words': amount_to_text_arabic(abs(payments.amount), 'SAR'),
        }
        return self.env['report'].render('multi_supplier_payment.report_payment_all', docargs)
