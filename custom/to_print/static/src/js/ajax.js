odoo.define('web.to_print', function (require) {
    "use strict";
    var core = require('web.core');
    var utils = require('web.utils');
    var time = require('web.time');
    var ajax = require('web.ajax');
    ajax.get_file = function get_file(options) {
        console.log(navigator)
        // need to detect when the file is done downloading (not used
        // yet, but we'll need it to fix the UI e.g. with a throbber
        // while dump is being generated), iframe load event only fires
        // when the iframe content loads, so we need to go smarter:
        // http://geekswithblogs.net/GruffCode/archive/2010/10/28/detecting-the-file-download-dialog-in-the-browser.aspx
        var timer, token = new Date().getTime(),
            cookie_name = 'fileToken', cookie_length = cookie_name.length,
            CHECK_INTERVAL = 1000, id = _.uniqueId('get_file_frame'),
            remove_form = false;
    
    
        // iOS devices doesn't allow iframe use the way we do it,
        // opening a new window seems the best way to workaround
        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
            var params = _.extend({}, options.data || {}, {token: token});
            var url = options.session.url(options.url, params);
            if (options.complete) { options.complete(); }
    
            return window.open(url);
        }

        var supportsPdfMimeType = typeof navigator.mimeTypes["application/pdf"] !== "undefined"
        var isIE = function () {
            return !!(window.ActiveXObject || "ActiveXObject" in window)
        }
        var supportsPdfActiveX = function () {
            return !!(createAXO("AcroPDF.PDF") || createAXO("PDF.PdfCtrl"))
        }
        var supportsPDFs = supportsPdfMimeType || isIE() && supportsPdfActiveX();

        if(JSON.parse(options.data['data'])[1] == "qweb-pdf" && supportsPDFs){
            var params = _.extend({}, options.data || {}, {token: token});
            var url = options.session.url(options.url, params);
            if (options.complete) { options.complete(); }

            var popup = window.open("", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=1500,height=800");

            popup.document.write('<style> .pdfobject-container { height: 99%;} .pdfobject { border: 1px solid #666; } </style>')
            
            popup.document.write('<a href="'+url+'">Download</a>')
            popup.document.write('<div id="DocViewer"><h1>loading</h1></div>')
            
            
            var options = {
                pdfOpenParams: {
                    view: "FitV",
                    pagemode: "thumbs",
                    search: "lorem ipsum"
                }
            }
            url = url.replace('/report/download','/report/download_custom')
            PDFObject.embed(url, popup.document.getElementById( "DocViewer"),options );

        
        }
    
        var $form, $form_data = $('<div>');
    
        var complete = function () {
            if (options.complete) { options.complete(); }
            clearTimeout(timer);
            $form_data.remove();
            $target.remove();
            if (remove_form && $form) { $form.remove(); }
        };
        var $target = $('<iframe target="_blank">')
            .attr({id: id, name: id})
            .appendTo(document.body)
            .load(function () {
                try {
                    if (options.error) {
                        var body = this.contentDocument.body;
                        var nodes = body.children.length === 0 ? body.childNodes : body.children;
                        var node = nodes[1] || nodes[0];
                        options.error(JSON.parse(node.textContent));
                    }
                } finally {
                    complete();
                }
            });
    
        if (options.form) {
            $form = $(options.form);
        } else {
            remove_form = true;
            $form = $('<form>', {
                action: options.url,
                method: 'POST'
            }).appendTo(document.body);
        }
        if (core.csrf_token) {
            $('<input type="hidden" name="csrf_token">')
                    .val(core.csrf_token)
                    .appendTo($form_data);
        }
    
        var hparams = _.extend({}, options.data || {}, {token: token});
        _.each(hparams, function (value, key) {
                var $input = $form.find('[name=' + key +']');
                if (!$input.length) {
                    $input = $('<input type="hidden" name="' + key + '">')
                        .appendTo($form_data);
                }
                $input.val(value);
            });
    
        $form
            .append($form_data)
            .attr('target', id)
        console.log("------------------------doc",document);
        $form.get(0).submit();
    
        var waitLoop = function () {
            var cookies = document.cookie.split(';');
            // setup next check
            timer = setTimeout(waitLoop, CHECK_INTERVAL);
            for (var i=0; i<cookies.length; ++i) {
                var cookie = cookies[i].replace(/^\s*/, '');
                if (!cookie.indexOf(cookie_name === 0)) { continue; }
                var cookie_val = cookie.substring(cookie_length + 1);
                if (parseInt(cookie_val, 10) !== token) { continue; }
    
                // clear cookie
                document.cookie = _.str.sprintf("%s=;expires=%s;path=/",
                    cookie_name, new Date().toGMTString());
                if (options.success) { options.success(); }
                complete();
                return;
            }
        };
        timer = setTimeout(waitLoop, CHECK_INTERVAL);
    };

});