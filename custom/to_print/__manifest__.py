# -*- coding: utf-8 -*-
# Copyright 2016 Siddharth Bhalgami <siddharth.bhalgami@techreceptives.com>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).
{
    "name": "to print",
    "summary": " ",
    "version": "10.0.1.0.0",
    "category": "web",
    "website": "https://www.techreceptives.com",
    "author": "alfadil",
    "license": "LGPL-3",
    "data": [
        "views/assets.xml",
    ],
    "depends": [
        "web",
    ],
    "qweb": [
    ],
    "installable": True,
}
