# -*- coding: utf-8 -*-

from odoo import fields, api, models
from odoo import _
from odoo.exceptions import ValidationError
import dateutil.parser


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    @api.one
    def fetch_products(self):
        #current_products = []
        #current_products = [x.product_id.id for x in self.order_line]
        #vals = []
	p_obj= self.env['product.packaging.line']
        current_products = [x.product_id.id for x in self.order_line]
	product_obj = self.env['product.product']
        vals = []

        if self.warehouse_id:
            if self.partner_id:
                if self.partner_id.prices_warehouse_ids:
                    conf_warehouses = filter(lambda x: x.warehouse_id.id == self.warehouse_id.id,
                                             self.partner_id.prices_warehouse_ids) or []
                    for wh in conf_warehouses:
                        if wh.priceslist_ids:
			    wh_products_ids= sorted(wh.priceslist_ids, key=lambda l: l.sequence)
                            for line in wh_products_ids:
			        product_idss= product_obj.search([('product_tmpl_id','=',line.product_id.id)])
				product_id= product_idss and product_idss[0].id or False
                                if product_id not in current_products:
                                    vals.append((0, False, {'product_id': product_id,
                                                            'product_uom': product_idss and product_idss[0].uom_id.id or False,
                                                            'price_unit': line.price or 0.00,
                                                            'new_price': line.price or 0.00,
                                                            'product_qty': 0.0,
                                                            'name': line.product_id.name,
							    'packaging_type_id': line.packaging_type_id.id,
                                                            'date_planned': self.date_planned}))
        if vals:
            ctx = self.env.context.copy()
            ctx.update({
                'no_delete_line': True
            })
            self.env.context = ctx
            self.order_line = vals



class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    packaging_type_id = fields.Many2one('product.packaging.type', string='Packaging Type')
    package_ids= fields.Many2many('product.packaging.type', 'package_purchase_lines_rel', 'package_id', 'pline_id', compute='get_packages', string='Packages')


    @api.multi
    @api.depends('product_id','order_id.warehouse_id')
    def get_packages(self):
	price_obj = self.env['product.packaging.line']
	for rec in self:
		p_ids= []
		if rec.product_id and rec.order_id.warehouse_id:
			price_ids = price_obj.search([('product_id','=',rec.product_id.product_tmpl_id.id),('warehouse_id','=',rec.order_id.warehouse_id.id)])
			for line in price_ids:
				if line.packaging_type_id.id not in p_ids:
					p_ids.append(line.packaging_type_id.id)
					
			rec.package_ids = [(6,0,p_ids)]

		else:
			rec.package_ids = [(6,0,[])]

    @api.multi
    @api.depends('product_id', 'order_id.warehouse_id')
    def get_packages(self):
	p_obj= self.env['res.partner']
	for rec in self:
		p_ids= []
		if rec.product_id and rec.order_id.warehouse_id:
			for line in rec.product_id.packaging_ids:
				if line.all_warehouses:
					if line.packaging_type_id.id not in p_ids:
						p_ids.append(line.packaging_type_id.id)
				else:
					w_ids= []
					for w in line.warehouses_ids:
						if w.id not in w_ids:
							w_ids.append(w.id)
					if rec.order_id.warehouse_id.id in w_ids:
						if line.packaging_type_id.id not in p_ids:
							p_ids.append(line.packaging_type_id.id)
					
			rec.package_ids = [(6,0,p_ids)]

		else:
			rec.package_ids = [(6,0,[])]

