# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.osv import expression
from odoo.exceptions import ValidationError, UserError, Warning

class ProductPackagingType(models.Model):
    _name = 'product.packaging.type'

    name = fields.Char('Name', required=True)
    active = fields.Boolean(string='Active', default=True)

class ProductPackagingLine(models.Model):
    _name = 'product.packaging.line'
    _rec_name= 'packaging_type_id'

    packaging_type_id = fields.Many2one('product.packaging.type', string='Packaging Type', required=True)
    min_price = fields.Float(string='Minimum Price')
    max_price = fields.Float(string='Maximum Price')
    min_weight = fields.Float(string='Minimum Weight')
    max_weight = fields.Float(string='Maximum Weight')
    warehouses_ids = fields.Many2many('stock.warehouse', 'packaging_warehouse_rel', 'pp_id', 'w_id', string='Warehouses')
    all_warehouses = fields.Boolean(string='All Warehouses?')
    product_id = fields.Many2one('product.template', string='Product Template', ondelete='cascade')


class CustomerPricelistWarehouse(models.Model):
    _name = 'customer.pricelist.warehouse'
    _rec_name= 'warehouse_id'

    partner_id = fields.Many2one('res.partner', string='Partner', ondelete='cascade')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', ondelete='cascade')
    priceslist_ids = fields.One2many('customer.packaging.pricelist.line', 'customer_warehouse_id', string="Prices List")

    _sql_constraints = [
        ('unique_warehouse', 'unique(partner_id, warehouse_id)', 'The Warehouse must be unique per Customer')
    ]

class CustomerPackagingPricelistLine(models.Model):
    _name = 'customer.packaging.pricelist.line'
    _rec_name= 'packaging_type_id'
    _order = 'sequence asc'

    customer_warehouse_id = fields.Many2one('customer.pricelist.warehouse', string='Customer Warehouse', ondelete='cascade')
    code = fields.Char(string='Code')
    packaging_type_id = fields.Many2one('product.packaging.type', string='Packaging Type', required=False, ondelete='cascade')
    price = fields.Float(string='Price')
    warehouse_id = fields.Many2one('stock.warehouse', related='customer_warehouse_id.warehouse_id', store=True, string='Warehouse')
    product_id = fields.Many2one('product.template', string='Product', ondelete='cascade')
    package_ids= fields.Many2many('product.packaging.type', 'package_priceslist_rel', 'packag_id', 'plist_id', compute='get_packages', string='Packages')
    sequence = fields.Integer('Sequence',default=10)

    _sql_constraints = [
        ('unique_product_packaging', 'unique(product_id, packaging_type_id ,warehouse_id)', 'The Product and Package must be unique per Warehouse for specific Customer')
    ]

    def action_delete(self):
	for rec in self:
		rec.unlink()
	return True

    @api.multi
    @api.onchange('product_id')
    def onchange_product_id(self):
	for rec in self:
		rec.packaging_type_id = False

    @api.multi
    @api.depends('product_id', 'warehouse_id')
    def get_packages(self):
	p_obj= self.env['res.partner']
	for rec in self:
		p_ids= []
		if rec.product_id and rec.warehouse_id:
			for line in rec.product_id.packaging_ids:
				if line.all_warehouses:
					if line.packaging_type_id.id not in p_ids:
						p_ids.append(line.packaging_type_id.id)
				else:
					w_ids= []
					for w in line.warehouses_ids:
						if w.id not in w_ids:
							w_ids.append(w.id)
					if rec.warehouse_id.id in w_ids:
						if line.packaging_type_id.id not in p_ids:
							p_ids.append(line.packaging_type_id.id)
					
			rec.package_ids = [(6,0,p_ids)]

		else:
			rec.package_ids = [(6,0,[])]




