# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class ResPartner(models.Model):
    _inherit = 'res.partner'

    prices_warehouse_ids = fields.One2many('customer.pricelist.warehouse', 'partner_id', string="Warehouses Prices List")
