from odoo import SUPERUSER_ID
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class vendor_invoice_custom(models.Model):
    
    _inherit = 'vendors.invoice'
    user_id = fields.Many2one('res.users', string='Responsible', required=False, default=lambda self: self.env.user)

    send_by_email = fields.Boolean(string='Sent By Email', store=True, default=False)
    send_date = fields.Date(string='Sent Date', store=True)

class wiz_mass_invoice_vendor(models.TransientModel):
    _name = 'wiz.mass.invoice.vendor'
    header_footer_id = fields.Many2one('header.footer.text', string='Template name', required=True)
    text1 = fields.Text('Begin Text',translate=True)
    text2 = fields.Text('End Text',translate=True)

    @api.onchange('header_footer_id')
    def set_front_end(self):
        res = []
        if self.header_footer_id:
            
            self.text1=self.header_footer_id.text1
            self.text2=self.header_footer_id.text2

        #return res

    @api.depends('send_by_email')
    @api.multi 
    def mass_invoice_email_send(self):
        context = self._context
        active_ids = context.get('active_ids')
        super_user = self.env['res.users'].browse(SUPERUSER_ID)
        _partner = []
        for a_id in active_ids:
            print "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD a_id = ** ",a_id
            account_invoice_brw = self.env['vendors.invoice'].browse(a_id)
            for partner in account_invoice_brw.vendor_id:
                partner_email = partner.email
                if not partner_email:
                    _partner.append(account_invoice_brw.vendor_id)
                    #raise UserError(_('%s customer has no email id please enter email address')% (account_invoice_brw.vendor_id.name)) 
                elif partner_email:
                    email_receipts=""+partner.email
                    list_receipts=[]
                    getObj= self.env['res.partner']
                    getAllIds = getObj.search([('parent_id','=',partner.id)])
                    for purchase_line_id in getAllIds :
                        wanted_vendor_id=getObj.browse([purchase_line_id])
                        email_receipts=email_receipts+" , "+wanted_vendor_id.id.email

                    self.env.cr.execute("UPDATE vendors_invoice SET send_by_email = 't', send_date = '%s'  WHERE id = %s;" %(fields.Date.today(),a_id))
                    template_id = self.env['ir.model.data'].get_object_reference(
                                                                        'send_by_email', 
                                                                        'vendor_custom_email_template_edi_invoice')[1]
                    email_template_obj = self.env['mail.template'].browse(template_id)
                    if template_id:
                        values = email_template_obj.generate_email(a_id, fields=None)
                        values['email_to'] = email_receipts
                        values['res_id'] = a_id
                        ir_attachment_obj = self.env['ir.attachment']
                        get_body=""+values['body']
                        get_html=""+values['body_html']
                        token_list1=get_body.split('@',2)
                        token_list2=get_html.split('@',2)
                        add_text=''+self.text1
                        new_text_body=token_list1[0]+add_text+"<br />"+token_list1[1]+"<br />"+self.text2+token_list1[2]
                        new_text_html=token_list2[0]+add_text+"<br />"+token_list2[1]+"<br />"+self.text2+token_list2[2]
                        values['body']=new_text_body
                        values['body_html']=new_text_html
                        #print ":::::::::::::::",values['attachments'][0][1]
                        vals = {
                                'name' : account_invoice_brw.name,
                                'type' : 'binary',
                                'datas' : values['attachments'][0][1],
                                'datas_fname' : values['attachments'][0][0],
                                'res_id' : a_id,
                                'res_model': 'vendors.invoice',
                        }
                        attachment_id = ir_attachment_obj.create(vals)
                        mail_mail_obj = self.env['mail.mail']
                        msg_id = mail_mail_obj.create(values)
                        msg_id.attachment_ids=[(6,0,[attachment_id.id])]
                        xxx=[(6,0,[attachment_id.id])]
                        if msg_id:
                            msg_id.send([msg_id])

        ne_email = self.duplicates(_partner)
        if ne_email:
            pa = ''
            for no in ne_email:
                pa += no.name + "\n"
            raise UserError(_('%s customer/s has no email address please enter email address')% (pa))

        return True
        
    def duplicates(self, arr):

        uniqueList = []
    
        # Iterate over the original list and for each element
        # add it to uniqueList, if its not already there.
        for elem in arr:
            if elem not in uniqueList:
                uniqueList.append(elem)
        
        # Return the list of unique elements        
        return uniqueList
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:






