# -*- coding: utf-8 -*-

from odoo import api, models


class AttachVendorInvoice(models.AbstractModel):
    _name = 'report.send_by_email.vendor_custom_report_invoice'

    @api.model
    def render_html(self, docids, data=None):
        docs = self.env['vendors.invoice'].browse(docids)
        invoices = []
        for _invoice_docs in docs:
            _invoice_line = []
            _custody_line = []
            _invoice_total = 0
            _custody_total = 0
            _tax_total = 0

            _invoice_data = []
            _invoice = []
            __page_num_total = 1
            _loop_last_paper = 0
            _page_num = 1

            _quantity = 0
            _total_discount = 0
            _total = 0
            for line in _invoice_docs.invoice_line:
                _invoice_line.append({
                    '_customer': line.customer_id.code,
                    '_purchase_price': line.purchase_price,
                    '_item': line.product_id.name,
                    '_quantity': line.product_qty,
                    '_price': line.unit_price,
                    '_purchase_taxes': line.purchase_taxes,
                    "_discount": round(line.unit_price, 4) - round(line.purchase_price, 4),
                    '_total_price': round(line.purchase_price, 4) * round(line.product_qty, 4) + round(
                        line.purchase_taxes, 4)
                })
                _quantity += line.product_qty
                _total_discount += round(line.unit_price, 4) - round(line.purchase_price, 4)
                _total += round(line.purchase_price, 4) * round(line.product_qty, 4) + round(line.purchase_taxes, 4)

                _invoice_total = _invoice_total + (
                round(line.purchase_price, 4) * round(line.product_qty, 4) + round(line.purchase_taxes, 4))
                _tax_total = _tax_total + round(line.purchase_taxes, 4)

            ass = []
            for lines in _invoice_docs.custody_lines:
                if lines.amount > 0:
                    ass.append({
                        'cus_name': lines.custody_name,
                        'amount': lines.amount
                    })
                    _custody_total += float(lines.amount)


            _custody_total += _invoice_docs.commision_total_amount

            docargs = {
                'doc_ids': docids,
                'doc_model': 'vendors.invoice',
                '_invoice_line': _invoice_line,
                '_invoice_total': _invoice_total,
                '_tax_total': _tax_total,
                '_quantity': _quantity,
                '_total_discount': _total_discount,
                '_total': _total,
                '_custody_line': ass,
                '_custody_total': _custody_total,
                '_data': _invoice_data,
                'docs': _invoice_docs

            }
            invoices.append(docargs)

        return self.env['report'].render('send_by_email.vendor_custom_report_invoice', {'all_data': invoices})




























        # for lines in docs.custody_lines:
        #     if lines.cash_account.name:
        #         _invoice_line.append({
        #             '_customer': '',
        #             '_purchase_price': '',
        #             '_item': '',
        #             '_quantity': '',
        #             '_price': '',
        #             'purchase_taxes': '',
        #             '_total_price': '',
        #             '_custody': lines.custody_name,
        #             '_total_price': lines.cash_amount
        #         })
        #         _custody_total += int(lines.cash_amount)
        #
        #     if lines.credit_account.name:
        #         _invoice_line.append({
        #             '_customer': '',
        #             '_purchase_price': '',
        #             '_item': '',
        #             '_quantity': '',
        #             '_price': '',
        #             'purchase_taxes': '',
        #             '_total_price': '',
        #             '_custody': lines.custody_name,
        #             '_total_price': lines.credit_amount
        #         })
        #         _custody_total += int(lines.credit_amount)
        #
        # # print(len(_invoice_line) / 12)
        # # print (len(_invoice_line))
        #
        # if (len(_invoice_line) == 12):
        #     _loop = 11
        # elif (len(_invoice_line) < 12):
        #     _loop = 11
        # elif (len(_invoice_line) > 12):
        #     _loop = 12
        #     if (len(_invoice_line) % 12 == 0):
        #         _loop_last_paper = 11
        #     else:
        #         _loop_last_paper = 11
        #
        # _page_num_total = len(_invoice_line) / _loop
        #
        #
        # for data in range(len(_invoice_line)):
        #     count += 1
        #     if count <= _loop:
        #         _invoice.append(_invoice_line[data])
        #     else:
        #         _invoice_data.append({
        #             '_last_page': '-1',
        #             '_invoice': _invoice,
        #             '_page_num': _page_num
        #         })
        #         _page_num = _page_num + 1
        #         __page_num_total += 1
        #
        #         if (__page_num_total == _page_num_total):
        #             _loop = _loop_last_paper
        #         _invoice = []
        #         _invoice.append(_invoice_line[data])
        #         count = 1
        #
        # # __page_num_total += 1
        # _invoice_data.append({
        #     '_last_page': '1',
        #     '_invoice': _invoice,
        #     '_page_num': _page_num
        # })
        #
        #
        # print _invoice_data
        #
        #
        # docargs = {
        #     'doc_ids': docids,
        #     'doc_model': 'vendors.invoice',
        #     '_invoice_line': _invoice_line,
        #     '_invoice_total': _invoice_total,
        #     '_custody_line': _custody_line,
        #     '_custody_total': _custody_total,
        #     '_data':_invoice_data,
        #     'docs': docs
        #
        # }
        #
        # return self.env['report'].render('send_by_email.vendor_custom_report_invoice', docargs)
