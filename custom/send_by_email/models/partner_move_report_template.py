# -*- coding: utf-8 -*-

import time
from odoo import api, models,_
from dateutil.parser import parse
from odoo.exceptions import UserError
from num2words import num2words
from openerp.addons.check_followups.models.money_to_text_ar import amount_to_text_arabic
from operator import itemgetter
from odoo.tools import ustr


class ReportSalesperson(models.AbstractModel):
    _name = 'report.custom_account.report_partner_moves_report_new'

    @api.model
    def render_html(self, docids, data=None):
	print "###########################################################**"
        #self.model = self.env.context.get('active_model')
        #docs = self.env[self.model].browse(self.env.context.get('active_id'))
        cr = self.env.cr
	dataaas = []
        self.model = data['form']['active_model1']
        docs = self.env[self.model].browse(data['form']['active_id1'])
        move_lines = []
        invoice_lines = []
        vendor_invoice_lines=[]
	#customer_invoices = self.env['account.invoice']
	partners_tag_obj = self.env['res.partner']
	#vendor_invoices = self.env['vendors.invoice']
	ml_obj = self.env['account.move.line']
	#vl_obj = self.env['vendors.custody.line']
	d_from = parse(docs.date_from)
	d_to = parse(docs.date_to)
	#s_man = docs.sales_man_id
	partners_ids = []
	if docs.partner_ids:
		partners_ids = docs.partner_ids
	elif docs.partner_tags:
		for pa in docs.partner_tags:
			partners_ids += partners_tag_obj.search([('category_id', '=', pa.id)])
	else:
		print "::::::::::::::::FFFFFFFFFFFFFFF"
		raise UserError("Please Select Partners or tags")

	for i in partners_ids:
		move_lines = []
		
		total = {'debit': 0.0, 'credit': 0.0, 'balance': 0.0}
		pre_total = {'debit': 0.0, 'credit': 0.0, 'balance': 0.0}
		is_pre_total = False
		invoice_detail = False
		
		if docs.date_from and docs.date_to:
			'''
		    if docs.invoice_detail:
		        invoice_detail = True
		        for order in orders:
		            if parse(order.date) < d_from and \
		                    order.account_id.id == i.property_account_receivable_id.id:
		                is_pre_total = True
		                pre_total['debit'] += abs(
		                    round(order.debit, 2))
		                pre_total['credit'] += abs(
		                    round(order.credit, 2))
		                pre_total['balance'] += abs(round(order.debit, 2)) - \
		                    abs(round(order.credit, 2))

		                total['debit'] += abs(
		                    round(order.debit, 2))
		                total['credit'] += abs(
		                    round(order.credit, 2))
		                total['balance'] += abs(round(order.debit, 2)) - \
		                    abs(round(order.credit, 2))

		            # vendors
		            elif parse(order.date) < d_from and \
		                    order.account_id.id == i.property_account_payable_id.id:
		                is_pre_total = True
		                pre_total['debit'] += abs(
		                    round(order.debit, 2))
		                pre_total['credit'] += abs(
		                    round(order.credit, 2))
		                pre_total['balance'] += abs(round(order.debit, 2)) - \
		                    abs(round(order.credit, 2))

		                total['debit'] += abs(
		                    round(order.debit, 2))
		                total['credit'] += abs(
		                    round(order.credit, 2))
		                total['balance'] += abs(round(order.debit, 2)) - \
		                    abs(round(order.credit, 2))

		            elif d_from <= parse(order.date) \
		                    and parse(order.date) <= d_to and order.payment_id and order.account_id.id == i.property_account_receivable_id.id:
		                total['debit'] += abs(
		                    round(order.debit, 2))
		                total['credit'] += abs(
		                    round(order.credit, 2))
		                total['balance'] += abs(round(order.debit, 2)) - \
		                    abs(round(order.credit, 2))
		                
		                move_lines.append(
		                    {'order': order,'date':order.date,'record_name':order.payment_id.name,'name':order.name,'sales_man_id':order.sales_man_id,'credit':order.credit,'debit':order.debit,'balance': total['balance']})

		            elif d_from <= parse(order.date) \
		                    and parse(order.date) <= d_to and order.payment_id and order.account_id.id == i.property_account_payable_id.id:
		                total['debit'] += abs(
		                    round(order.debit, 2))
		                total['credit'] += abs(
		                    round(order.credit, 2))
		                total['balance'] += abs(round(order.debit, 2)) - \
		                    abs(round(order.credit, 2))

		                move_lines.append(
		                    {'order': order,'date':order.date,'record_name':order.payment_id.name,'name':order.name,'sales_man_id':order.sales_man_id,'credit':order.credit,'debit':order.debit,'balance': total['balance']})

		        if s_man:
			    sql= (" select vi.id as iid from vendors_invoice vi " \
				"WHERE vi.vendor_id = %s and sales_man_id = %s ORDER BY vi.invoice_date asc,vi.id asc")
			    params = (i.id,s_man.id,)
			    cr.execute(sql, params)
			    vendor_invoice= vendor_invoices.browse([x['iid'] for x in cr.dictfetchall()])



		            #vendor_invoice=vendor_invoices.search([('vendor_id', '=', i.id),('sales_man_id', '=', s_man.id)],order='invoice_date asc,id asc')
		        else:


			    sql= (" select vi.id as iid from vendors_invoice vi " \
				"WHERE vi.vendor_id = %s ORDER BY vi.invoice_date asc,vi.id asc")
			    params = (i.id,)
			    cr.execute(sql, params)
			    vendor_invoice= vendor_invoices.browse([x['iid'] for x in cr.dictfetchall()])


		            #vendor_invoice=vendor_invoices.search([('vendor_id', '=', i.id)],order='invoice_date asc,id asc')
		        if vendor_invoice:
		            for invoice in vendor_invoice:
		                if d_from <= parse(invoice.invoice_date) and parse(invoice.invoice_date) <= d_to:
		                    for line in invoice.invoice_line:
		                        credit,debit=0.0,0.0
		                        total['debit'] += 0.0
		                        total['credit'] += abs(round(line.unit_price*line.product_qty, 2))
		                        credit=abs(round(line.unit_price*line.product_qty, 2))
		                        total['balance'] += 0.0 - abs(round(line.unit_price*line.product_qty, 2))
		                        name=ustr(line.product_id.name) +_(" quantity ")+ str(int(line.product_qty) or 0)+_(" × ")+ str(line.unit_price)+  _(" + ") + str(line.taxes_amount or 0.0) 
					#print "AAAAAA   0000"
		                        move_lines.append(
		                            {'order': order ,'invoice': line,'date':invoice.invoice_date,'record_name':invoice.name,'sales_man_id':invoice.sales_man_id,'credit':credit,'debit':debit,'name': name, 'balance': abs(total['balance'])})
		                    if invoice.custody_lines:
		                        for custody in invoice.custody_lines:
		                            if custody.credit_account and custody.credit_amount > 0:
		                                credit,debit=0.0,0.0
		                                total['debit'] += 0.0
		                                total['credit'] += abs(round(custody.credit_amount, 2))
		                                credit=abs(round(custody.credit_amount, 2))
		                                total['balance'] += 0.0 - abs(round(custody.credit_amount, 2))
						#print "AAAAAA   111"
		                                move_lines.append(
		                                    {'order': order ,'invoice': custody,'date':invoice.invoice_date,'record_name':invoice.name,'sales_man_id':invoice.sales_man_id,'credit':credit,'debit':debit,'name': custody.custody_name, 'balance': abs(total['balance'])})


		        sql= (" select vcl.id as iid from vendors_custody_line vcl " \
				"WHERE vcl.cash_account > 0")
		        params = (i.id,)
		        cr.execute(sql, params)
		        vendor_invoice_custody= vl_obj.browse([x['iid'] for x in cr.dictfetchall()])



		        #vendor_invoice_custody=vl_obj.search([('cash_account', '!=', False),('cash_amount', '>', 0)])
		        if vendor_invoice_custody:
		            for invoice_custody in vendor_invoice_custody:                                
		                if d_from <= parse(invoice_custody.vendor_invoice_id.invoice_date) and parse(invoice_custody.vendor_invoice_id.invoice_date) <= d_to:
		                    if s_man:
		                        if invoice_custody.vendor_invoice_id.sales_man_id and invoice_custody.vendor_invoice_id.sales_man_id.id == s_man.id:
		                            if invoice_custody.cash_account and invoice_custody.cash_amount > 0:
		                                if invoice_custody.vendor_invoice_id.sales_man_id.linked_customer.id == i.id:
						    #print "AAAAAA   222"
		                                    credit,debit=0.0,0.0
		                                    total['debit'] += 0.0
		                                    total['credit'] += abs(round(invoice_custody.cash_amount, 2))
		                                    credit=abs(round(invoice_custody.cash_amount, 2))
		                                    total['balance'] += 0.0 - abs(round(invoice_custody.cash_amount, 2))
		                                    move_lines.append(
		                                        {'order': order ,'invoice': invoice_custody,'date':invoice_custody.vendor_invoice_id.invoice_date,'record_name':invoice_custody.vendor_invoice_id.name,'sales_man_id':invoice_custody.vendor_invoice_id.sales_man_id,'credit':credit,'debit':debit,'name': invoice_custody.custody_name, 'balance': abs(total['balance'])})
		                                elif invoice_custody.vendor_invoice_id.vendor_id.id == i.id:
						    #print "AAAAAA   33333"
		                                    credit,debit=0.0,0.0
		                                    total['debit'] += 0.0
		                                    total['credit'] += abs(round(invoice_custody.credit_amount, 2))
		                                    credit=abs(round(invoice_custody.credit_amount, 2))
		                                    total['balance'] += 0.0 - abs(round(invoice_custody.credit_amount, 2))
		                                    move_lines.append(
		                                        {'order': order ,'invoice': invoice_custody,'date':invoice_custody.vendor_invoice_id.invoice_date,'record_name':invoice_custody.vendor_invoice_id.name,'sales_man_id':invoice_custody.vendor_invoice_id.sales_man_id,'credit':credit,'debit':debit,'name': invoice_custody.custody_name, 'balance': abs(total['balance'])})
		                    else:
		                        if invoice_custody.cash_account and invoice_custody.cash_amount > 0:
		                            if invoice_custody.vendor_invoice_id.sales_man_id.linked_customer.id == i.id:
		                                credit,debit=0.0,0.0
		                                total['debit'] += 0.0
		                                total['credit'] += abs(round(invoice_custody.cash_amount, 2))
		                                credit=abs(round(invoice_custody.cash_amount, 2))
		                                total['balance'] += 0.0 - abs(round(invoice_custody.cash_amount, 2))
						#print "AAAAAA   444444"
		                                move_lines.append(
		                                    {'order': order ,'invoice': invoice_custody,'date':invoice_custody.vendor_invoice_id.invoice_date,'record_name':invoice_custody.vendor_invoice_id.name,'sales_man_id':invoice_custody.vendor_invoice_id.sales_man_id,'credit':credit,'debit':debit,'name': invoice_custody.custody_name, 'balance': abs(total['balance'])})
		                            elif invoice_custody.vendor_invoice_id.vendor_id.id == i.id:
		                                credit,debit=0.0,0.0
		                                total['debit'] += 0.0
		                                total['credit'] += abs(round(invoice_custody.credit_amount, 2))
		                                credit=abs(round(invoice_custody.credit_amount, 2))
		                                total['balance'] += 0.0 - abs(round(invoice_custody.credit_amount, 2))
						#print "AAAAAA   5555"
		                                move_lines.append(
		                                    {'order': order ,'invoice': invoice_custody,'date':invoice_custody.vendor_invoice_id.invoice_date,'record_name':invoice_custody.vendor_invoice_id.name,'sales_man_id':invoice_custody.vendor_invoice_id.sales_man_id,'credit':credit,'debit':debit,'name': invoice_custody.custody_name, 'balance': abs(total['balance'])})


	 
		        #invoices=customer_invoices.search([('partner_id', '=', i.id)],order='date_invoice asc,id asc')
		        sql= (" select ai.id as iid from account_invoice ai " \
				"WHERE ai.partner_id = %s ORDER BY ai.date_invoice asc,ai.id asc")
		        params = (i.id,)
		        cr.execute(sql, params)
		        invoices= customer_invoices.browse([x['iid'] for x in cr.dictfetchall()])



		        if invoices:
		            for invoice in invoices:
		                for line in invoice.invoice_line_ids:
		                    if d_from <= parse(invoice.date_invoice) and parse(invoice.date_invoice) <= d_to:
		                        if s_man:
		                            if line.sales_man_id and line.sales_man_id.id == s_man.id:
		                                credit,debit=0.0,0.0
		                                if invoice.type == 'in_invoice':
		                                    total['debit'] += 0.0
		                                    total['credit'] += abs(round(line.price_subtotal+line.tax_amount, 2))
		                                    credit=abs(round(line.price_subtotal+line.tax_amount, 2))
		                                    total['balance'] += 0.0 - abs(round(line.price_subtotal+line.tax_amount, 2))
		                                else:
		                                    total['debit'] += abs(round(line.price_subtotal+line.tax_amount, 2))
		                                    debit=abs(round(line.price_subtotal+line.tax_amount, 2))
		                                    total['credit'] += 0.0
		                                    total['balance'] += abs(round(line.price_subtotal+line.tax_amount, 2))
		                                name=ustr(line.product_id.name) +_(" quantity ")+ str(int(line.quantity) or 0)+_(" × ")+ str(line.price_unit)+  _(" + ") + str(line.tax_amount or 0.0) 
						#print "AAAAAA   66666"
		                                move_lines.append(
		                                    {'order': order ,'invoice': line,'date':invoice.date_invoice,'record_name':invoice.name,'sales_man_id':line.sales_man_id,'credit':credit,'debit':debit,'name': name, 'balance': abs(total['balance'])})
		                        else:
		                            credit,debit=0.0,0.0
		                            if invoice.type == 'in_invoice':
		                                total['debit'] += 0.0
		                                total['credit'] += abs(round(line.price_subtotal+line.tax_amount, 2))
		                                credit=abs(round(line.price_subtotal+line.tax_amount, 2))
		                                total['balance'] += 0.0 - abs(round(line.price_subtotal+line.tax_amount, 2))
		                            else:
		                                total['debit'] += abs(round(line.price_subtotal+line.tax_amount, 2))
		                                debit=abs(round(line.price_subtotal+line.tax_amount, 2))
		                                total['credit'] += 0.0
		                                total['balance'] += abs(round(line.price_subtotal+line.tax_amount, 2))
		                            name=ustr(line.product_id.name) +_(" quantity ")+ str(int(line.quantity) or 0)+_(" × ")+ str(line.price_unit)+  _(" + ") + str(line.tax_amount or 0.0) 
					    #print "AAAAAA   77777777"
		                            move_lines.append(
		                                {'order': order ,'invoice': line,'date':invoice.date_invoice,'record_name':invoice.name,'sales_man_id':line.sales_man_id,'credit':credit,'debit':debit,'name': name, 'balance': abs(total['balance'])})

			'''
				
			if i.property_account_receivable_id.id:
				self._cr.execute("select sum(account_move_line.debit) debit, sum(account_move_line.credit) credit "\
								"from account_move_line "\
								"inner join account_move on account_move_line.move_id = account_move.id "\
								"inner join res_partner on account_move_line.partner_id= res_partner.id "\
								"where account_move.state='posted' and account_move.date<'%s' and account_move_line.partner_id=%s "\
								"and account_move_line.account_id=%s;" %(d_from, i.id, i.property_account_receivable_id.id))
				res = self._cr.dictfetchall()
				if res[0]['debit'] is not None and res[0]['credit'] is not None:
					is_pre_total = True
					pre_total['debit'] += abs(round(res[0]['debit'], 2))
					pre_total['credit'] += abs(round(res[0]['credit'], 2))
					pre_total['balance'] += abs(round(res[0]['debit'], 2)) - abs(round(res[0]['credit'], 2))

					total['debit'] += abs(round(res[0]['debit'], 2))
					total['credit'] += abs(round(res[0]['credit'], 2))
					total['balance'] += abs(round(res[0]['debit'], 2)) - abs(round(res[0]['credit'], 2))

				self._cr.execute("select account_move_line.id,account_move.date,res_partner.code,res_partner.name,account_move_line.debit, "\
									"account_move_line.credit, account_move_line.date,account_move_line.ref, "\
									"account_move_line.name "\
									"from account_move_line "\
									"inner join account_move on account_move_line.move_id = account_move.id "\
									"inner join res_partner on account_move_line.partner_id= res_partner.id "\
									"where account_move.state='posted' and account_move.date<='%s' and account_move.date>='%s' "\
									"and account_move_line.partner_id=%s "\
									"and account_move_line.account_id=%s "\
									"order by account_move.date;" %(d_to, d_from, i.id, i.property_account_receivable_id.id))
				res = self._cr.dictfetchall()
				for lin in res:
					print lin['debit']
					total['debit'] += abs(round(lin['debit'], 2))
					total['credit'] += abs(round(lin['credit'], 2))
					total['balance'] += abs(round(lin['debit'], 2)) - abs(round(lin['credit'], 2))
					
					order = ml_obj.browse(lin['id'])
					print "/////////////////////////////////",order.credit
					move_lines.append(
							{'order': order , 'n':lin['name'], 'date':lin['date'], 
							 'balance': total['balance'], 'credit': order.credit, 
							 'debit': order.debit, 'ref':order.ref, 'name': order.name})
			
			if i.property_account_payable_id.id:
				self._cr.execute("select sum(account_move_line.debit) debit, sum(account_move_line.credit) credit "\
								"from account_move_line "\
								"inner join account_move on account_move_line.move_id = account_move.id "\
								"inner join res_partner on account_move_line.partner_id= res_partner.id "\
								"where account_move.state='posted' and account_move.date<'%s' and account_move_line.partner_id=%s "\
								"and account_move_line.account_id=%s;" %(d_from, i.id, i.property_account_payable_id.id))
				res = self._cr.dictfetchall()
				if res[0]['debit'] is not None and res[0]['credit'] is not None:
					is_pre_total = True
					pre_total['debit'] += abs(round(res[0]['debit'], 2))
					pre_total['credit'] += abs(round(res[0]['credit'], 2))
					pre_total['balance'] += abs(round(res[0]['debit'], 2)) - abs(round(res[0]['credit'], 2))

					total['debit'] += abs(round(res[0]['debit'], 2))
					total['credit'] += abs(round(res[0]['credit'], 2))
					total['balance'] += abs(round(res[0]['debit'], 2)) - abs(round(res[0]['credit'], 2))

				self._cr.execute("select account_move_line.id,account_move.date,res_partner.code,res_partner.name,account_move_line.debit, "\
									"account_move_line.credit, account_move_line.date,account_move_line.ref, "\
									"account_move_line.name "\
									"from account_move_line "\
									"inner join account_move on account_move_line.move_id = account_move.id "\
									"inner join res_partner on account_move_line.partner_id= res_partner.id "\
									"where account_move.state='posted' and account_move.date<='%s' and account_move.date>='%s' "\
									"and account_move_line.partner_id=%s "\
									"and account_move_line.account_id=%s "\
									"order by account_move.date;" %(d_to, d_from, i.id, i.property_account_payable_id.id))
				res = self._cr.dictfetchall()
				for lin in res:
					print lin['debit']
					total['debit'] += abs(round(lin['debit'], 2))
					total['credit'] += abs(round(lin['credit'], 2))
					total['balance'] += abs(round(lin['debit'], 2)) - abs(round(lin['credit'], 2))
					
					order = ml_obj.browse(lin['id'])
					print "/////////////////////////////////",order.credit
					move_lines.append(
							{'order': order , 'n':lin['name'], 'date':lin['date'], 
							 'balance': total['balance'], 'credit': order.credit, 
							 'debit': order.debit, 'ref':order.ref, 'name': order.name})
					
				

			############
				
		docargs_o = {
		        'doc_ids': self.ids,
		        'doc_model': self.model,
		        'docs': docs,
		        'time': time,
		        'pre_total': pre_total,
		        'orders': move_lines,
		        'invoices': invoice_lines,
		        'vendor_invoices':vendor_invoice_lines,
		        'total': total,
		        'total_words': amount_to_text_arabic(abs(total['balance']), 'SAR'),
		        'is_pre_total':is_pre_total,
		        'invoice_detail':invoice_detail,
				'partner_id':i,
		    }

		dataaas.append(docargs_o)
        print 'email ***************************',dataaas
	docargs = {
                'doc_ids': self.ids,
                'doc_model': self.model,
                'docs': docs,
                'time': time,
				'dataaas':dataaas,
            }
        return self.env['report'].render('custom_account.report_partner_moves_report2', docargs)
