# -*- coding: utf-8 -*-


{
    "name" : "Send Invoices by Email",
    "version" : "1.0",
    "category" : "Accounting",
    "depends" : ['base','sale','account','vegetables', 'custom_account'],
    'summary': 'Apps helps to send mass email for invoices in one click.',
    "data": [
        'security/ir.model.access.csv',
        'views/mass_mail_view.xml',
        'views/vendors_view.xml',
        'views/custom_report_invoice.xml',
        'views/vendor_custom_report_invoice.xml',
        'views/my_template.xml',
        'views/my_vendor_template.xml',
        'views/customer_view.xml',
        #'views/partner_move_report_template.xml',
        #'views/partner_move_report.xml',
        #'views/partner_move_email_template.xml',
        
        
    ],
    'qweb': [],
    "auto_install": False,
    "installable": True,
    "images":['static/description/banner.png'],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
