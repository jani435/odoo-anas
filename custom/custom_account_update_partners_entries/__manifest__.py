# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : "Update Partners Entries",
    'version' : '1.0',
    'summary': '',
    'sequence': 3,
    'description':'',
    'category': 'Custom',
    'website': '',
    'depends' : ['multi_supplier_payment','custom_account', 'check_followups','custom_check_account'],

    'data': [
            #'views/account_payment_view.xml',
			#'views/first_payment_view.xml',
	
	'views/account_account_view.xml',
         
    ],
    #'css': ['static/src/css/account_move_line.css'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

