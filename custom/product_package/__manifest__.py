# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "VE.18.0 Product Category",
    'version': '1.0',
    'summary': '',
    'description': '',
    'category': 'Custom',
    'website': '',
    'depends': ['base', 'product', 'account_accountant',],

    'data': [
        
        'views/product_product.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
