# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.osv import expression


class ProductProduct(models.Model):
    _inherit = 'product.template'

    product_cate_id = fields.Many2many('product.cate', string="Product Category")


class ProductCate(models.Model):
    _name = 'product.cate'

    name = fields.Char('Name')
    no_from = fields.Integer('From')
    no_to = fields.Integer('To')
    price = fields.Float('Price')
    code = fields.Char('Code')
