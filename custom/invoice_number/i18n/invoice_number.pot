# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* invoice_number
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-28 07:04+0000\n"
"PO-Revision-Date: 2018-05-28 07:04+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_number_report_template
msgid "${company.name}"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_number_report_template
msgid "<small>\n"
"                                <span>Page</span>\n"
"                                <span class=\"page\"/>\n"
"                                of\n"
"                                <span class=\"topage\"/>\n"
"                            </small>"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_invoice_number_form_view
msgid "Activate"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_active
#: selection:manual.invoice.number,state:0
msgid "Active"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_invoice_number_search_view
msgid "Active Numbers"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_invoice_number_search_view
msgid "Areas Status"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_number_report_view
msgid "Cancel"
msgstr ""

#. module: invoice_number
#: model:ir.actions.act_window,help:invoice_number.manual_invoice_number_action
msgid "Click to create new invoice number."
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_create_uid
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_lines_create_uid
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_create_uid
msgid "Created by"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_create_date
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_lines_create_date
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_create_date
msgid "Created on"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_delegate_id
msgid "Delegate"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_display_name
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_lines_display_name
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_display_name
#: model:ir.model.fields,field_description:invoice_number.field_report_invoice_number_manual_number_report_template_display_name
msgid "Display Name"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_date_to
msgid "End Date"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_range_from
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_range_from
msgid "From"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_id
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_lines_id
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_id
#: model:ir.model.fields,field_description:invoice_number.field_report_invoice_number_manual_number_report_template_id
msgid "ID"
msgstr ""

#. module: invoice_number
#: selection:manual.invoice.number,state:0
msgid "Inactive"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_invoice_number_search_view
msgid "Inactive Numbers"
msgstr ""

#. module: invoice_number
#: code:addons/invoice_number/models/invoice_number.py:35
#, python-format
msgid "Invalid number range..!"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_invoice_number_tree_view
msgid "Invoice Number"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number___last_update
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_lines___last_update
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard___last_update
#: model:ir.model.fields,field_description:invoice_number.field_report_invoice_number_manual_number_report_template___last_update
msgid "Last Modified on"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_lines_write_uid
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_write_uid
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_write_uid
msgid "Last Updated by"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_lines_write_date
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_write_date
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_write_date
msgid "Last Updated on"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_invoice_number_form_view
msgid "Manual Invoice Number"
msgstr ""

#. module: invoice_number
#: model:ir.actions.act_window,name:invoice_number.manual_invoice_number_action
#: model:ir.ui.menu,name:invoice_number.manual_invoice_number_menu
#: model:ir.ui.menu,name:invoice_number.manual_invoice_number_root_menu
msgid "Manual Number"
msgstr ""

#. module: invoice_number
#: model:ir.actions.act_window,name:invoice_number.action_manual_number_report_wizard
#: model:ir.actions.report.xml,name:invoice_number.action_manual_number_report_template
#: model:ir.ui.menu,name:invoice_number.manual_number_report_menu
msgid "Manual Number Report"
msgstr ""

#. module: invoice_number
#: model:ir.model,name:invoice_number.model_manual_number_report_wizard
msgid "Manual Number Report Wizard"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_name
msgid "Name"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_lines_note
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_note
msgid "Note"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_invoice_number_form_view
msgid "Note Lines"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_note_lines
#: model:ir.ui.view,arch_db:invoice_number.manual_invoice_number_form_view
msgid "Notes"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_lines_number
msgid "Number"
msgstr ""

#. module: invoice_number
#: code:addons/invoice_number/models/invoice_number.py:45
#, python-format
msgid "Number range already exist..!"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_number_report_view
msgid "Print"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_invoice_number_form_view
msgid "Range"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_lines_manual_number_id
msgid "Reference"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_date_from
msgid "Start Date"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_state
msgid "Status"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_invoice_number_form_view
msgid "Suspend"
msgstr ""

#. module: invoice_number
#: code:addons/invoice_number/models/invoice_number.py:52
#, python-format
msgid "This Number not in range...!"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_range_to
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_range_to
msgid "To"
msgstr ""

#. module: invoice_number
#: model:ir.model.fields,field_description:invoice_number.field_manual_invoice_number_type
#: model:ir.model.fields,field_description:invoice_number.field_manual_number_report_wizard_type
msgid "Type"
msgstr ""

#. module: invoice_number
#: selection:manual.invoice.number,type:0
#: selection:manual_number_report.wizard,type:0
msgid "Vendor Invoice"
msgstr ""

#. module: invoice_number
#: model:ir.model,name:invoice_number.model_manual_invoice_number
msgid "manual.invoice.number"
msgstr ""

#. module: invoice_number
#: model:ir.model,name:invoice_number.model_manual_invoice_number_lines
msgid "manual.invoice.number.lines"
msgstr ""

#. module: invoice_number
#: model:ir.ui.view,arch_db:invoice_number.manual_number_report_view
msgid "or"
msgstr ""

#. module: invoice_number
#: model:ir.model,name:invoice_number.model_report_invoice_number_manual_number_report_template
msgid "report.invoice_number.manual_number_report_template"
msgstr ""

