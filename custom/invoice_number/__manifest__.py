# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Manual invoice numbers",
    'version': '1.0',
    'summary': '',
    'sequence': 2,
    'description': '',
    'category': 'IES',
    'website': '',
    'depends': ['account_accountant', 'hr'],

    'data': [
            'security/ir.model.access.csv',
            'sequences/manual_invoice_number.xml',
            'views/invoice_number.xml',
            'reports/manual_number_report.xml',
            'wizard/manual_number_report.xml',
	    'views/invoice_number_tree.xml',
	    'reports/report_manual_number_template.xml',
	    'wizard/invoice_number_tree_wiz.xml',
	    'wizard/invoice_number_update_wiz.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

