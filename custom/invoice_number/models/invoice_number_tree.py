# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class InvoiceNumbersTree(models.Model):
	_name = 'manual.invoice.number.tree'


	date_from  = fields.Date(string=' Date From')
	date_to = fields.Date(string=' Date To')
	range_from = fields.Integer('From')
	range_to = fields.Integer('To')
	note_lines = fields.One2many('manual.invoice.number.tree.lines', 'manual_number_id', string='Notes')
	sales_man_id = fields.Many2one('hr.employee', string='Sales Man', domain=[('is_sales_man', '=', True)])
	manual_invoice_number_id = fields.Many2one('manual.invoice.number', string='Manual Invoice Number')
	type = fields.Selection(
	    [('vendor_invoice', 'Vendor Invoice'), ('purchase', 'Purchase Orders'), ('sale', 'Sales Orders'),
	     ('delivery', 'Delivery Orders'), ('receipts', 'Receipts Orders'), ('receipt', 'Receipt'), ('pay', 'Pay'),
	     ('customer', 'Customer Invoices'), ('vendor', 'Vendor Bills')], string='Type')


	@api.multi
	def get_data(self):
		
		domain = [('manual_number_id.type','=','vendor_invoice')]
		if self.sales_man_id:
			domain.append(('delegate_id','=',self.sales_man_id.id))
		if self.manual_invoice_number_id:
			domain.append(('manual_number_id','=',self.manual_invoice_number_id.id))
		
		range_list = range(self.range_from, self.range_to+1)
				
		note_lines_list = []

		for num in range_list:
			domain2 = domain
			domain2.append(('number','=',num))
			manual_number_line_ids = self.env['manual.invoice.number.lines'].sudo().search(domain2)
			for line in manual_number_line_ids:
				manual_number_ids = self.env['manual.invoice.number']
				invoice_numbers = self.env['vendors.invoice'].sudo().search([('invoice_date' ,'>=' , self.date_from),
					('invoice_date' ,'<=' , self.date_to) , ('transmission_number','=',num),('manual_invoice_number','=',line.manual_number_id.id)])
				
				if invoice_numbers:
					for invoice in invoice_numbers :

							note_lines_list.append({
							'manual_number_id': self.id,
							'number': invoice.transmission_number,
							'manual_number': invoice.manual_invoice_number.name,
							'invoice_number' : invoice.name,
							'state' :invoice.state.title(),
							'vendor' : invoice.sales_man_id.name,
							'date':invoice.invoice_date,
							'amount':invoice.net_total,
							'vendor_invoice_id': invoice.id,
							'manual_invoice_number_id': line.manual_number_id.id,
							'note': line.note ,
							'manual_invoice_number_line_id': line.id,
							})

							line.invoice_number = invoice.name
							line.state = invoice.state.title
							line.date = invoice.invoice_date
							line.vendor = invoice.sales_man_id.name
							line.amount = invoice.net_total
							line.vendor_invoice_id = invoice.id

				if not invoice_numbers :
						note_lines_list.append({
						'manual_number_id': self.id,
						'number': num,
						'manual_number': line.manual_number_id.name,
						'manual_invoice_number_id': line.manual_number_id.id,
						'note': line.note,
						'manual_invoice_number_line_id': line.id,

						})
		
		for line in self.note_lines:
			line.unlink()

		if note_lines_list:
			[self.env['manual.invoice.number.tree.lines'].sudo().create(dicts) for dicts in note_lines_list ]

		

	




class InvoiceNumbersTreeLines(models.Model):
    _name = 'manual.invoice.number.tree.lines'

    manual_number_id = fields.Many2one('manual.invoice.number.tree', string='Reference',  ondelete='cascade', index=True)
    number = fields.Char('Number')
    manual_number = fields.Char('Number of Manual Number')
    invoice_number = fields.Char('Number of Invoice')
    state = fields.Char('State of Invoice')
    vendor = fields.Char('Sales Man')
    date = fields.Date('Date')
    amount = fields.Float('Amount')
    note = fields.Char('Note')
    vendor_invoice_id = fields.Many2one('vendors.invoice', string='Vendor Invoice Reference', index=True, copy=False)
    manual_invoice_number_id = fields.Many2one('manual.invoice.number', string='Manual Invoice Number', index=True)
    manual_invoice_number_line_id = fields.Many2one('manual.invoice.number.lines', string='Manual Invoice Line Number', index=True)



    @api.multi
    def write(self,vals):
    	for rec in self:
    		super(InvoiceNumbersTreeLines,self).write(vals)
        	if 'note' in vals:
        		if rec.manual_invoice_number_line_id:
        			rec.manual_invoice_number_line_id.write({'note':vals['note']})
        		else:
        			manual_number_line_ids = self.env['manual.invoice.number.lines'].sudo().search(
        				[('manual_number_id','=',rec.manual_invoice_number_id.id),('number','=',int(rec.number))])
        			if manual_number_line_ids:
        				manual_number_line_ids[0].write({'note':vals['note']})

        return True













