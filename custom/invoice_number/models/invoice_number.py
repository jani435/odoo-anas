# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class InvoiceNumbers(models.Model):
    _name = 'manual.invoice.number'

    name = fields.Char('Name', readonly=True)
    note = fields.Text('Note')
    range_from = fields.Integer('From')
    range_to = fields.Integer('To')
    for_all_delegates = fields.Boolean('For All Delegates')
    delegate_id = fields.Many2one('hr.employee', 'Delegate')

    active = fields.Boolean(default=True)
    state = fields.Selection([('True', 'Active'), ('False', 'Inactive')], string='Status', default='True')
    type = fields.Selection(
        [('vendor_invoice', 'Vendor Invoice'), ('purchase', 'Purchase Orders'), ('sale', 'Sales Orders'),
         ('delivery', 'Delivery Orders'), ('receipts', 'Receipts Orders'), ('receipt', 'Receipt'), ('pay', 'Pay'),
         ('customer', 'Customer Invoices'), ('vendor', 'Vendor Bills')], string='Type')
    note_lines = fields.One2many('manual.invoice.number.lines', 'manual_number_id', string='Notes')

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('manual.invoice.number')
        create_id = super(InvoiceNumbers, self).create(vals)
	for num in range(vals['range_from'],vals['range_to']+1):
	    line_vals = {
	        'manual_number_id': create_id.id,
	        'number': num,
	        #'delegate_id': vals['delegate_id'],
	    }
	    self.env['manual.invoice.number.lines'].create(line_vals)

        return create_id

    @api.multi
    def update_data(self):
	v_inv_obj= self.env['vendors.invoice']
	for rec in self:
		if rec.type == 'vendor_invoice':
			if not rec.for_all_delegates:
				for line in rec.note_lines:
					#print "000000"
					if line.number:
						#print "111"
						v_ids= v_inv_obj.search([('transmission_number','=',line.number),('manual_invoice_number','=',rec.id)],limit=1)
						#print str(line.number), " UUU  ",v_ids, "  qqq    ",rec.delegate_id.name
						line.vendor_invoice_id= v_ids and v_ids[0].id or False

						line.date= v_ids and v_ids[0].invoice_date or False
						line.state= v_ids and v_ids[0].state or False
						line.amount= v_ids and v_ids[0].net_total or False
						line.sales_man_id= v_ids and v_ids[0].sales_man_id.id or False
						line.delegate_id= rec.delegate_id.id
			else:
				for line in rec.note_lines:
					#print "000000"
					if line.number:
						#print "111"
						v_ids= v_inv_obj.search([('transmission_number','=',line.number),('manual_invoice_number','=',rec.id)],limit=1)
						#print str(line.number), " UUU  ",v_ids, "  qqq    ",rec.delegate_id.name
						line.vendor_invoice_id= v_ids and v_ids[0].id or False

						line.date= v_ids and v_ids[0].invoice_date or False
						line.state= v_ids and v_ids[0].state or False
						line.amount= v_ids and v_ids[0].net_total or False
						line.sales_man_id= v_ids and v_ids[0].sales_man_id.id or False
						line.delegate_id= rec.delegate_id.ids

    @api.multi
    def write(self,vals):
        if not self._context.get('skip',False) and not vals.get('note_lines',False):
            raise ValidationError(_('You can only make Invoice number unactive and create another one'))
        return super(InvoiceNumbers,self).write(vals)
 
    @api.multi
    def change_status(self):
        for rec in self:
		rec.with_context(skip=True).write({'state':str(not rec.active),'active':not rec.active})

    @api.constrains('range_to', 'range_from', 'active', 'type', 'delegate_id', 'for_all_delegates')
    def _check_range(self):


        if self.range_from > self.range_to:
            raise ValidationError(_('Invalid number range..!'))
	lines_obj= self.env['manual.invoice.number.lines']
	if not self.id:
		line_ids= lines_obj.search([('manual_number_id.state','!=','False'),('manual_number_id.delegate_id','=',self.delegate_id.id),('manual_number_id.for_all_delegates','=',self.for_all_delegates),('number','in',list(range(int(self.range_from),int(self.range_to)+1))),('vendor_invoice_id','=',False)])
	else:
		line_ids= lines_obj.search([('manual_number_id.state','!=','False'),('manual_number_id.delegate_id','=',self.delegate_id.id),('manual_number_id.for_all_delegates','=',self.for_all_delegates),('number','in',list(range(int(self.range_from),int(self.range_to)+1))),('vendor_invoice_id','=',False),('manual_number_id','!=',self.id)])
	if line_ids:
            raise ValidationError(_('Sorry, Some numbers are empty..!'))
 

    @api.constrains('note_lines')
    def _check_note_lines(self):
        for line in self.note_lines:

            if line.number not in list(range(int(self.range_from), int(self.range_to)+1)):
                    raise ValidationError(_('This Number not in range...!'))


class InvoiceNumbersLines(models.Model):
    _name = 'manual.invoice.number.lines'

    #vendor_invoice_id = fields.Many2one('vendors.invoice', string='Vendor Invoice Reference', index=True, copy=False)
    manual_number_id = fields.Many2one('manual.invoice.number', string='Reference',  ondelete='cascade', index=True)
    number = fields.Integer('Number')
    note = fields.Char('Note')
    invoice_number = fields.Char('Number of Invoice')
    state = fields.Char(string='State of Invoice')
    sales_man_id = fields.Many2one('hr.employee', string='Sales Man')
    currency_id= fields.Many2one('res.currency', string='Currency', copy=False)
    date = fields.Date(string='Date',)
    amount = fields.Monetary(string='Amount')
    delegate_id = fields.Many2one('hr.employee', 'Delegate')
    for_all_delegates = fields.Boolean(related="manual_number_id.for_all_delegates", store=True, string='For All Delegates')

class InvoicesUpdateNumber(models.Model):
    _name = 'invoices.update.number'

    #vendor_invoice_id = fields.Many2one('vendors.invoice', string='Vendor Invoice Reference', index=True, copy=False)
    manual_number_id = fields.Many2one('manual.invoice.number', string='Reference',  ondelete='cascade', index=True)
    number = fields.Integer('Number')
    #note = fields.Char('Note')
    invoice_number = fields.Char('Number of Invoice')
    state = fields.Char(string='State of Invoice')
    sales_man_id = fields.Many2one('hr.employee', string='Sales Man')
    #currency_id= fields.Many2one('res.currency', string='Currency', copy=False)
    date = fields.Date(string='Date',)
    amount = fields.Float(string='Amount')
    #delegate_id = fields.Many2one('hr.employee', 'Delegate')


