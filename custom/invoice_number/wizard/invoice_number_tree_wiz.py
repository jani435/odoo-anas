# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class InvoiceNumbersTreeWiz(models.TransientModel):
	_name = 'manual.invoice.number.tree.wiz'


	date_from  = fields.Date(string=' Date From')
	date_to = fields.Date(string=' Date To')
	range_from = fields.Integer('From')
	range_to = fields.Integer('To')
	note_lines = fields.One2many('manual.invoice.number.tree.lines', 'manual_number_id', string='Notes')
	sales_man_id = fields.Many2one('hr.employee', string='Sales Man', domain=[('is_sales_man', '=', True)])
	manual_invoice_number_id = fields.Many2one('manual.invoice.number', string='Manual Invoice Number')
	delegate_id = fields.Many2one('hr.employee', 'Delegate')
	for_all_delegates = fields.Boolean('For All Delegates')
	type = fields.Selection(
	    [('vendor_invoice', 'Vendor Invoice'), ('purchase', 'Purchase Orders'), ('sale', 'Sales Orders'),
	     ('delivery', 'Delivery Orders'), ('receipts', 'Receipts Orders'), ('receipt', 'Receipt'), ('pay', 'Pay'),
	     ('customer', 'Customer Invoices'), ('vendor', 'Vendor Bills')], string='Type')

	@api.multi
        @api.onchange('delegate_id')
	def onchange_delegate_id(self):
		for rec in self:
			if not rec.delegate_id:
				return {'value':{'manual_invoice_number_id':False},'domain':{'manual_invoice_number_id':[('id','!=',-1)]}}
			return {'value':{'manual_invoice_number_id':False},'domain':{'manual_invoice_number_id':[('delegate_id','=',rec.delegate_id.id)]}}


	@api.multi
	def get_data(self):
		note_lines_list = []
		line_ids = []
		domain = [('manual_number_id.type','=','vendor_invoice'),('manual_number_id.state','!=','False')]
		if self.sales_man_id:
			domain.append(('sales_man_id','=',self.sales_man_id.id))
		if self.delegate_id:
			domain.append(('manual_number_id.delegate_id','=',self.delegate_id.id))
		if self.for_all_delegates:
			domain.append(('for_all_delegates','=',self.for_all_delegates))
		if self.date_from:
			domain.append(('date','>=',self.date_from))
		if self.date_to:
			domain.append(('date','<=',self.date_to))
		if self.range_from:
			domain.append(('number','>=',self.range_from))
		if self.range_to:
			domain.append(('number','<=',self.range_to))
		if self.manual_invoice_number_id:
			domain.append(('manual_number_id','=',self.manual_invoice_number_id.id))
		print  " DDDDD ", domain
		manual_number_line_ids = self.env['manual.invoice.number.lines'].sudo().search(domain)
		action = self.env.ref('invoice_number.manual_invoice_number_line_action').read()[0]
		if manual_number_line_ids:
		#for line in manual_number_line_ids:
			#line_ids.append(line.id)
			action.update({'domain':[('id','in',manual_number_line_ids.ids)]})
		else:
			action.update({'domain':[('id','=',-1)]})
		return action

		













