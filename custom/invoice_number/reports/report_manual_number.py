# -*- coding: utf-8 -*-

from odoo import api, models


class ReportManualNumber(models.AbstractModel):
    _name = 'report.invoice_number.report_manual_number_template'

    @api.model
    def render_html(self, docids, data=None):
        docs = self.env['manual.invoice.number.tree'].browse(docids)
        _invoice_data = []
        _invoice = []
        invoices = []
        for _invoice_docs in docs:
		docargs = {
		        'doc_ids': docids,
		        'doc_model': 'manual.invoice.number.tree',
		        '_data': _invoice_data,
		        'docs': _invoice_docs

		}

		invoices.append(docargs)



        return self.env['report'].render('invoice_number.report_manual_number_template', {'all_data': docs})


















