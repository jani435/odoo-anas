# -*- coding: utf-8 -*-

from odoo import api, models, _


class PrintManualNumber(models.AbstractModel):
    _name = 'report.invoice_number.manual_number_report_template'

    @api.model
    def render_html(self, docids, data=None):
        result = []
        if data['form']['type'] != 'all':
            manual_number_type = [data['form']['type']]
        else:
            manual_number_type = ['purchase', 'sale', 'delivery', 'vendor_invoice', 'receipts', 'receipt', 'pay',
                                  'customer', 'vendor']

        manual_numbers = self.env['manual.invoice.number'].search([('type', 'in', manual_number_type)])

        for manual_number in manual_numbers:
            result += self.get_lines_data(manual_number, data['form'])
        docargs = {
            'doc_ids': docids,
            'data': result,
            'date_from': data['form']['date_from'],
            'date_to': data['form']['date_to'],
        }

        return self.env['report'].render('invoice_number.manual_number_report_template', docargs)

    @api.multi
    def get_lines_data(self, manual_number, data):
        result = []
        if (data['type'] == 'vendor_invoice' or data['type'] == 'all') and manual_number.type == 'vendor_invoice':
            if int(data['range_from']) > 0 and int(data['range_to']):
                invoices = self.env['vendors.invoice'].search([
                    ('transmission_number', 'in',
                     list(range(int(manual_number.range_from), int(manual_number.range_to) + 1))),
                    ('transmission_number', 'in',
                     list(range(int(data['range_from']), int(data['range_to']) + 1)))])
            else:
                invoices = self.env['vendors.invoice'].search([('transmission_number', '!=', '')])
            for invoice in invoices.sorted(key=lambda r: int(r.transmission_number)):
                if data['date_from'] and data['date_to']:
                    if data['date_from'] <= invoice.invoice_date <= data['date_to']:
                        result.append({
                            'date': invoice.invoice_date,
                            'name': invoice.name,
                            'type': 'فاتورة مزرعة',
                            'state': invoice.state.title(),
                            'amount': invoice.net_total,
                            'currency_id': invoice.currency_id,
                            'partner': invoice.vendor_id.name,
                            'number': invoice.transmission_number,
                            'note': '',
                            'notebook_number': manual_number.name,
                        })
                else:
                    result.append({
                        'date': invoice.invoice_date,
                        'name': invoice.name,
                        'type': 'فاتورة مزرعة',
                        'state': invoice.state.title(),
                        'amount': invoice.net_total,
                        'currency_id': invoice.currency_id,
                        'partner': invoice.vendor_id.name,
                        'number': invoice.transmission_number,
                        'note': '',
                        'notebook_number': manual_number.name,
                    })

        if (data['type'] == 'sale' or data['type'] == 'all') and manual_number.type == 'sale':

            if int(data['range_from']) > 0 and int(data['range_to']):
                sales_orders = self.env['sale.order'].search([
                    ('manual_inv_num', 'in',
                     list(range(int(manual_number.range_from), int(manual_number.range_to) + 1))),
                    ('manual_inv_num', 'in',
                     list(range(int(data['range_from']), int(data['range_to']) + 1)))])
            else:
                sales_orders = self.env['sale.order'].search([('manual_inv_num', '!=', '')])
            for sale in sales_orders.sorted(key=lambda r: int(r.manual_inv_num)):
                if data['date_from'] and data['date_to']:
                    if data['date_from'] <= sale.date_order <= data['date_to']:
                        result.append({
                            'date': sale.date_order,
                            'name': sale.name,
                            'type': 'امر بيع',
                            'state': sale.state.title(),
                            'amount': sale.amount_total,
                            'currency_id': sale.currency_id,
                            'partner': sale.partner_id.name,
                            'number': sale.manual_inv_num,
                            'note': '',
                            'notebook_number': manual_number.name,
                        })
                else:
                    result.append({
                        'date': sale.date_order,
                        'name': sale.name,
                        'type': 'امر بيع',
                        'state': sale.state.title(),
                        'amount': sale.amount_total,
                        'currency_id': sale.currency_id,
                        'partner': sale.partner_id.name,
                        'number': sale.manual_inv_num,
                        'note': '',
                        'notebook_number': manual_number.name,
                    })

        if (data['type'] == 'purchase' or data['type'] == 'all') and manual_number.type == 'purchase':

            if int(data['range_from']) > 0 and int(data['range_to']):
                purchase_orders = self.env['purchase.order'].search([
                    ('manual_inv_num', 'in',
                     list(range(int(manual_number.range_from), int(manual_number.range_to) + 1))),
                    ('manual_inv_num', 'in',
                     list(range(int(data['range_from']), int(data['range_to']) + 1)))])
            else:
                purchase_orders = self.env['purchase.order'].search([('manual_inv_num', '!=', '')])
            for purchase in purchase_orders.sorted(key=lambda r: int(r.manual_inv_num)):
                if data['date_from'] and data['date_to']:
                    if data['date_from'] <= purchase.date_order <= data['date_to']:
                        result.append({
                            'date': purchase.date_order,
                            'name': purchase.name,
                            'type': 'امر شراء',
                            'state': purchase.state.title(),
                            'amount': purchase.amount_total,
                            'currency_id': purchase.currency_id,
                            'partner': purchase.partner_id.name,
                            'number': purchase.manual_inv_num,
                            'note': '',
                            'notebook_number': manual_number.name,
                        })
                else:
                    result.append({
                        'date': purchase.date_order,
                        'name': purchase.name,
                        'type': 'امر بيع',
                        'state': purchase.state.title(),
                        'amount': purchase.amount_total,
                        'currency_id': purchase.currency_id,
                        'partner': purchase.partner_id.name,
                        'number': purchase.manual_inv_num,
                        'note': '',
                        'notebook_number': manual_number.name,
                    })

        if (data['type'] == 'delivery' or data['type'] == 'all') and manual_number.type == 'delivery':

            if int(data['range_from']) > 0 and int(data['range_to']):
                delivery_orders = self.env['stock.picking'].search([('picking_type_id.code', '=', 'outgoing'),
                                                                    ('manual_inv_num', 'in',
                                                                     list(range(int(manual_number.range_from),
                                                                                int(manual_number.range_to) + 1))),
                                                                    ('manual_inv_num', 'in',
                                                                     list(range(int(data['range_from']),
                                                                                int(data['range_to']) + 1)))])
            else:
                delivery_orders = self.env['stock.picking'].search(
                    [('manual_inv_num', '!=', ''), ('picking_type_id.code', '=', 'outgoing')])
            for delivery in delivery_orders.sorted(key=lambda r: int(r.manual_inv_num)):
                if data['date_from'] and data['date_to']:
                    if data['date_from'] <= delivery.min_date <= data['date_to']:
                        result.append({
                            'date': delivery.min_date,
                            'name': delivery.name,
                            'type': 'صرف بضاعة',
                            'state': delivery.state.title(),
                            'amount': 0,
                            'currency_id': self.env.user.company_id.currency_id,
                            'partner': delivery.partner_id.name,
                            'number': delivery.manual_inv_num,
                            'note': '',
                            'notebook_number': manual_number.name,
                        })
                else:
                    result.append({
                        'date': delivery.min_date,
                        'name': delivery.name,
                        'type': 'صرف بضاعة',
                        'state': delivery.state.title(),
                        'amount': 0,
                        'currency_id': self.env.user.company_id.currency_id,
                        'partner': delivery.partner_id.name,
                        'number': delivery.manual_inv_num,
                        'note': '',
                        'notebook_number': manual_number.name,
                    })

        if (data['type'] == 'receipts' or data['type'] == 'all') and manual_number.type == 'receipts':

            if int(data['range_from']) > 0 and int(data['range_to']):
                receipts_orders = self.env['stock.picking'].search([('picking_type_id.code', '=', 'incoming'),
                                                                    ('manual_inv_num', 'in',
                                                                     list(range(int(manual_number.range_from),
                                                                                int(manual_number.range_to) + 1))),
                                                                    ('manual_inv_num', 'in',
                                                                     list(range(int(data['range_from']),
                                                                                int(data['range_to']) + 1)))])
            else:
                receipts_orders = self.env['stock.picking'].search(
                    [('manual_inv_num', '!=', ''), ('picking_type_id.code', '=', 'incoming')])
            for receipt in receipts_orders.sorted(key=lambda r: int(r.manual_inv_num)):
                if data['date_from'] and data['date_to']:
                    if data['date_from'] <= receipt.min_date <= data['date_to']:
                        result.append({
                            'date': receipt.min_date,
                            'name': receipt.name,
                            'type': 'استلام بضاعة',
                            'state': receipt.state.title(),
                            'amount': 0,
                            'currency_id': self.env.user.company_id.currency_id,
                            'partner': receipt.partner_id.name,
                            'number': receipt.manual_inv_num,
                            'note': '',
                            'notebook_number': manual_number.name,
                        })
                else:
                    result.append({
                        'date': receipt.min_date,
                        'name': receipt.name,
                        'type': 'استلام بضاعة',
                        'state': receipt.state.title(),
                        'amount': 0,
                        'currency_id': self.env.user.company_id.currency_id,
                        'partner': receipt.partner_id.name,
                        'number': receipt.manual_inv_num,
                        'note': '',
                        'notebook_number': manual_number.name,
                    })

        if (data['type'] == 'receipt' or data['type'] == 'all') and manual_number.type == 'receipt':

            if int(data['range_from']) > 0 and int(data['range_to']):
                payments = self.env['account.payment'].search([('payment_type', '=', 'inbound'),
                                                               ('manual_inv_num', 'in',
                                                                list(range(int(manual_number.range_from),
                                                                           int(manual_number.range_to) + 1))),
                                                               ('manual_inv_num', 'in',
                                                                list(range(int(data['range_from']),
                                                                           int(data['range_to']) + 1)))])
            else:
                payments = self.env['account.payment'].search(
                    [('manual_inv_num', '!=', ''), ('payment_type', '=', 'inbound')])
            for payment in payments.sorted(key=lambda r: int(r.manual_inv_num)):
                if data['date_from'] and data['date_to']:
                    if data['date_from'] <= payment.payment_date <= data['date_to']:
                        result.append({
                            'date': payment.payment_date,
                            'name': payment.name,
                            'type': 'سند استلام',
                            'state': payment.state.title(),
                            'amount': payment.amount,
                            'currency_id': payment.currency_id,
                            'partner': payment.partner_id.name,
                            'number': payment.manual_inv_num,
                            'note': '',
                            'notebook_number': manual_number.name,
                        })
                else:
                    result.append({
                        'date': payment.payment_date,
                        'name': payment.name,
                        'type': 'سند استلام',
                        'state': payment.state.title(),
                        'amount': payment.amount,
                        'currency_id': payment.currency_id,
                        'partner': payment.partner_id.name,
                        'number': payment.manual_inv_num,
                        'note': '',
                        'notebook_number': manual_number.name,
                    })

        if (data['type'] == 'pay' or data['type'] == 'all') and manual_number.type == 'pay':

            if int(data['range_from']) > 0 and int(data['range_to']):
                payments = self.env['account.payment'].search([('payment_type', '=', 'outbound'),
                                                               ('manual_inv_num', 'in',
                                                                list(range(int(manual_number.range_from),
                                                                           int(manual_number.range_to) + 1))),
                                                               ('manual_inv_num', 'in',
                                                                list(range(int(data['range_from']),
                                                                           int(data['range_to']) + 1)))],
                                                              order='manual_inv_num')
            else:
                payments = self.env['account.payment'].search(
                    [('manual_inv_num', '!=', ''), ('payment_type', '=', 'outbound')],
                    order='manual_inv_num')
            for payment in payments:
                if data['date_from'] and data['date_to']:
                    if data['date_from'] <= payment.payment_date <= data['date_to']:
                        result.append({
                            'date': payment.payment_date,
                            'name': payment.name,
                            'type': 'سند استلام',
                            'state': payment.state.title(),
                            'amount': payment.amount,
                            'currency_id': payment.currency_id,
                            'partner': payment.partner_id.name,
                            'number': payment.manual_inv_num,
                            'note': '',
                            'notebook_number': manual_number.name,
                        })
                else:
                    result.append({
                        'date': payment.payment_date,
                        'name': payment.name,
                        'type': 'سند استلام',
                        'state': payment.state.title(),
                        'amount': payment.amount,
                        'currency_id': payment.currency_id,
                        'partner': payment.partner_id.name,
                        'number': payment.manual_inv_num,
                        'note': '',
                        'notebook_number': manual_number.name,
                    })

        if (data['type'] == 'customer' or data['type'] == 'all') and manual_number.type == 'customer':

            if int(data['range_from']) > 0 and int(data['range_to']):
                customers_invoices = self.env['account.invoice'].search([('type', 'in', ['out_invoice', 'out_refund']),
                                                                         ('manual_inv_num', 'in',
                                                                          list(range(int(manual_number.range_from),
                                                                                     int(manual_number.range_to) + 1))),
                                                                         ('manual_inv_num', 'in',
                                                                          list(range(int(data['range_from']),
                                                                                     int(data['range_to']) + 1)))])
            else:
                customers_invoices = self.env['account.invoice'].search(
                    [('manual_inv_num', '!=', ''), ('type', 'in', ['out_invoice', 'out_refund'])])
            for invoice in customers_invoices.sorted(key=lambda r: int(r.manual_inv_num)):
                if data['date_from'] and data['date_to']:
                    if data['date_from'] <= invoice.date_invoice <= data['date_to']:
                        result.append({
                            'date': invoice.date_invoice,
                            'name': invoice.number,
                            'type': 'فاتورة عميل',
                            'state': invoice.state.title(),
                            'amount': invoice.amount_total,
                            'currency_id': invoice.currency_id,
                            'partner': invoice.partner_id.name,
                            'number': invoice.manual_inv_num,
                            'note': '',
                            'notebook_number': manual_number.name,
                        })
                else:
                    result.append({
                        'date': invoice.date_invoice,
                        'name': invoice.number,
                        'type': 'فاتورة عميل',
                        'state': invoice.state.title(),
                        'amount': invoice.amount_total,
                        'currency_id': invoice.currency_id,
                        'partner': invoice.partner_id.name,
                        'number': invoice.manual_inv_num,
                        'note': '',
                        'notebook_number': manual_number.name,
                    })

        if (data['type'] == 'vendor' or data['type'] == 'all') and manual_number.type == 'vendor':

            if int(data['range_from']) > 0 and int(data['range_to']):
                vendors_invoices = self.env['account.invoice'].search([('type', 'in', ['in_invoice', 'in_refund']),
                                                                       ('manual_inv_num', 'in',
                                                                        list(range(int(manual_number.range_from),
                                                                                   int(manual_number.range_to) + 1))),
                                                                       ('manual_inv_num', 'in',
                                                                        list(range(int(data['range_from']),
                                                                                   int(data['range_to']) + 1)))])
            else:
                vendors_invoices = self.env['account.invoice'].search(
                    [('manual_inv_num', '!=', ''), ('type', 'in', ['in_invoice', 'in_refund'])])
            for invoice in vendors_invoices.sorted(key=lambda r: int(r.manual_inv_num)):
                if data['date_from'] and data['date_to']:
                    if data['date_from'] <= invoice.date_invoice <= data['date_to']:
                        result.append({
                            'date': invoice.date_invoice,
                            'name': invoice.number,
                            'type': 'فاتورة مورد',
                            'state': invoice.state.title(),
                            'amount': invoice.amount_total,
                            'currency_id': invoice.currency_id,
                            'partner': invoice.partner_id.name,
                            'number': invoice.manual_inv_num,
                            'note': '',
                            'notebook_number': manual_number.name,
                        })
                else:
                    result.append({
                        'date': invoice.date_invoice,
                        'name': invoice.number,
                        'type': 'فاتورة مورد',
                        'state': invoice.state.title(),
                        'amount': invoice.amount_total,
                        'currency_id': invoice.currency_id,
                        'partner': invoice.partner_id.name,
                        'number': invoice.manual_inv_num,
                        'note': '',
                        'notebook_number': manual_number.name,
                    })

        for line in manual_number.note_lines.sorted(key=lambda r: int(r.number)):
            if line.number in list(range(int(data['range_from']), int(data['range_to']) + 1)):
                result.append({
                    'date': '-',
                    'name': '-',
                    'type': '-',
                    'state': '-',
                    'notebook_number': manual_number.name,
                    'partner': '-',
                    'amount': 0,
                    'currency_id': self.env.user.company_id.currency_id,
                    'number': line.number,
                    'note': line.note,
                })

        return result
