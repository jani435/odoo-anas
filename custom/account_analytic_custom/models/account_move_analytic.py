# -*- coding: utf-8 -*-

import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare
from odoo.tools.misc import formatLang

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging



from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import ValidationError, UserError, Warning


class account_analytic_account(models.Model):
    _inherit = "account.analytic.account"
    tag_id = fields.Many2one('account.analytic.tag', 'Analytic Tag', required=True, ondelete='cascade', index=True)

class account_move(models.Model):
    _inherit = "account.move"


     
    
class account_move_line(models.Model):
    _inherit = "account.move.line"
    def go_to_wiz_analytic(self):
        for rec in self:

                return {'name': _('Tags Analytics'),
                        
                        
##                        'context':'none',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'wiz.go.analytic',
                        'view_id': False,
                        'type': 'ir.actions.act_window',
                        'target':'new',
			'res_id': self.detail_id and self.detail_id.id or False,
			'context':{'default_move_line_id':rec.id}
                        
                        
                }
    def save(self):
        # TDE FIXME: does not seem to be used -> actually, it does
        # TDE FIXME: move me somewhere else, because the return indicated a wizard, in pack op, it is quite strange
        # HINT: 4. How to manage lots of identical products?
        # Create a picking and click on the Mark as TODO button to display the Lot Split icon. A window will pop-up. Click on Add an item and fill in the serial numbers and click on save button
        """for pack in self:
            if pack.moveds_id.tracking != 'none':
                pack.write({'qty_done': sum(pack.pack_lot_ids.mapped('qty'))})"""
	print "AAAAAAAAA"
        return {'type': 'ir.actions.act_window_close'} 


 
    @api.multi
    @api.depends('detail_id')
    def _get_total_analytic_amount(self):
	for rec in self:
		amount = 0.00
		for line in rec.detail_id.tagline_ids:
			amount += (line.amount or 0.00)
		rec.total_analytic_amount = amount or 0.00


    tags_amount_totals = fields.Float('Line Amount', compute='_line_amount_get',store=True,)
    wiz_ids = fields.One2many('wiz.go.analytic', 'line_sum_id', string='Analytic line sum', copy=True)
    #state = fields.Selection(selection=[ ('draft', 'Unposted'), ('posted', 'Posted')], default='draft',related='move_id.state', string="State")
    detail_id= fields.Many2one('wiz.go.analytic', string="Details")
    total_analytic_amount = fields.Float(compute='_get_total_analytic_amount',store=True, string='Total Analytic Amount', )

    @api.multi
    @api.depends('wiz_ids', 'wiz_ids.lines_amount')
    def _line_amount_get(self):
        for move in self:
            total = 0.0
            for line in move.wiz_ids:
                total = line.lines_amount
            move.tags_amount_totals = total

   
####

class wiz_go_analytic(models.Model):
    _name = "wiz.go.analytic"
    
    @api.multi
    @api.depends('move_line_id')
    def _get_total_amount(self):
	for rec in self:
		rec.total_amount = (rec.move_line_id.debit > 0.00 and rec.move_line_id.debit) or (rec.move_line_id.credit > 0.00 and rec.move_line_id.credit) or 0.00

    move_line_id = fields.Many2one('account.move.line', 'Move Line', ondelete='cascade', index=True) 
    line_sum_id = fields.Many2one('account.move.line', 'Analytic amount', required=False, ondelete='cascade', index=True) 
    tag_id = fields.Many2one('account.analytic.tag', 'Analytic Tag', required=True,  ondelete='cascade', index=True)
    tagline_ids = fields.One2many('analytic.analytic.tag', 'moveds_id', string='Analytic Tag Items', copy=True)
    lines_amount = fields.Float('Total tags Amount', compute='_total_lines_amount',store=True, )

    total_amount = fields.Float(compute='_get_total_amount',store=True, string='Total Amount', )

    @api.multi
    @api.depends('tagline_ids', 'tagline_ids.amount')
    def _total_lines_amount(self):
        for move in self:
            total = 0.0
            for line in move.tagline_ids:
                total += line.amount
            move.lines_amount = total

    @api.constrains('lines_amount','total_amount')
    def _check_detail_total_amount(self):
	for rec in self:
		if rec.lines_amount > rec.total_amount:
                	raise ValidationError(_('Sorry, You Cannot Exceed the Total Amount.'))

    def action_save(self):
        move_line_obj= self.env['account.move.line']
	move_line_obj.browse(self._context.get('active_id',False)).detail_id = self.id
	
        return {'type': 'ir.actions.act_window_close'} 
   
####


class analytic_analytic_tag(models.Model):

    _name = "analytic.analytic.tag"
    _description = "Tag Analytics"
    _order = 'sequence asc'


    @api.multi
    @api.depends('move_line_id','amount')
    def _get_total_s_amount(self):
	for rec in self:
		rec.s_amount = (rec.move_line_id.debit > 0.00 and rec.amount) or (rec.move_line_id.credit > 0.00 and (- 1 *(rec.amount))) or 0.00


    date = fields.Date(related="move_line_id.date", store=True, string='Date', index=True) 
    partner_id = fields.Many2one('res.partner', related="move_line_id.partner_id", store=True, string='Partner', ondelete='cascade', index=True) 
    account_id = fields.Many2one('account.account', related="move_line_id.account_id", store=True, string='Account', ondelete='cascade', index=True) 
    move_line_id = fields.Many2one('account.move.line', string='Move Line', ondelete='cascade', index=True) 
    moveds_id = fields.Many2one('wiz.go.analytic', 'Analytic Tag', required=False , index=True)
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic Account', required=True)
    amount = fields.Float('amount', )
    sequence = fields.Integer('Sequence',default=10)
    tag_id = fields.Many2one('account.analytic.tag', related='analytic_account_id.tag_id', string='Analytic Tag', store=True)
    note = fields.Char('Note')
    s_amount = fields.Float(compute='_get_total_s_amount',store=True, string='amount', )



    

