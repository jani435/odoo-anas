# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : "Account Analytic Custom",
    'version' : '1.0',
    'summary': '',
    'sequence': 3,
    'description':'',
    'category': 'Custom',
    'website': '',
    'depends' : ['base', 'account','analytic'],

    'data': [
            'views/account_move_analytic_view.xml',
    ],
    #'css': ['static/src/css/account_move_line.css'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

