# -*- coding: utf-8 -*-
# Copyright 2016 Onestein (<http://www.onestein.eu>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, api


class PurchaseOrderConfirmWizard(models.TransientModel):
    _name = "purchase.order.confirm.wizard"
    _description = "Wizard - Purchase Order Confirm"

    @api.multi
    def confirm_purchase_orders(self):
        self.ensure_one()
        active_ids = self._context.get('active_ids')
        orders = self.env['purchase.order'].browse(active_ids)
        for order in orders:
            if order.state in ['draft', 'sent']:
                order.button_confirm()
