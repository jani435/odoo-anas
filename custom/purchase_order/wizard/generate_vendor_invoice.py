# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


class GenerateVendorInvoice(models.TransientModel):
    _name = "generate.vendor_invoice.wizard"

    vendor_id = fields.Many2one('res.partner', string='Vendor', domain=[('supplier', '=', True)])
    product_id = fields.Many2one('product.product', string='Product')
    quantity = fields.Integer('Quantity')
    price = fields.Float('Price')
    invoice_number = fields.Char('Manual Number')
    date_invoice = fields.Date('Invoice Date', default=fields.Date.today)
    invoice_lines = fields.One2many('invoice.line.data', 'invoice_id', string='Invoice Line')

    @api.onchange('vendor_id', 'product_id', 'quantity', 'price')
    def fill_invoice_lines_data(self):
        invoice_lines = []
        old_ids = []
        if self.vendor_id:
            invoices = self.env['purchase.order'].sudo().search([('partner_id', '=', self.vendor_id.id),
                                                                 ('invoice_status', '!=', 'invoiced'),
                                                                 ('state', '=', 'purchase')])
            for invoice in invoices:
                for line in invoice.order_line:
                    if line.qty_invoiced < line.product_qty:
                        if self.product_id and self.product_id.id != line.product_id.id:
                            continue
                        if self.quantity > 0 and self.quantity != line.new_quantity - line.new_qty_invoiced:
                            continue
                        if self.price > 0.0 and self.price != line.new_price:
                            continue
                        tmp_price = (line.product_qty - line.qty_invoiced) * line.price_unit
                        if line.taxes_id:
                            tmp_price = (line.product_qty - line.qty_invoiced) * line.price_unit + (
                                ((line.product_qty - line.qty_invoiced) * line.price_unit * line.taxes_id[
                                    0].amount) / 100)
                        invoice_lines.append((0, 0, {
                            'invoice_id': self._origin.id,
                            'purchase_id': line.order_id.id,
                            'purchase_line_id': line.id,
                            'product_id': line.product_id.id,
                            'unit_id': line.product_uom.id,
                            'new_qty_invoiced': line.new_qty_invoiced,
                            'new_quantity': (line.new_quantity + line.free_quantity) - line.new_qty_invoiced,
                            'weight': line.new_weight,
                            'new_price': line.new_price,
                            'total_price': tmp_price,
                            'tax_id': line.taxes_id[0].id if line.taxes_id else False,
                        }))

            # collect old ids and added to mew product lis
            for line in self.invoice_lines:
                if line.selected:
                    if line.purchase_line_id.id not in [purchase_line_id[2]['purchase_line_id'] for
                                                        purchase_line_id in invoice_lines]:
                        old_ids.insert(0, (0, 0, {
                            'invoice_id': self._origin.id,
                            'purchase_id': line.purchase_id.id,
                            'purchase_line_id': line.purchase_line_id.id,
                            'product_id': line.product_id.id,
                            'unit_id': line.unit_id.id,
                            'new_quantity': line.new_quantity,
                            'weight': line.weight,
                            'new_price': line.new_price,
                            'total_price': line.total_price,
                            'tax_id': line.tax_id.id if line.tax_id else False,
                            'selected': True,
                        }))
                    else:
                        for purchase_line_id in invoice_lines:
                            if purchase_line_id[2]['purchase_line_id'] == line.purchase_line_id.id:
                                purchase_line_id[2]['selected'] = True
                                break

        old_ids.extend(invoice_lines)
        return {
            'domain': {'products_ids': []},
            'value': {'invoice_lines': old_ids}
        }

    @api.multi
    def generate_invoice(self):
        invoice_lines = []
        for line in self.invoice_lines:
            if line.selected:
                if (line.purchase_line_id.new_quantity + line.purchase_line_id.free_quantity) < line.new_quantity:
                    raise UserError(_('Quantity must be less than ' + str(line.new_quantity) + ' for product ' + str(
                        line.product_id.display_name)))
                elif line.purchase_line_id.new_price < line.new_price:
                    raise UserError(_('Price must be less than ' + str(line.new_price) + ' for product ' + str(
                        line.product_id.display_name)))
                # get account_id from product or product category
                if line.product_id.property_account_income_id:
                    account_id = line.product_id.property_account_expense_id.id
                elif line.product_id.categ_id.property_account_income_categ_id:
                    account_id = line.product_id.categ_id.property_account_income_categ_id.id
                else:
                    account_id = False

                invoice_lines.append((0, 0, {
                    'purchase_id': line.purchase_line_id.order_id.id,
                    'purchase_line_id': line.purchase_line_id.id,
                    'new_quantity': line.new_quantity,
                    'new_price': line.new_price,
                    'new_weight': line.weight,
                    'free_quantity': line.purchase_line_id.free_quantity,
                    'product_id': line.product_id.id,
                    'name': line.product_id.name,
                    'origin': line.purchase_line_id.order_id.origin,
                    'uom_id': line.unit_id.id,
                    'price_unit': line.purchase_line_id.price_unit,
                    'quantity': line.new_quantity * line.weight,
                    'invoice_line_tax_ids': [(4, line.tax_id.id)] if line.tax_id else False,
                    'account_id': account_id,
                    'invoice_number': line.purchase_line_id.order_id.manual_inv_num,
                }))

        if invoice_lines:

            # get default journal for vendor invoice
            journal_id = self.env['account.journal'].search(
                [('type', '=', 'purchase'), ('company_id', '=', self.env.user.company_id.id)], limit=1)

            # Create Invoice for vendor
            vendor_invoice = self.env['account.invoice'].sudo().create({
                "partner_id": self.vendor_id.id,
                "journal_type": 'purchase',
                "type": 'in_invoice',
                "journal_id": journal_id.id if journal_id else False,
                "manual_inv_num": self.invoice_number,
                "date_invoice": self.date_invoice,
                "invoice_line_ids": invoice_lines
            })

            # fill field origin with all purchases origin name in this invoice
            purchase_ids = vendor_invoice.invoice_line_ids.mapped('purchase_id')
            if purchase_ids:
                vendor_invoice.origin = ', '.join(purchase_ids.mapped('name'))

            # return Invoice form to allow user to edit invoice before validate
            action = self.env.ref('account.action_invoice_tree2')
            result = action.read()[0]
            res = self.env.ref('account.invoice_supplier_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = vendor_invoice.id
            return result


class InvoiceLinesData(models.TransientModel):
    _name = 'invoice.line.data'

    @api.depends('new_quantity', 'new_price', 'tax_id')
    def _compute_total_amount(self):
        for rec in self:
            tax_amount = 0
            if rec.tax_id:
                tax_amount = (rec.new_quantity * rec.new_price) * rec.tax_id.amount / 100

            rec.total_price = rec.new_quantity * rec.new_price + tax_amount

    invoice_id = fields.Many2one('generate.vendor_invoice.wizard', ondelete='cascade')
    purchase_id = fields.Many2one('purchase.order', 'Order Reference')
    purchase_line_id = fields.Many2one('purchase.order.line')
    product_id = fields.Many2one('product.product', ondelete='cascade', string='Product')
    unit_id = fields.Many2one('product.uom', string="Unit")
    new_quantity = fields.Integer('Quantity')
    new_qty_invoiced = fields.Integer('Quantity Invoiced')
    weight = fields.Float('Weight')
    new_price = fields.Float('Price')
    total_price = fields.Float(string='Total Price', compute='_compute_total_amount')
    tax_id = fields.Many2one('account.tax', string='Taxes')
    selected = fields.Boolean('Selected')

    @api.onchange('new_quantity', 'new_price')
    def _onchange_new_quantity_new_price(self):
        if self.new_quantity > 0:
            if self.purchase_line_id.new_quantity < self.new_quantity:
                raise UserError(_('Quantity must be less than ' + str(self.new_quantity) + ' for product ' + str(
                    self.product_id.display_name)))
        elif self.new_price > 0:
            if self.purchase_line_id.new_price < self.new_price:
                raise UserError(_('Price must be less than ' + str(self.new_price) + ' for product ' + str(
                    self.product_id.display_name)))

    @api.multi
    def action_select(self):
        print '************* '
        self.selected = not self.selected
