# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class StockPicking(models.Model):
    _inherit = "stock.picking"

    manual_inv_num = fields.Char('Manual Number')

    @api.constrains('manual_inv_num')
    def _check_manual_inv_num(self):

        invoice_numbers = []
        if self.env['stock.picking'].browse(self.id).picking_type_id.code == 'incoming':
            invoice_numbers = self.env['manual.invoice.number'].sudo().search([('type', '=', 'receipts')])
        elif self.env['stock.picking'].browse(self.id).picking_type_id.code == 'outgoing':
            invoice_numbers = self.env['manual.invoice.number'].sudo().search([('type', '=', 'delivery')])

        if self.manual_inv_num and invoice_numbers:
            if len(self.env['stock.picking'].sudo().search(
                    [('manual_inv_num', '=', self.manual_inv_num), ('state', '!=', 'cancel')])) > 1:
                raise ValidationError(_('This number is reserved in another stock operation..!'))

            invoice_number_found = False

            for invoice in invoice_numbers:
                if invoice.range_from <= int(self.manual_inv_num) <= invoice.range_to:
                    invoice_number_found = True
                    break
            if not invoice_number_found and len(invoice_numbers) > 0:
                raise ValidationError(_('Invalid invoice number, this number not found in Stock Operations range.'))

    @api.model
    def create(self, vals):
        cr_location_id = self.env.context.get('cr_location_id')
        res = super(StockPicking, self).create(vals)
        if cr_location_id:
            res.location_dest_id = cr_location_id.id
        return res
