# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "VE.18.05 Custom Inventory",
    'version': '1.0',
    'summary': '',
    'sequence': 5,
    'description': '',
    'category': 'Custom',
    'website': '',
    'depends': ['base_setup', 'stock', 'sale', 'purchase'],

    'data': [
        'security/security.xml',
        'views/res_user_view.xml',
        'views/stock_packing.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
